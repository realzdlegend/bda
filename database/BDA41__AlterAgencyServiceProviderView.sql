ALTER VIEW [v_AgencyProviders]

AS

SELECT  AP.Id, AgencyId, ProviderId, OA.Name Agency, OA.Code Code, P.ParentServiceProviderId,
OP.Name Provider, OP.Code AIN FROM AgencyProviders AP
INNER JOIN Agency A ON AP.AgencyId = A.Id
INNER JOIN Organizations OA ON A.Id = OA.Id
INNER JOIN ServiceProvider P ON AP.ProviderId = P.Id
INNER JOIN Organizations OP ON P.Id = OP.Id

GO 