/*
   Sunday, February 14, 20165:57:09 PM
   User: 
   Server: .
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
--BEGIN TRANSACTION
--SET QUOTED_IDENTIFIER ON
--SET ARITHABORT ON
--SET NUMERIC_ROUNDABORT OFF
--SET CONCAT_NULL_YIELDS_NULL ON
--SET ANSI_NULLS ON
--SET ANSI_PADDING ON
--SET ANSI_WARNINGS ON
--COMMIT
--BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.EbillsBeneficiary SET (LOCK_ESCALATION = TABLE)
--GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.EbillsBeneficiary', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EbillsBeneficiary', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EbillsBeneficiary', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.SettlementBeneficiary ADD
--	EbillsBenId int NULL
--GO
--ALTER TABLE dbo.SettlementBeneficiary ADD CONSTRAINT
--	FK_SettlementBeneficiary_EbillsBeneficiary FOREIGN KEY
--	(
--	EbillsBenId
--	) REFERENCES dbo.EbillsBeneficiary
--	(
--	Id
--	) ON UPDATE  NO ACTION 
--	 ON DELETE  NO ACTION 
	
--GO
--ALTER TABLE dbo.SettlementBeneficiary SET (LOCK_ESCALATION = TABLE)
--GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.SettlementBeneficiary', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SettlementBeneficiary', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SettlementBeneficiary', 'Object', 'CONTROL') as Contr_Per 