/*
   Saturday, February 13, 20161:04:16 PM
   User: 
   Server: .
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
--BEGIN TRANSACTION
--SET QUOTED_IDENTIFIER ON
--SET ARITHABORT ON
--SET NUMERIC_ROUNDABORT OFF
--SET CONCAT_NULL_YIELDS_NULL ON
--SET ANSI_NULLS ON
--SET ANSI_PADDING ON
--SET ANSI_WARNINGS ON
--COMMIT
--BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.Settlement
--	DROP CONSTRAINT FK_Settlement_Agency
--GO
--ALTER TABLE dbo.Agency SET (LOCK_ESCALATION = TABLE)
--GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.Agency', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Agency', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Agency', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.Settlement SET (LOCK_ESCALATION = TABLE)
--GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.Settlement', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Settlement', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Settlement', 'Object', 'CONTROL') as Contr_Per 