
CREATE TABLE [dbo].[RemitaPaymentNotification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[rrr] [nvarchar](250) NULL,
	[Channnel] [nvarchar](250) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[DebitDate] [datetime] NOT NULL,
	[Bank] [nvarchar](200) NULL,
	[Branch] [nvarchar](200) NULL,
	[ServiceTypeId] [nvarchar](250) NULL,
	[DateSent] [datetime] NOT NULL,
	[UniqueIdentifier] [nvarchar] (250) NULL,
	[DateRequested] [datetime] NOT NULL,
	[Currency] [int] NOT NULL,
	[InvoiceNumber] [nvarchar](250) NULL,
	[PayerName] [nvarchar](250) NULL, 
	[PayerEmail] [nvarchar](250) NULL,
	[PayerPhoneNumber] [nvarchar](50) NULL,  
	[CreatedDate] [datetime] NOT NULL,
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[RemitaPaymentNotification] SET (LOCK_ESCALATION = TABLE)
 
