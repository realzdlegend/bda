/*
   Sunday, February 14, 20165:46:11 PM
   User: 
   Server: .
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
--BEGIN TRANSACTION
--SET QUOTED_IDENTIFIER ON
--SET ARITHABORT ON
--SET NUMERIC_ROUNDABORT OFF
--SET CONCAT_NULL_YIELDS_NULL ON
--SET ANSI_NULLS ON
--SET ANSI_PADDING ON
--SET ANSI_WARNINGS ON
--COMMIT
--BEGIN TRANSACTION
--GO
--CREATE TABLE dbo.EbillsBeneficiary
--	(
--	Id int NOT NULL IDENTITY (1, 1),
--	Name varchar(100) NOT NULL,
--	BeneficiaryCode varchar(50) NOT NULL,
--	Description varchar(1000) NULL
--	)  ON [PRIMARY]
--GO
--ALTER TABLE dbo.EbillsBeneficiary ADD CONSTRAINT
--	PK_EbillsBeneficiary PRIMARY KEY CLUSTERED 
--	(
--	Id
--	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--GO
--ALTER TABLE dbo.EbillsBeneficiary SET (LOCK_ESCALATION = TABLE)
--GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.EbillsBeneficiary', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EbillsBeneficiary', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EbillsBeneficiary', 'Object', 'CONTROL') as Contr_Per 