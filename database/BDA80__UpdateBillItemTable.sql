/*
   29 January 201811:05:21
   User: 
   Server: DESKTOP-ISQFERH
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_BillItems
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	Amount decimal(18, 2) NOT NULL,
	Description varchar(1500) NOT NULL,
	SessionKey uniqueidentifier NOT NULL,
	EntryDate datetime NOT NULL,
	Quantity decimal(18, 2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_BillItems SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_BillItems ON
GO
IF EXISTS(SELECT * FROM dbo.BillItems)
	 EXEC('INSERT INTO dbo.Tmp_BillItems (Id, Amount, Description, SessionKey, EntryDate, Quantity)
		SELECT Id, Amount, Description, SessionKey, EntryDate, CONVERT(decimal(18, 2), Quantity) FROM dbo.BillItems WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_BillItems OFF
GO
DROP TABLE dbo.BillItems
GO
EXECUTE sp_rename N'dbo.Tmp_BillItems', N'BillItems', 'OBJECT' 
GO
ALTER TABLE dbo.BillItems ADD CONSTRAINT
	PK_BillItems PRIMARY KEY NONCLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE CLUSTERED INDEX IX_BillItems ON dbo.BillItems
	(
	SessionKey
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
select Has_Perms_By_Name(N'dbo.BillItems', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.BillItems', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.BillItems', 'Object', 'CONTROL') as Contr_Per 