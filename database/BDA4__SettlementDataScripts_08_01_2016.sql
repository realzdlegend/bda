
--/****** Object:  Table [dbo].[Settlement]    Script Date: 01/08/2016 10:52:28 ******/
--INSERT [dbo].[Settlement] ([Id], [BeneficiaryCode], [BeneficiaryShare], [AgencyShare]) VALUES (10, N'98765', 4, 96)
--/****** Object:  Table [dbo].[SettlementBeneficiary]    Script Date: 01/08/2016 10:52:28 ******/
--SET IDENTITY_INSERT [dbo].[SettlementBeneficiary] ON
--INSERT [dbo].[SettlementBeneficiary] ([Id], [Name], [BeneficiaryCode], [Share], [SettlementId]) VALUES (1, N'FBN-1', N'93908393', 20, 10)
--INSERT [dbo].[SettlementBeneficiary] ([Id], [Name], [BeneficiaryCode], [Share], [SettlementId]) VALUES (2, N'FBN-2', N'93939693', 20, 10)
--INSERT [dbo].[SettlementBeneficiary] ([Id], [Name], [BeneficiaryCode], [Share], [SettlementId]) VALUES (3, N'AVITECH', N'939374393', 60, 10)
--SET IDENTITY_INSERT [dbo].[SettlementBeneficiary] OFF
