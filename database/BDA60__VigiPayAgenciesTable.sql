CREATE TABLE [dbo].[VigiPayAgencies](
	[AgencyId] [int] NOT NULL,
	[UserEmail] [varchar](128) NULL,
	[Password] [varchar](128) NULL,
	[CorporateCode] [varchar](32) NULL,
 CONSTRAINT [PK_VigiPayAgencies_1] PRIMARY KEY CLUSTERED 
(
	[AgencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO