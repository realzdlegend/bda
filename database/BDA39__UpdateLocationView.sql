ALTER VIEW [v_LocationView]

AS 

SELECT  L.Id,L.ParentLocationId, L.AgencyId, L.Code, L.LocationType, L.LocationAddress, L.StateId,
        L.IsRegion, L.FidesicId,  R.Id RegionId, R.Code RegionCode, 
        R.LocationAddress RegionAddress FROM Locations L
LEFT JOIN LocationMapping LM ON L.Id = LM.LocationId
LEFT JOIN Locations R ON LM.RegionId = R.Id
 
GO
 