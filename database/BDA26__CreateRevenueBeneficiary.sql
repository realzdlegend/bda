

/****** Object:  Table [dbo].[RevenueBeneficiary]    Script Date: 10/13/2016 15:02:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RevenueBeneficiary](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[RevenueId] [INT] NOT NULL,
	[SettlementAccountId] [INT] NOT NULL,
	[SharePercentage] [FLOAT] NOT NULL,
	[DeductFeeFrom] [BIT] NOT NULL,
 CONSTRAINT [PK_RevenueBeneficiary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RevenueBeneficiary]  WITH CHECK ADD  CONSTRAINT [FK_RevenueBeneficiary_Revenue] FOREIGN KEY([RevenueId])
REFERENCES [dbo].[Revenue] ([Id])
GO

ALTER TABLE [dbo].[RevenueBeneficiary] CHECK CONSTRAINT [FK_RevenueBeneficiary_Revenue]
GO

ALTER TABLE [dbo].[RevenueBeneficiary]  WITH CHECK ADD  CONSTRAINT [FK_RevenueBeneficiary_SettlementAccount] FOREIGN KEY([SettlementAccountId])
REFERENCES [dbo].[SettlementAccount] ([Id])
GO

ALTER TABLE [dbo].[RevenueBeneficiary] CHECK CONSTRAINT [FK_RevenueBeneficiary_SettlementAccount]
GO


