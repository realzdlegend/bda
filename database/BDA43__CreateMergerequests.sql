
CREATE TABLE [MergeRequests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](150) NULL,
	[ParentId] [varchar](150) NULL,
	[ChildId] [varchar](150) NULL,
	[DataType] [varchar](150) NULL,
	[Statusflag] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL,
	[Info] [varchar](250) NULL,
 CONSTRAINT [PK_MergeRequests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO