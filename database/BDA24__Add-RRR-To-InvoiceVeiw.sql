
/****** Object:  View [dbo].[v_InvoiceView]    Script Date: 09/29/2016 11:54:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[v_InvoiceView]
AS
SELECT Id, I.AgencyId, I.InvoiceNumber, InvoiceDate, I.Amount,I.[Sent], I.CreatedDate, 
I.DueDate, I.ReturnedInvoiceId, ProviderId, RevenueId, I.CurrencyId,I.RemitaRetrievalReference, LocationId, Deleted, 
[Sent] SentToFidesic, ISNULL(PaymentCount, 0) PaymentCount, ISNULL(R.AmountPaid, 0) AmountPaid, 
ISNULL(DepositOption, '') DepositOption, P.CreatedDate PaymentDate, CI.CreatedBy, CI.Name, CI.Email, CI.Phone, 
CI.CAddress, BillCount, MerchantReference, PaidScore PaymentPrediction FROM dbo.Invoice I 
INNER JOIN dbo.CustomerInvoices CI ON I.Id = CI.InvoiceId
LEFT JOIN dbo.InvoicePredictions IP ON I.Id = IP.InvoiceId
LEFT JOIN 
(SELECT InvoiceNumber, MAX(RR.PaymentId) PaymentId, COUNT(RR.PaymentId) PaymentCount, SUM(RR.Amount) AmountPaid 
FROM dbo.Remittances RR INNER JOIN dbo.Payments ON RR.PaymentId = dbo.Payments.PaymentId
GROUP BY InvoiceNumber) R
ON I.InvoiceNumber = R.InvoiceNumber
LEFT JOIN dbo.Payments P ON R.PaymentId = P.PaymentId










GO


