/*
   Sunday, February 14, 20167:59:10 PM
   User: 
   Server: .
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
--BEGIN TRANSACTION
--SET QUOTED_IDENTIFIER ON
--SET ARITHABORT ON
--SET NUMERIC_ROUNDABORT OFF
--SET CONCAT_NULL_YIELDS_NULL ON
--SET ANSI_NULLS ON
--SET ANSI_PADDING ON
--SET ANSI_WARNINGS ON
--COMMIT
--BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.SettlementBeneficiary
--	DROP COLUMN Name, BeneficiaryCode
--GO
--ALTER TABLE dbo.SettlementBeneficiary SET (LOCK_ESCALATION = TABLE)
--GO
--COMMIT
--select Has_Perms_By_Name(N'dbo.SettlementBeneficiary', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SettlementBeneficiary', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SettlementBeneficiary', 'Object', 'CONTROL') as Contr_Per 