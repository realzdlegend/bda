--I did this to fix omitted migrations
--23
ALTER TABLE dbo.Invoice
ADD RemitaRetrievalReference VARCHAR(50) NULL

go

--24
ALTER VIEW [dbo].[v_InvoiceView]
AS
SELECT Id, I.AgencyId, I.InvoiceNumber, InvoiceDate, I.Amount,I.[Sent], I.CreatedDate, 
I.DueDate, I.ReturnedInvoiceId, ProviderId, RevenueId, I.CurrencyId,I.RemitaRetrievalReference, LocationId, Deleted, 
[Sent] SentToFidesic, ISNULL(PaymentCount, 0) PaymentCount, ISNULL(R.AmountPaid, 0) AmountPaid, 
ISNULL(DepositOption, '') DepositOption, P.CreatedDate PaymentDate, CI.CreatedBy, CI.Name, CI.Email, CI.Phone, 
CI.CAddress, BillCount, MerchantReference, PaidScore PaymentPrediction FROM dbo.Invoice I 
INNER JOIN dbo.CustomerInvoices CI ON I.Id = CI.InvoiceId
LEFT JOIN dbo.InvoicePredictions IP ON I.Id = IP.InvoiceId
LEFT JOIN 
(SELECT InvoiceNumber, MAX(RR.PaymentId) PaymentId, COUNT(RR.PaymentId) PaymentCount, SUM(RR.Amount) AmountPaid 
FROM dbo.Remittances RR INNER JOIN dbo.Payments ON RR.PaymentId = dbo.Payments.PaymentId
GROUP BY InvoiceNumber) R
ON I.InvoiceNumber = R.InvoiceNumber
LEFT JOIN dbo.Payments P ON R.PaymentId = P.PaymentId

go

--25
CREATE TABLE [dbo].[SettlementAccount](
  [Id] [INT] IDENTITY(1,1) NOT NULL,
  [AccountName] [VARCHAR](50) NOT NULL,
  [AccountNumber] [VARCHAR](50) NOT NULL,
  [Organization] [VARCHAR](100) NULL,
  [Bank] [VARCHAR](100) NULL,
  [BankCode] [VARCHAR](50) NOT NULL,
  [RetrievalCode] [VARCHAR](50) NULL,
  [Description] [VARCHAR](100) NULL,
 CONSTRAINT [PK_BankAccount] PRIMARY KEY CLUSTERED 
(
  [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_AccountNumber] UNIQUE NONCLUSTERED 
(
  [AccountNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--26
CREATE TABLE [dbo].[RevenueBeneficiary](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[RevenueId] [INT] NOT NULL,
	[SettlementAccountId] [INT] NOT NULL,
	[SharePercentage] [FLOAT] NOT NULL,
	[DeductFeeFrom] [BIT] NOT NULL,
 CONSTRAINT [PK_RevenueBeneficiary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RevenueBeneficiary]  WITH CHECK ADD  CONSTRAINT [FK_RevenueBeneficiary_Revenue] FOREIGN KEY([RevenueId])
REFERENCES [dbo].[Revenue] ([Id])
GO

ALTER TABLE [dbo].[RevenueBeneficiary] CHECK CONSTRAINT [FK_RevenueBeneficiary_Revenue]
GO

ALTER TABLE [dbo].[RevenueBeneficiary]  WITH CHECK ADD  CONSTRAINT [FK_RevenueBeneficiary_SettlementAccount] FOREIGN KEY([SettlementAccountId])
REFERENCES [dbo].[SettlementAccount] ([Id])
GO

ALTER TABLE [dbo].[RevenueBeneficiary] CHECK CONSTRAINT [FK_RevenueBeneficiary_SettlementAccount]
GO


--27
CREATE TABLE [dbo].[RemitaLog](
	[Id] [BIGINT] IDENTITY(1,1) NOT NULL,
	[InvoiceNumber] [VARCHAR](50) NOT NULL,
	[PostedData] [VARCHAR](MAX) NOT NULL,
	[LogDate] [DATETIME] NOT NULL,
	[Response] [VARCHAR](500) NULL,
 CONSTRAINT [PK_RemitaLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



--28
CREATE TABLE [dbo].[Bank](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Name] [VARCHAR](100) NULL,
	[BankCode] [VARCHAR](50) NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



--29
ALTER TABLE [dbo].[SettlementAccount]
DROP COLUMN Bank
GO

ALTER TABLE [dbo].[SettlementAccount]
DROP COLUMN BankCode
GO

ALTER TABLE[dbo].[SettlementAccount]
ADD BankId INT NOT NULL



ALTER TABLE [dbo].[SettlementAccount]  WITH CHECK ADD  CONSTRAINT [FK_SettlementAccount_Bank] FOREIGN KEY([BankId])
REFERENCES [dbo].[Bank] ([Id])
GO

ALTER TABLE [dbo].[SettlementAccount] CHECK CONSTRAINT [FK_SettlementAccount_Bank]
GO

--30
ALTER TABLE dbo.SettlementAccount
DROP CONSTRAINT IX_AccountNumber
GO
--31

ALTER TABLE dbo.SettlementAccount
ADD CONSTRAINT uc_AccountNumber UNIQUE (AccountNumber)

GO




--continue 42

ALTER view [v_InvoiceView]
as 
SELECT I.Id, I.AgencyId, I.InvoiceNumber, InvoiceDate, I.Amount,I.[Sent], I.CreatedDate, 
I.DueDate, I.ReturnedInvoiceId, I.ProviderId, SI.ParentServiceProviderId as ProviderParentId, RevenueId, I.CurrencyId,I.RemitaRetrievalReference,
 i.LocationId, li.ParentLocationId as LocationParentId, Deleted, 
[Sent] SentToFidesic, ISNULL(PaymentCount, 0) PaymentCount, ISNULL(R.AmountPaid, 0) AmountPaid, 
ISNULL(DepositOption, '') DepositOption, P.CreatedDate PaymentDate, CI.CreatedBy, CI.Name, CI.Email, CI.Phone, 
CI.CAddress, BillCount, MerchantReference, PaidScore PaymentPrediction FROM Invoice I 
INNER JOIN CustomerInvoices CI ON I.Id = CI.InvoiceId
LEFT JOIN InvoicePredictions IP ON I.Id = IP.InvoiceId
left join ServiceProvider SI on i.ProviderId = SI.Id
left join Locations LI on i.LocationId = LI.Id
LEFT JOIN 
(SELECT InvoiceNumber, MAX(RR.PaymentId) PaymentId, COUNT(RR.PaymentId) PaymentCount, SUM(RR.Amount) AmountPaid 
FROM Remittances RR INNER JOIN Payments ON RR.PaymentId = Payments.PaymentId  
GROUP BY InvoiceNumber) R
ON I.InvoiceNumber = R.InvoiceNumber
LEFT JOIN Payments P ON R.PaymentId = P.PaymentId
GO


ALTER VIEW [v_PaymentView]

AS

SELECT  R.InvoiceNumber, R.Amount, I.Id InvoiceId, P.PaymentId, TotalAmount, 
DepositOption, P.CreatedDate PaymentDate, P.DueDate, ProcessingDate, PaymentOption, 
ReferenceNumber, MerchantReference, InvoiceDate, RetrieveDate,
CASE WHEN TotalAmount >= I.Amount THEN 'Paid' ELSE 'Partially Paid' END PaymentStatus, 
I.Amount InvoiceAmount, ProviderId, RevenueId, I.CurrencyId, CUR.Name Currency, 
CUR.Code CurrencyCode, Deleted, I.LocationId, OA.Name Agency, OA.Id AgencyId, 
OA.Code AgencyCode, OC.Name Customer, CI.Name CustomerName, OC.Code CustomerAIN, 
C.Id CustomerId, REV.Name Revenue, REV.Code RevenueCode, R.Id, 
L.Code LocationCode, L.LocationAddress, S.Name StateName, C.ParentServiceProviderId as ProviderParentId,
L.ParentLocationId as LocationParentId
FROM Remittances R
INNER JOIN (SELECT InvoiceNumber, SUM(Amount) TotalAmount FROM Remittances
GROUP BY InvoiceNumber) TR ON R.InvoiceNumber = TR.InvoiceNumber
INNER JOIN Payments P ON R.PaymentId = P.PaymentId
INNER JOIN Invoice I ON  I.InvoiceNumber = R.InvoiceNumber
INNER JOIN CustomerInvoices CI ON I.Id = CI.InvoiceId
INNER JOIN Revenue REV ON REV.Id = I.RevenueId
INNER JOIN Agency A ON I.AgencyId = A.Id
INNER JOIN Organizations OA ON OA.Id = A.Id
INNER JOIN ServiceProvider C ON C.Id = I.ProviderId 
INNER JOIN Organizations OC ON OC.Id = C.Id
INNER JOIN Currencies CUR ON CUR.Id = I.CurrencyId
INNER JOIN Locations L ON I.LocationId = L.Id 
INNER JOIN States S ON L.StateId = S.Id


GO


 