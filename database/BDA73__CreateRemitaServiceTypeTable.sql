/*
   27 November 201714:13:25
   User: 
   Server: DESKTOP-ISQFERH
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.RemitaServiceType
	(
	Id int NOT NULL IDENTITY (1, 1),
	ServiceTypeId int NULL,
	ServiceTypeName nchar(50) NULL,
	AgencyId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.RemitaServiceType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.RemitaServiceType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.RemitaServiceType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.RemitaServiceType', 'Object', 'CONTROL') as Contr_Per 