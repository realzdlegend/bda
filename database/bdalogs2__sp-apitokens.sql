
/****** Object:  StoredProcedure [dbo].[sp_InsertApiCredentials]    Script Date: 06/02/2016 15:33:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Oluwatobi Akinseye>
-- Create date: <Create Date,,01-Jun-2016>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertApiToken] @name NVARCHAR(100),@apicode NVARCHAR(100), @apikey NVARCHAR(100), @datecreated DATETIME
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.ApiCredentials
        ( Name ,
          ApiCode ,
          ApiKey ,
          DateCreated
        )
VALUES  ( @name , -- Name - nvarchar(50)
          @apicode , -- ApiCode - nvarchar(50)
          @apikey , -- ApiKey - nvarchar(50)
          @datecreated  -- DateCreated - datetime
        )
END
