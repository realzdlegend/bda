

--Script Date: 05/04/2016 15:45:33
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (select * from sysobjects where name = 'TG_Remittances')
  DROP TRIGGER TG_Remittances
  PRINT 'dropped remmittance trigger'
go

 --Trigger to build the audit message using the modified data
   PRINT 'Re-creating remmittance trigger'
go

	CREATE TRIGGER [dbo].[TG_Remittances]
	   ON [dbo].[Remittances]
	   AFTER INSERT, UPDATE, DELETE
	AS 
	BEGIN
		SET NOCOUNT ON

		DECLARE @data XML, @table sysname
		
		-- The table name is kept static to avoid any performance overhead associated with reading from
		-- the metadata, however if overhead is not a concern, this can be done by reading from sys.triggers
		SELECT @table = N'Remmittances'

		-- Build audit message
		SET @data = 
		'<AUDIT_MESSAGE>
			<SERVER_NAME>' + @@SERVERNAME +'</SERVER_NAME>
			<DB_NAME>' + DB_NAME() +'</DB_NAME>
			<TABLE>'+ @table + '</TABLE>
			<AUDIT_USER>' + SUSER_NAME() + '</AUDIT_USER>
			<AUDIT_DATETIME>' + CAST(GETDATE() AS VARCHAR) + '</AUDIT_DATETIME>
			<INSERTED>' + ISNULL((SELECT * FROM INSERTED FOR XML RAW), '') + '</INSERTED>
			<DELETED>'  + ISNULL((SELECT * FROM DELETED FOR XML RAW), '') + '</DELETED>
		</AUDIT_MESSAGE>';

		EXEC USP_SEND_AUDIT_MESSAGE @data
	END


GO


