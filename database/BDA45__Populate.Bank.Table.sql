USE [bda]
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 23/06/2017 12:57:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
 
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Bank] ON 

/* INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (1, N'ACCESS BANK PLC ', N'044') */

INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (5, N'ECOBANK NIGERIA PLC', N'050')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (6, N'FIDELITY BANK PLC', N'070')
/* INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (7, N'FIRST BANK OF NIGERIA PLC ', N'011') */
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (8, N'FIRST CITY MONUMENT BANK PLC', N'214')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (9, N'GUARANTY TRUST BANK PLC', N'058')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (10, N'STANBIC IBTC BANK PLC ', N'221')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (11, N'CITI BANK', N'023')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (12, N'SKYE BANK PLC', N'076')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (13, N'ENTERPRISE BANK PLC', N'084')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (14, N'STANDARD CHARTERED BANK PLC', N'068')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (15, N'STERLING BANK PLC', N'232')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (16, N'UNION BANK OF NIGERIA PLC', N'032')
/* INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (17, N'UNITED BANK FOR AFRICA PLC', N'033') */
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (18, N'UNITY BANK PLC', N'215')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (19, N'WEMA BANK PLC', N'035')
/* INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (20, N'ZENITH INTERNATIONAL BANK PLC', N'057') */
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (21, N'HERITAGE BANK', N'030')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (22, N'JAIZ BANK', N'301')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (23, N'MAIN STREET BANK NIGERIA PLC ', N'014')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (24, N'KEYSTONE BANK PLC ', N'082')
INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (25, N'DIAMOND BANK PLC', N'063')
SET IDENTITY_INSERT [dbo].[Bank] OFF
