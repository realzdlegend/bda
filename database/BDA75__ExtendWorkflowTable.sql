/*
   12 December 201713:10:32
   User: 
   Server: DESKTOP-ISQFERH
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Workflows
	DROP CONSTRAINT FK_Workflows_Organization
GO
ALTER TABLE dbo.Organizations SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Organizations', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Organizations', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Organizations', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Workflows
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	OrganizationId int NOT NULL,
	Process nvarchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Workflows SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Workflows ON
GO
IF EXISTS(SELECT * FROM dbo.Workflows)
	 EXEC('INSERT INTO dbo.Tmp_Workflows (Id, Name, OrganizationId, Process)
		SELECT Id, Name, OrganizationId, CONVERT(nvarchar(50), Process) FROM dbo.Workflows WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Workflows OFF
GO
ALTER TABLE dbo.WorkflowUsers
	DROP CONSTRAINT FK_WorkflowUsers_Workflows
GO
DROP TABLE dbo.Workflows
GO
EXECUTE sp_rename N'dbo.Tmp_Workflows', N'Workflows', 'OBJECT' 
GO
ALTER TABLE dbo.Workflows ADD CONSTRAINT
	PK_Workflows PRIMARY KEY NONCLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE CLUSTERED INDEX IX_Workflows ON dbo.Workflows
	(
	OrganizationId,
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Workflows ADD CONSTRAINT
	FK_Workflows_Organization FOREIGN KEY
	(
	OrganizationId
	) REFERENCES dbo.Organizations
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Workflows', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Workflows', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Workflows', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.WorkflowUsers ADD CONSTRAINT
	FK_WorkflowUsers_Workflows FOREIGN KEY
	(
	WorkflowId
	) REFERENCES dbo.Workflows
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.WorkflowUsers SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.WorkflowUsers', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.WorkflowUsers', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.WorkflowUsers', 'Object', 'CONTROL') as Contr_Per 