/*
   29 January 201811:12:47
   User: 
   Server: DESKTOP-ISQFERH
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Bills
	DROP CONSTRAINT FK_Bills_Currencies
GO
ALTER TABLE dbo.Currencies SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Currencies', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Currencies', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Currencies', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Bills
	DROP CONSTRAINT FK_Bills_MasterList
GO
ALTER TABLE dbo.MasterList SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MasterList', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MasterList', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MasterList', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Bills
	DROP CONSTRAINT FK_Bills_Locations
GO
ALTER TABLE dbo.Locations SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Locations', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Locations', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Locations', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Bills
	(
	Id bigint NOT NULL,
	Amount decimal(18, 2) NOT NULL,
	LastEditDate datetime NOT NULL,
	Description varchar(500) NOT NULL,
	TransactionDate datetime NOT NULL,
	CurrencyCode int NOT NULL,
	LocationId int NOT NULL,
	Status int NOT NULL,
	Quantity decimal(18, 2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Bills SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Bills)
	 EXEC('INSERT INTO dbo.Tmp_Bills (Id, Amount, LastEditDate, Description, TransactionDate, CurrencyCode, LocationId, Status, Quantity)
		SELECT Id, Amount, LastEditDate, Description, TransactionDate, CurrencyCode, LocationId, Status, CONVERT(decimal(18, 2), Quantity) FROM dbo.Bills WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.BillDetails
	DROP CONSTRAINT FK_BillDetails_Bills
GO
ALTER TABLE dbo.ActiveDisputes
	DROP CONSTRAINT FK_ActiveDisputes_Bills
GO
ALTER TABLE dbo.BillHistory
	DROP CONSTRAINT FK_BillHistory_Bills
GO
ALTER TABLE dbo.InvoiceItem
	DROP CONSTRAINT FK_InvoiceItem_Bills
GO
DROP TABLE dbo.Bills
GO
EXECUTE sp_rename N'dbo.Tmp_Bills', N'Bills', 'OBJECT' 
GO
ALTER TABLE dbo.Bills ADD CONSTRAINT
	PK_Revenue PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Bills ADD CONSTRAINT
	FK_Bills_Locations FOREIGN KEY
	(
	LocationId
	) REFERENCES dbo.Locations
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Bills ADD CONSTRAINT
	FK_Bills_MasterList FOREIGN KEY
	(
	Id
	) REFERENCES dbo.MasterList
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Bills ADD CONSTRAINT
	FK_Bills_Currencies FOREIGN KEY
	(
	CurrencyCode
	) REFERENCES dbo.Currencies
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Bills', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Bills', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Bills', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.InvoiceItem ADD CONSTRAINT
	FK_InvoiceItem_Bills FOREIGN KEY
	(
	BillId
	) REFERENCES dbo.Bills
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.InvoiceItem SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.InvoiceItem', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.InvoiceItem', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.InvoiceItem', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.BillHistory ADD CONSTRAINT
	FK_BillHistory_Bills FOREIGN KEY
	(
	BillId
	) REFERENCES dbo.Bills
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.BillHistory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.BillHistory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.BillHistory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.BillHistory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ActiveDisputes ADD CONSTRAINT
	FK_ActiveDisputes_Bills FOREIGN KEY
	(
	BillId
	) REFERENCES dbo.Bills
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActiveDisputes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ActiveDisputes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ActiveDisputes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ActiveDisputes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.BillDetails ADD CONSTRAINT
	FK_BillDetails_Bills FOREIGN KEY
	(
	BillId
	) REFERENCES dbo.Bills
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.BillDetails SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.BillDetails', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.BillDetails', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.BillDetails', 'Object', 'CONTROL') as Contr_Per 