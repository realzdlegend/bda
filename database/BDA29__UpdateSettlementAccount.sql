


ALTER TABLE [dbo].[SettlementAccount]
DROP COLUMN Bank
GO

ALTER TABLE [dbo].[SettlementAccount]
DROP COLUMN BankCode
GO

ALTER TABLE[dbo].[SettlementAccount]
ADD BankId INT NOT NULL



ALTER TABLE [dbo].[SettlementAccount]  WITH CHECK ADD  CONSTRAINT [FK_SettlementAccount_Bank] FOREIGN KEY([BankId])
REFERENCES [dbo].[Bank] ([Id])
GO

ALTER TABLE [dbo].[SettlementAccount] CHECK CONSTRAINT [FK_SettlementAccount_Bank]
GO
