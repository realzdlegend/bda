USE [bda]
GO
/****** Object:  StoredProcedure [dbo].[edb_InsertBillDetail]    Script Date: 29/01/2018 11:58:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[edb_InsertBillDetail]

@Amount DECIMAL(18, 2),
@BillId INT,
@Description VARCHAR(100),
@LineNumber INT,
@Quantity DECIMAL(18, 2) 

AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 
 INSERT INTO BillDetails ( [Description], Amount, EditedAmount, BillId, LineNumber,Quantity )
 VALUES  (@Description, @Amount, @Amount, @BillId, @LineNumber,@Quantity)
 
 SELECT @@IDENTITY
END
