USE [bda]
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 01/11/2017 12:57:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
 
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Bank] ON 

 INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (26, N'ACCESS BANK PLC ', N'044')
 
 INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (27, N'FIRST BANK OF NIGERIA PLC ', N'011') 

 INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (28, N'UNITED BANK FOR AFRICA PLC', N'033') 
 INSERT [dbo].[Bank] ([Id], [Name], [BankCode]) VALUES (29, N'ZENITH INTERNATIONAL BANK PLC', N'057') 
SET IDENTITY_INSERT [dbo].[Bank] OFF
