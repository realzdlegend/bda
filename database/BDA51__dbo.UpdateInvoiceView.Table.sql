
/****** Object:  View [dbo].[v_InvoiceView]    Script Date: 05/07/2017 18:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[v_InvoiceView]
as 
SELECT I.Id, I.AgencyId, I.InvoiceNumber, InvoiceDate, I.Amount,I.[Sent], I.CreatedDate, 
I.DueDate, I.ReturnedInvoiceId, I.ProviderId, SI.ParentServiceProviderId as ProviderParentId, RevenueId, I.CurrencyId,I.RemitaRetrievalReference,
 i.LocationId, li.ParentLocationId as LocationParentId, Deleted, 
[Sent] SentToFidesic, ISNULL(PaymentCount, 0) PaymentCount, ISNULL(R.AmountPaid, 0) AmountPaid, 
ISNULL(DepositOption, '') DepositOption, P.CreatedDate PaymentDate, CI.CreatedBy, CI.Name, CI.Email, CI.Phone, 
CI.CAddress, BillCount, MerchantReference, PaidScore PaymentPrediction FROM Invoice I 
INNER JOIN CustomerInvoices CI ON I.Id = CI.InvoiceId
LEFT JOIN InvoicePredictions IP ON I.Id = IP.InvoiceId
left join ServiceProvider SI on i.ProviderId = SI.Id
left join Locations LI on i.LocationId = LI.Id
LEFT JOIN 
(SELECT InvoiceNumber, MAX(RR.PaymentId) PaymentId, COUNT(RR.PaymentId) PaymentCount, SUM(RR.Amount) AmountPaid 
FROM Remittances RR INNER JOIN Payments ON RR.PaymentId = Payments.PaymentId  
GROUP BY InvoiceNumber) R
ON I.InvoiceNumber = R.InvoiceNumber
LEFT JOIN Payments P ON R.PaymentId = P.PaymentId









GO


