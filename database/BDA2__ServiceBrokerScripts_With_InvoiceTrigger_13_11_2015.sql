 --ALTER DATABASE [bda] SET NEW_BROKER WITH ROLLBACK IMMEDIATE; 
 --go

 ---- Create Message Type of the Audit Message
 --CREATE MESSAGE TYPE AUDIT
 --	VALIDATION = WELL_FORMED_XML
 --GO

 ---- Create Message Type of the Audit Message's Response
 --CREATE MESSAGE TYPE AUDIT_RESPONSE
 --	VALIDATION = NONE	
 --GO

 ---- Create Contract for the Conversation
 --CREATE CONTRACT AUDIT_CONTRACT
 --	(AUDIT SENT BY INITIATOR, 
 --	 AUDIT_RESPONSE SENT BY TARGET )
 --GO
 ---- Stored Procedure for ending the Conversation
 --CREATE PROCEDURE USP_END_AUDIT_CONVERSATION
 --AS 
 --BEGIN
 --    SET NOCOUNT ON

 --    DECLARE @handle UNIQUEIDENTIFIER  
 --    WAITFOR	(RECEIVE TOP (1) 
 --				@handle = CONVERSATION_HANDLE
 --			 FROM AUDIT_RESPONSES -- Queue which stores the responses
 --			)
 --			,TIMEOUT 10000; -- 10 seconds
		
 --    IF @handle IS NULL 
 --        RETURN	-- In case of a Timeout

 --    END CONVERSATION @handle;
 --END
 --GO

 ---- Queue to store the Audit Messages
 --CREATE QUEUE AUDIT_MESSAGES 

 --	WITH STATUS = ON, 
 --	RETENTION = OFF  
 --	ON [PRIMARY] 
 --GO

 ---- Queue to store Responses
 --CREATE QUEUE AUDIT_RESPONSES
 --	WITH 
 --	STATUS = ON,
 --	RETENTION = OFF,
 --	ACTIVATION (
 --	STATUS = ON,
 --	PROCEDURE_NAME = USP_END_AUDIT_CONVERSATION,
 --	MAX_QUEUE_READERS = 10,  -- Up to 10 instances of USP_END_AUDIT_CONVERSATION can be kicked off 
 --							 --	depending on the load (Internal Activation)
 --	EXECUTE AS SELF) -- The stored procedure will be executed as the current user
 --	ON [PRIMARY]
 --GO

 ---- Initiator Service
 --CREATE SERVICE INITIATOR_SERVICE  
 --	ON QUEUE AUDIT_RESPONSES
 --	([AUDIT_CONTRACT]) -- Message Contract
 --GO

 ---- Target Service
 --CREATE SERVICE TARGET_SERVICE  
 --	ON QUEUE AUDIT_MESSAGES
 --	([AUDIT_CONTRACT]) -- Message Contract
 --GO


 ---- Stored Procedure which will be used to send the Audit Message to the Queue
 ---- Error handling has been avoided to keep the sample code brief
 --CREATE PROCEDURE USP_SEND_AUDIT_MESSAGE(@data XML)
 --AS
 --BEGIN
 --	DECLARE @handle UNIQUEIDENTIFIER;  
  
 --	BEGIN DIALOG CONVERSATION @handle  
 --	FROM SERVICE INITIATOR_SERVICE  
 --	TO SERVICE 'TARGET_SERVICE'  
 --	ON CONTRACT AUDIT_CONTRACT
 --	WITH ENCRYPTION = OFF; -- Messages are not encrypted
   
 --	SEND ON CONVERSATION @handle  
 --	MESSAGE TYPE AUDIT  
 --	(@data)  	
 --END
 --GO


 ---- Trigger to build the audit message using the modified data
 --create TRIGGER [dbo].[TG_Invoice]
 --   ON [dbo].Invoice
 --   AFTER INSERT, UPDATE, DELETE
 --AS 
 --BEGIN
 --	SET NOCOUNT ON

 --	DECLARE @data XML, @table sysname
	
 --	-- The table name is kept static to avoid any performance overhead associated with reading from
 --	-- the metadata, however if overhead is not a concern, this can be done by reading from sys.triggers
 --	SELECT @table = N'Invoice'

 --	-- Build audit message
 --	SET @data = 
 --	'<AUDIT_MESSAGE>
 --	<DB_NAME>' + DB_NAME() +'</DB_NAME>
 --		<TABLE>'+ @table + '</TABLE>
 --		<AUDIT_USER>' + SUSER_NAME() + '</AUDIT_USER>
 --		<AUDIT_DATETIME>' + CAST(GETDATE() AS VARCHAR) + '</AUDIT_DATETIME>
 --		<INSERTED>' + ISNULL((SELECT * FROM INSERTED FOR XML RAW), '') + '</INSERTED>
 --		<DELETED>'  + ISNULL((SELECT * FROM DELETED FOR XML RAW), '') + '</DELETED>
 --	</AUDIT_MESSAGE>';

 --	EXEC USP_SEND_AUDIT_MESSAGE @data
 --END
 --GO

 --CREATE PROCEDURE GetPrimaryKeyColumn
 --@table_schema NVARCHAR(500), @table_name NVARCHAR(4000)

 --AS
 --BEGIN
 --SELECT column_name
 --FROM INFORMATION_SCHEMA.COLUMNS
 --where COLUMNPROPERTY (OBJECT_ID(Table_Name),Column_Name,'IsIdentity') = 1
 -- and table_schema = @table_schema and table_name = @table_name
 
 -- END
 -- go
 
 --------Set Compatibility level-----------
 --ALTER DATABASE [bda]
 --SET COMPATIBILITY_LEVEL = 100 
 --go


 --ALTER QUEUE AUDIT_MESSAGES WITH STATUS = ON;
 --go
