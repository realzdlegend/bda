
CREATE TABLE [dbo].[InvoiceRevenueGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[RevenueGroupId] [int] NOT NULL,
 CONSTRAINT [PK_InvoiceRevenueGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_InvoiceId] UNIQUE NONCLUSTERED 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InvoiceRevenueGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_InvoiceRevenueGroup_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO

ALTER TABLE [dbo].[InvoiceRevenueGroup] CHECK CONSTRAINT [FK_InvoiceRevenueGroup_Invoice]
GO

ALTER TABLE [dbo].[InvoiceRevenueGroup]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceRevenueGroup_RevenueGroup] FOREIGN KEY([RevenueGroupId])
REFERENCES [dbo].[AgencyRevenueGroup] ([Id])
GO

ALTER TABLE [dbo].[InvoiceRevenueGroup] CHECK CONSTRAINT [FK_InvoiceRevenueGroup_RevenueGroup]
GO


