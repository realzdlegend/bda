/*
   Thursday, February 18, 201612:43:09 PM
   User: 
   Server: .
   Database: bda
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Revenue ADD
	RevenueGroupId int NULL
GO
ALTER TABLE dbo.Revenue SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Revenue', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Revenue', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Revenue', 'Object', 'CONTROL') as Contr_Per 