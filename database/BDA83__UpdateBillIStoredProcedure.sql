USE [bda]
GO
/****** Object:  StoredProcedure [dbo].[edb_InsertBill]    Script Date: 29/01/2018 11:58:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[edb_InsertBill]

@MainId INT,
@Amount DECIMAL(18, 2),
@BillDate DATETIME,
@Description VARCHAR(100),
@TransactionDate DATETIME,
@Currency INT,
@Location VARCHAR(50),
@Status INT,
@Quantity DECIMAL(18, 2)



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO Bills ( Id, Amount, LastEditDate, [Description], 
	                     TransactionDate, CurrencyCode, LocationId, [Status], Quantity )
	VALUES  ( @MainId, @Amount, @BillDate, @Description, 
				@TransactionDate, @Currency, @Location, @Status, @Quantity)
END
