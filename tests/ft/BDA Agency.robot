*** Settings ***
Library           Selenium2Library

*** Variables ***
${LoginUsername id}    id=ctl00_ContentPlaceHolder1_MainContent_LoginUser_UserName
${LoginPassword id}    id=ctl00_ContentPlaceHolder1_MainContent_LoginUser_Password
${LoginButton id}    id=ctl00_ContentPlaceHolder1_MainContent_LoginUser_LoginButton
${LogOut id}      id=ctl00_HeadLoginView_HeadLoginStatus
${browser}        Firefox

*** Test Cases ***
Valid Login
    [Documentation]    *TestCase*
    ...    1. Open url in Firefox Browser
    ...    2. Enter "henry@splasherstech.com" in the username field
    ...    3. Enter "password1" in the password field
    ...    4. Click on the Login button
    ...    5. Verify that logout button is displayed
    Open Browser    http://64.91.230.133:8006/Login.aspx    ${browser}
    Maximize Browser Window
    Wait until element is visible    ${LoginButton id}    30 s
    Input Text    ${LoginUsername id}    henry.mba@avitechng.com
    Input Text    ${LoginPassword id}    password1
    Click Element    ${LoginButton id}
    Page should contain element    ${LogOut id}

Search Invoice By Invoice Number
    [Documentation]    *Search Invoice By Invoice Number*
    ...    1. Click Invoices
    ...    2. Enter a date range and enter an invoice number in the Invoice Number textbox
    ...    3. Click search
    ...    4. Click on the invoice number to view its details
    Click Element    //*[@id="main-menu"]/li[1]/a
    Wait Until Element is Visible    id=ctl00_MainContent_btnSearch    30 s
    Click Element    id=ctl00_MainContent_txtStartDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[3]/a
    Click Element    id=ctl00_MainContent_txtEndDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a
    Input Text    id=ctl00_MainContent_txtInvoice    9100010012213
    Click Element    id=ctl00_MainContent_btnSearch
    Set Browser Implicit Wait    30 sec
    Table should Contain    ctl00_MainContent_gvInvoices    Link=9100010012213
    Click Link    Link= 9100010012213

Create Invoice
    [Documentation]    *Create Invoice*
    ...    1. Click on Create Invoices
    ...    2. Enter invoice details
    ...    3. Select add item
    ...    4. Click the Create Invoice button
    Click Element    //*[@id="main-menu"]/li[2]/a/span
    Wait Until Element is visible    id=ctl00_MainContent_btnAddInvoice    30 s
    Select From List By Value    id=ctl00_MainContent_ddProviders    897
    Set Browser Implicit Wait    30 s
    Select From list By Value    id=ctl00_MainContent_ddRevenue    14
    Set Browser Implicit Wait    30 s
    Select From List By value    id=ctl00_MainContent_ddLocation    9
    Input Text    id=ctl00_MainContent_txtDescription    Item
    Input Text    id=ctl00_MainContent_txtAmount    1900
    Click Element    id=ctl00_MainContent_btnAdd
    Wait Until Element is Visible    //*[@id="ctl00_MainContent_gvItems"]/tbody/tr[2]/td[5]/a
    Click Element    id=ctl00_MainContent_btnAddInvoice
    Wait until Element is Visible    id=ctl00_MainContent_btnPrintInvoice    20 min

Print Invoice
    [Documentation]    *Print Invoice*
    ...    1. Click on the Print Invoice Button
    Click Element    id=ctl00_MainContent_btnPrintInvoice
    Set Browser Implicit Wait    2 min
    Get Alert message    dismiss=true

Void Invoice
    [Documentation]    *Void Invoice*
    ...    1. After creating an invoice, click Void Invoice
    ...    2. Enter a reason for the void
    ...    3. Click Void Invoice
    Click Element    id=ctl00_MainContent_voidInvoice
    Wait until Element is visible    id=ctl00_MainContent_voidInvoice    10 s
    Input Text    id=ctl00_MainContent_txtReason    Test
    Click Element    id=ctl00_MainContent_btnVoid
    Wait Until Element is Visible    id=ctl00_MainContent_lbVoid    5 min

Change Password
    [Documentation]    *Change Password*
    ...    1. Click on the user's profile
    ...    2. Click Change Password
    ...    3. Enter current and new password
    ...    4. Confirm new password and save
    Click Element    id=ctl00_HeadLoginView_LabelName
    Set Browser implicit Wait    10 sec
    Click Element    id=ctl00_MainContent_HyperLink1
    Set Browser Implicit Wait    10 sec
    Input Text    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_CurrentPassword    Password1
    Input Text    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_NewPassword    password1
    Input Text    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_ConfirmNewPassword    password1
    Click Element    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_Button2
    Wait Until Element is Visible    id=ctl00_MainContent_ChangeUserPassword_SuccessContainerID_Continue

Add Location
    [Documentation]    *Add Location*
    ...    1. Navigate to settings and click Location
    ...    2. Click Add New Location
    ...    3. Enter Code, Name
    ...    4. Select Type
    ...    5. Fill other details and save location.
    Click Element    //*[@id="main-menu"]/li[7]/a/span
    Set Browser implicit Wait    10 sec
    Click Element    //*[@id="main-menu"]/li[7]/ul/li[2]/a/span
    Set Browser implicit Wait    10 sec
    Click Element    id=ctl00_MainContent_HyperLink1
    Set Browser Implicit Wait    10 sec
    Input Text    id=ctl00_MainContent_txtCode    ABJ2
    Input text    id=ctl00_MainContent_txtAddress    Abuja, Nigeria
    Select From list by Value    id=ctl00_MainContent_ddType    Office
    Select Checkbox    id=ctl00_MainContent_CheckBox1
    Select from list by Value    id=ctl00_MainContent_ddState    1
    Click Element    id=ctl00_MainContent_Button1
    Wait until Element is Visible    id=ctl00_MainContent_lbStatus    30 sec

Create User
    [Documentation]    *Create User*
    ...    1. Navigate to Profile and Click on Manage Users
    ...    2. Click on Create New User
    ...    3. Add user's details
    ...    4. Click Save
    Click Element    //*[@id="main-menu"]/li[4]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[4]/ul/li[1]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_lnkAddUsers
    Set Browser Implicit Wait    30 sec
    Input Text    id=ctl00_MainContent_txtFirstName    Mujidah
    Input Text    id=ctl00_MainContent_txtLastName    Mujidah
    Input Text    id=ctl00_MainContent_txtEmail    mujidt2@splash.com
    Input Text    id=ctl00_MainContent_txtPassword    Password1
    Input Text    id=ctl00_MainContent_txtPasswordConfirm    Password1
    Input Text    id=ctl00_MainContent_txtPhone    08066666666
    Select Checkbox    id=ctl00_MainContent_chkActive
    Select Checkbox    id=chkPermissionList_6
    Select Checkbox    id=chRevenues_31
    Select Checkbox    id=chRegions_1
    Select Checkbox    id=chLocations_5
    Set Browser Implicit Wait    15 sec
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    1 min
    Page should Contain Element    id=ctl00_MainContent_lbUpdate

Search Invoice By Date
    [Documentation]    *Search Invoice By Date*
    ...    1. Click Invoices
    ...    2. Select Start and End dates
    ...    3. Click Search
    Click Element    //*[@id="main-menu"]/li[1]/a
    Wait Until Element is Visible    id=ctl00_MainContent_btnSearch    5 min
    Click Element    id=ctl00_MainContent_txtStartDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[3]/a
    Click Element    id=ctl00_MainContent_txtEndDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a
    Click Element    id=ctl00_MainContent_btnSearch
    Set Browser Implicit Wait    30 sec
    Page should Contain Element    id=ctl00_MainContent_gvInvoices

Recover Password
    [Documentation]    *Recover Password*
    ...    1. Open url and Click Recover password
    ...    2. Enter Valid email address
    ...    3. Click Send
    Open Browser    http://64.91.230.133:8006/Login.aspx    ${browser}
    Maximize Browser Window
    Wait until element is visible    ${LoginButton id}    30 s
    Click Element    //*[@id="login-block"]/div/div/div/div[6]/a[1]
    Set Browser Implicit Wait    30 s
    Input Text    id=ctl00_MainContent_txtEmail    mujidat@splasherstech.com
    Click Element    id=ctl00_MainContent_Button1
    Set browser Implicit Wait    30 s
    Page should contain element    id=ctl00_MainContent_Label1

Edit User
    Click Element    //*[@id="main-menu"]/li[4]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[4]/ul/li[1]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="ctl00_MainContent_gvListPersons"]/tbody/tr[5]/td[5]/a
    Set Browser Implicit Wait    30 sec
    Select Checkbox    id=chkPermissionList_0
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain element    id=ctl00_MainContent_lbUpdate

Edit Location
    Click Element    //*[@id="main-menu"]/li[7]/a/span
    Set Browser implicit Wait    10 sec
    Click Element    //*[@id="main-menu"]/li[7]/ul/li[2]/a/span
    Set Browser implicit Wait    10 sec
    Click Element    //*[@id="ctl00_MainContent_GridView1"]/tbody/tr[6]/td[1]/a
    Clear element text    id=ctl00_MainContent_txtAddress
    Input Text    id=ctl00_MainContent_txtAddress    New code
    Click Element    id=ctl00_MainContent_Button1
    Set Browser Implicit Wait    30 sec
    Page should contain element    id=ctl00_MainContent_lbStatus

Add Revenue group
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[6]/ul/li[2]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_btnAddGroup
    Set Browser Implicit Wait    10 sec
    Input Text    id=ctl00_MainContent_txtGroup    Group11
    Input Text    id=ctl00_MainContent_txtName    Name
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain element    id=ctl00_MainContent_Label1

Edit RevenueGroup
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[6]/ul/li[2]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_GridView1_ctl04_btnEdit
    Set browser implicit wait    30 sec
    Clear element text    id=ctl00_MainContent_GridView1_ctl04_txtRev
    Input text    id=ctl00_MainContent_GridView1_ctl04_txtRev    Test GroupOne
    Click Element    id=ctl00_MainContent_GridView1_ctl04_btnUpdate
    Set Browser Implicit Wait    10 sec
    Get Alert Message    dismiss=true

Create Customer
    Click Element    //*[@id="main-menu"]/li[4]/a/span
    Set Browser Implicit wait    10 sec
    Click Element    //*[@id="main-menu"]/li[4]/ul/li[2]/a/span
    Set Browser implicit wait    30 sec
    Click Element    id=ctl00_MainContent_HyperLink1
    Set Browser Implicit Wait    10 sec
    Select From List by Value    id=ctl00_MainContent_ddlServiceCategory    02
    Input Text    id=ctl00_MainContent_txtName    Emjay Company
    Select From List by Value    id=ctl00_MainContent_ddlType    1
    Input Text    id=ctl00_MainContent_txtAddress    Lagos
    Input Text    id=ctl00_MainContent_txtRCNo    0113445
    Click Element    id=ctl00_MainContent_txtIncDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[3]/a
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl02_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl02_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl02_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl03_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl03_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl03_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl04_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl04_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl04_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl05_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl05_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl05_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl06_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl06_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl06_txtEmail    mj@spl.com
    Select Checkbox    id=ctl00_MainContent_chklAgencies_5
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should Contain Element    id=ctl00_MainContent_lbSaved

Edit Revenue
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="ctl00_MainContent_gvRevenue"]/tbody/tr[4]/td[5]/a
    Set Browser implicit Wait    30 sec
    Select from list by value    id=ctl00_MainContent_ddType    1
    Select checkbox    id=ctl00_MainContent_chksCurrency_1
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain element
