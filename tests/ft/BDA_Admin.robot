*** Settings ***
Library           Selenium2Library

*** Variables ***
${LoginUsername id}    id=ctl00_ContentPlaceHolder1_MainContent_LoginUser_UserName
${LoginPassword id}    id=ctl00_ContentPlaceHolder1_MainContent_LoginUser_Password
${LoginButton id}    id=ctl00_ContentPlaceHolder1_MainContent_LoginUser_LoginButton
${LogOut id}      id=ctl00_HeadLoginView_HeadLoginStatus
${browser}        firefox

*** Test Cases ***
Valid Login
    [Documentation]    *TestCase*
    ...    1. Open url in Firefox Browser
    ...    2. Enter "henry@splasherstech.com" in the username field
    ...    3. Enter "password1" in the password field
    ...    4. Click on the Login button
    ...    5. Verify that logout button is displayed
    Open Browser    http://64.91.230.133:8006/Login.aspx    ${browser}
    Maximize Browser Window
    Wait until element is visible    ${LoginButton id}    30 s
    Input Text    ${LoginUsername id}    tosin.salami@avitechng.com
    Input Text    ${LoginPassword id}    Password2
    Click Element    ${LoginButton id}
    Page should contain element    ${LogOut id}

Change Password
    [Documentation]    *Change Password*
    ...    1. Click on the user's profile
    ...    2. Click Change Password
    ...    3. Enter current and new password
    ...    4. Confirm new password and save
    Click Element    id=ctl00_HeadLoginView_LabelName
    Set Browser implicit Wait    10 sec
    Click Element    id=ctl00_MainContent_HyperLink1
    Set Browser Implicit Wait    10 sec
    Input Text    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_CurrentPassword    Password1
    Input Text    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_NewPassword    Password2
    Input Text    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_ConfirmNewPassword    Password2
    Click Element    id=ctl00_MainContent_ChangeUserPassword_ChangePasswordContainerID_Button2
    Wait Until Element is Visible    id=ctl00_MainContent_ChangeUserPassword_SuccessContainerID_Continue

Create User
    [Documentation]    *Create User*
    ...    1. Navigate to Profile and Click on Manage Users
    ...    2. Click on Create New User
    ...    3. Add user's details
    ...    4. Click Save
    Click Element    //*[@id="main-menu"]/li[3]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_lnkAddUsers
    Set Browser Implicit Wait    30 sec
    Input Text    id=ctl00_MainContent_txtFirstName    Mujidah
    Input Text    id=ctl00_MainContent_txtLastName    Mujidah
    Input Text    id=ctl00_MainContent_txtEmail    emjay@splasher.com
    Input Text    id=ctl00_MainContent_txtPassword    Password1
    Input Text    id=ctl00_MainContent_txtPasswordConfirm    Password1
    Input Text    id=ctl00_MainContent_txtPhone    08066666666
    Select from List by Value    id=ctl00_MainContent_ddlOrganization    897
    Set Browser Implicit Wait    20 s
    Select Checkbox    id=ctl00_MainContent_chkActive
    Select Checkbox    id=ctl00_MainContent_chkPermissionList_0
    Select Checkbox    id=ctl00_MainContent_chkPermissionList_3
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    1 min
    Page should Contain Element    id=ctl00_MainContent_lblMsg

Create Customer
    [Documentation]    *Create Customer*
    ...    1. Click Organizations
    ...    2. Click Create customer
    ...    3. Enter Customer Details and Save
    Click Element    //*[@id="main-menu"]/li[3]/a/span
    Set browser Implicit wait    30 sec
    Click Element    //*[@id="main-menu"]/li[3]/ul/li[2]/a/span
    Set browser implicit wait    30 sec
    Click Element    id=ctl00_MainContent_HyperLink1
    Set Browser Implicit Wait    10 sec
    Select From List by Value    id=ctl00_MainContent_ddlServiceCategory    02
    Input Text    id=ctl00_MainContent_txtName    MJD Comp.
    Select From List by Value    id=ctl00_MainContent_ddlType    1
    Input Text    id=ctl00_MainContent_txtAddress    Lagos
    Input Text    id=ctl00_MainContent_txtRCNo    011344
    Click Element    id=ctl00_MainContent_txtIncDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[2]/td[3]/a
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl02_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl02_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl02_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl03_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl03_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl03_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl04_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl04_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl04_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl05_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl05_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl05_txtEmail    mj@spl.com
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl06_txtName    MJ
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl06_txtPhone    08022222222
    Input Text    id=ctl00_MainContent_gvAddOrganisation_ctl06_txtEmail    mj@spl.com
    Select Checkbox    id=ctl00_MainContent_chklAgencies_5
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should Contain Element    id=ctl00_MainContent_lbSaved

Add Revenue
    [Documentation]    *Add Revenue*
    ...    1. Navigate to Revenues and Click Revenue Settings
    ...    2. Click New Revenue
    ...    3. Enter necessary details
    ...    4. Click Save
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_lnkNew
    Set Browser Implicit Wait    10 sec
    Input Text    id=ctl00_MainContent_txtCode    IBD2
    Input Text    id=ctl00_MainContent_txtName    Sales Tax
    Select from List by Value    id=ctl00_MainContent_ddAgency    10
    Set Browser Implicit Wait    10 sec
    Select from List by Value    id=ctl00_MainContent_ddType    4
    Select from List by Value    id=ctl00_MainContent_ddWorkflow    0
    Input Text    id=ctl00_MainContent_txtDWindow    3
    Select from List by Value    id=ctl00_MainContent_ddSchedules    1
    Select from List by Value    id=ctl00_MainContent_ddSettlement    1
    Select from List by Value    id=ctl00_MainContent_ddInvoicing    true
    Select Checkbox    id=ctl00_MainContent_chksCurrency_0
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain Element    id=ctl00_MainContent_lblMsg

Customer Merge
    [Documentation]    *Customer Merge*
    ...    1. Navigate to Settings and click on Customer Merge
    ...    2. Search for customers to merge and add them to list
    ...    3. Click Continue and Enter Fidesic credentials
    ...    4. Click Merge
    Click ELement    //*[@id="main-menu"]/li[7]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[7]/ul/li[4]/a/span
    Set Browser Implicit Wait    30 sec
    Input Text    id=ctl00_MainContent_txtCustomer    A
    Click Element    id=ctl00_MainContent_btnCustomerSearch
    Set Browser Implicit Wait    30 sec
    Select From List by Value    id=ctl00_MainContent_ddOrganization    408
    Input Text    id=ctl00_MainContent_txtSubcustomer    A
    Click Element    id=ctl00_MainContent_btnSearch
    Set Browser Implicit Wait    30 sec
    Select From List by Value    id=ctl00_MainContent_ddSubCustomer    475
    Click Element    id=ctl00_MainContent_btnContinue
    Set Browser Implicit Wait    20 sec
    Input Text    id=ctl00_MainContent_txtFidesicName    support@avitechng.com
    Input Text    id=ctl00_MainContent_txtFidesicPassword    venture-123
    Click Element    id=ctl00_MainContent_btnMerge
    Set Browser Implicit Wait    30 sec
    Page should contain Element

Recover Password
    [Documentation]    *Recover Password*
    ...    1. Open url and Click Recover password
    ...    2. Enter Valid email address
    ...    3. Click Send
    Open Browser    http://64.91.230.133:8006/Login.aspx    ${browser}
    Maximize Browser Window
    Wait until element is visible    ${LoginButton id}    30 s
    Click Element    //*[@id="login-block"]/div/div/div/div[6]/a[1]
    Set Browser Implicit Wait    30 s
    Input Text    id=ctl00_MainContent_txtEmail    mujidat@splasherstech.com
    Click Element    id=ctl00_MainContent_Button1
    Set browser Implicit Wait    30 s
    Page should contain element    id=ctl00_MainContent_Label1

Edit User
    Click Element    //*[@id="main-menu"]/li[3]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="ctl00_MainContent_gvListPersons"]/tbody/tr[6]/td[6]/a
    Set Browser Implicit Wait    30 sec
    Select Checkbox    id=ctl00_MainContent_chkPermissionList_0
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain element    id=ctl00_MainContent_lblMsg

Add RevenueGroup
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[6]/ul/li[2]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_btnAddGroup
    Set Browser Implicit Wait    10 sec
    Select from list by value    id=ctl00_MainContent_ddlAgency2    10
    Input Text    id=ctl00_MainContent_txtGroup    Group11
    Input Text    id=ctl00_MainContent_txtName    New Group
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain element    id=ctl00_MainContent_Label1

Edit RevenueGroup
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="main-menu"]/li[6]/ul/li[2]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    id=ctl00_MainContent_GridView1_ctl04_btnEdit
    Set browser implicit wait    30 sec
    Clear element text    id=ctl00_MainContent_GridView1_ctl04_txtRev
    Input text    id=ctl00_MainContent_GridView1_ctl04_txtRev    Test GroupOne
    Click Element    id=ctl00_MainContent_GridView1_ctl04_btnUpdate
    Set Browser Implicit Wait    10 sec
    Get Alert Message    dismiss=true

Search Invoice by InvoiceNumber
    Click Element    //*[@id="main-menu"]/li[1]/a
    Wait Until Element is Visible    id=ctl00_MainContent_btnSearch    30 s
    Click Element    id=ctl00_MainContent_txtStartDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[3]/a
    Click Element    id=ctl00_MainContent_txtEndDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a
    Input Text    id=ctl00_MainContent_txtInvoice    9100010012213
    Click Element    id=ctl00_MainContent_btnSearch
    Set Browser Implicit Wait    30 sec
    Comment    Table should Contain    //*[@id="ctl00_MainContent_gvInvoices"]    id=ctl00_MainContent_gvInvoices_ctl03_LinkButton1
    Click Link    Link= 9100010012213

Print Invoice
    Click Element    id=ctl00_MainContent_btnPrintInvoice
    Set Browser Implicit Wait    2 min
    Get Alert message    dismiss=true

Search Invoice by Date
    Click Element    //*[@id="main-menu"]/li[1]/a
    Wait Until Element is Visible    id=ctl00_MainContent_btnSearch    5 min
    Click Element    id=ctl00_MainContent_txtStartDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[1]/td[3]/a
    Click Element    id=ctl00_MainContent_txtEndDate
    Click Element    //*[@id="ui-datepicker-div"]/div/a[1]/span
    Click Element    //*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[4]/a
    Click Element    id=ctl00_MainContent_btnSearch
    Set Browser Implicit Wait    30 sec
    Page should Contain Element    id=ctl00_MainContent_gvInvoices

Edit Revenue
    Click Element    //*[@id="main-menu"]/li[6]/a/span
    Set Browser Implicit Wait    30 sec
    Click Element    //*[@id="ctl00_MainContent_gvRevenue"]/tbody/tr[4]/td[5]/a
    Set Browser implicit Wait    30 sec
    Select from list by value    id=ctl00_MainContent_ddType    1
    Select checkbox    id=ctl00_MainContent_chksCurrency_1
    Click Element    id=ctl00_MainContent_btnSave
    Set Browser Implicit Wait    30 sec
    Page should contain element    id=ctl00_MainContent_lblMsg
