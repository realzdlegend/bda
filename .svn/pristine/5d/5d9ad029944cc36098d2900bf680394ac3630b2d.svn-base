﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDA.Logic.logic;
using BDA.Domain.Entity;

public partial class Agency_EditRevenue : PageBase
{
    #region Repository
    RevenueRepository revRep = new RevenueRepository();
    OrganizationRepository orgRep = new OrganizationRepository();
    #endregion

    private int revenueId
    {
        get
        {
            try
            {
                int revenueId = int.Parse(_queryString["revenueId"]);
                return revenueId;
            }
            catch
            {
                return 0;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ddAgency.DataSource = orgRep.GetAgencies(-1, -1);
            ddAgency.DataBind();
            chksCurrency.DataSource = new InvoiceRepository().GetAllCurrency();
            chksCurrency.DataBind();
            Revenue revenue = revenueId > 0 ? revRep.GetRevenue(revenueId) : null;
            if (revenue != null)
            {
                txtCode.Text = revenue.Code;
                txtName.Text = revenue.Name;
                ddAgency.SelectedValue = revenue.AgencyId.ToString();
                txtDWindow.Text = revenue.DisputeWindow.ToString();
                ddSchedules.SelectedValue = revenue.ScheduleId.HasValue ? revenue.ScheduleId.ToString() : "0";
                ddSettlement.SelectedValue = revenue.SettlementId.ToString();
                ddInvoicing.SelectedValue = revenue.EnableInvoicing ? "true" : "false";
                if (!revenue.RevenueCurrency.IsEmpty)
                {
                    chksCurrency.ClearSelection();
                    foreach (var revCurrency in revenue.RevenueCurrency)
                    {
                        chksCurrency.Items.FindByValue(revCurrency.Id.ToString()).Selected = true;
                    }
                }
                ddAgency.Enabled = CurrentRole.RoleName == "Admin" ? revRep.CanEdit(revenueId) : false;
            }
            else
            {

            }
            ddWorkflow.DataSource = new WorkflowRepository().GetWorkflows(int.Parse(ddAgency.SelectedValue));
            ddWorkflow.DataBind();
            ddWorkflow.Items.Insert(0, new ListItem("Not Set", "0"));
           
            if (revenue != null && revenue.DisputeWorkflowId.HasValue)
            {
                ddWorkflow.SelectedValue = revenue.DisputeWorkflowId.Value.ToString();
            }
            try
            {
                if (revenue != null)
                {
                    BindRevenueGroup(revenue.RevenueGroupId);
                }
                else
                {
                    BindRevenueGroup(null);
                }
                
               // ddType.Text = revenue.RevType;
                    // string.IsNullOrWhiteSpace((revenue.RevType)) ? revenue.RevType : string.Empty;
                // != null? (int)revenue.RevenueGroupId: -1; //revenue.RevType;
            }
            catch(Exception ex)
            {
                ErrorLogger.Log(ex);
            }
           
        }

      
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        try
        {
            var isEdit = Request.QueryString.AllKeys;
            Revenue revenue = (revenueId > 0) ? revRep.GetRevenue(revenueId) : new Revenue();
            revenue.AgencyId = int.Parse(ddAgency.SelectedValue);
            revenue.Code = txtCode.Text;
            revenue.DisputeWindow = string.IsNullOrEmpty(txtDWindow.Text) ? 0 : int.Parse(txtDWindow.Text);
            revenue.Name = txtName.Text;
            revenue.RevType = ddType.SelectedItem.Text.Contains("-Select-") ? "None" : ddType.SelectedItem.Text;
            revenue.SettlementId = int.Parse(ddSettlement.SelectedValue);
            revenue.ScheduleId = int.Parse(ddSchedules.SelectedValue);
            revenue.DisputeWorkflowId = int.Parse(ddWorkflow.SelectedValue);
            revenue.EnableInvoicing = bool.Parse(ddInvoicing.SelectedValue);
            if (int.Parse(ddType.SelectedValue) > 0)
            {
                revenue.RevenueGroupId = int.Parse(ddType.SelectedValue);
            }
            else if (int.Parse(ddType.SelectedValue) == -1)
            {
                revenue.RevenueGroupId = null;
            }

            var currencies = new InvoiceRepository().GetAllCurrency();
            var selectedCurrencies = new List<Currency>();
            foreach (var currency in currencies)
            {
                if (chksCurrency.Items.FindByValue(currency.Id.ToString()).Selected)
                    selectedCurrencies.Add(currency);
            }
            revRep.SaveRevenue(revenue, selectedCurrencies);

            if (isEdit.Length > 0)
            {
                lblMsg.Text = string.Format("{0} has been updated", revenue.ToString());
                lblMsg.Visible = true;
                lbMsg.Visible = false;
            }
            else
            {
                lblMsg.Text = string.Format("{0} has been created", revenue.ToString());
                lblMsg.Visible = true;
                lbMsg.Visible = false;
            }
            //Show successful message
           
            //return;
            ClearField();

            //Response.Redirect("RevenueSettings.aspx?view=saved");
        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
            lblMsg.Visible = false;
            lbMsg.Text = @"Error saving: " + ex.Message;
            lbMsg.Visible = true;
        }
    }
    protected void ddAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindRevenueGroup(null);
    }

    private void ClearField()
    {
        txtCode.Text = string.Empty;
        txtName.Text = string.Empty;
        ddType.SelectedIndex = 0;
        ddSettlement.SelectedIndex = 0;
        //ddAgency.SelectedValue = string.Empty;
        

        chksCurrency.ClearSelection();
    }

    private void BindRevenueGroup(int? rgpId)
    {
        int agencyid = int.Parse(ddAgency.SelectedValue);
        IList<AgencyRevenueGroup> lst = revRep.GetAgencyRevenueGroup(agencyid);
        ddType.DataSource = lst;

        if (rgpId != null)
        {
            var agencyRevenueGroup = revRep.GetRevenueGroup(Convert.ToInt32(rgpId));
            var sId = lst.IndexOf(agencyRevenueGroup);
            ddType.SelectedIndex = lst.IndexOf(agencyRevenueGroup) + 1;

        }

       ddType.DataBind();
              
    }
}