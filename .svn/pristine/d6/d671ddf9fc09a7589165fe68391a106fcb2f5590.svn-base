﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BDA.Logic.interfaces;
using BDA.Domain.Entity;
using BDA.Domain.Util;
using BDA.Domain;
using NHibernate.Criterion;

namespace BDA.Logic.logic
{
    public class DisputeManager : BaseRepository<Dispute, int>
    {
        AuditRepository audit = new AuditRepository();

        public DisputeStatus CanDispute(long billId)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(billId);
            if (active != null)
                return DisputeStatus.Active;

            BillView bill = _session.Get<BillView>(billId);
            if (AppSettings.LocalTime.Date > bill.ClosingBillDate.Date)
                return DisputeStatus.Disabled;

            if (bill.Status > 1)
                return DisputeStatus.ProcessedBill;

            if (bill.Status == 1)
                return DisputeStatus.AllowNew;

            return DisputeStatus.Error;
        }

        public bool CanApprove(long billId, Guid personId)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(billId);
            if (active == null)
                return false;

            Bill bill = _session.Get<Bill>(billId);
            int? workflowId = bill.MasterList.Revenue.DisputeWorkflowId;
            if (!workflowId.HasValue)
                return false;
            
            var query1 = _session.QueryOver<DisputeApproval>();
            query1.Where(a=>a.DisputeId==active.DisputeId);
            int approvalcount = query1.Select(Projections.RowCount()).SingleOrDefault<int>();
            int newLevel = approvalcount + 1;

            var query2 = _session.QueryOver<WorkflowUser>();
            query2.Where(u => u.WorkflowId == workflowId.Value);
            query2.Where(u => u.PersonId == personId);
            query2.Where(u => u.WorkflowLevel == newLevel);

            return query2.SingleOrDefault<WorkflowUser>() == null ? false : true;
        }

        public bool CanViewDispute(long billId, Guid personId)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(billId);
            if (active == null)
                return false;

            Bill bill = _session.Get<Bill>(billId);
            int? workflowId = bill.MasterList.Revenue.DisputeWorkflowId;
            if (!workflowId.HasValue)
                return false;

            var query = _session.QueryOver<WorkflowUser>();
            query.Where(u => u.WorkflowId == workflowId.Value);
            query.Where(u => u.PersonId == personId);

            return query.List<WorkflowUser>().Count > 0 ? true : false;
        }

        public bool CanEdit(long billId, Guid personId)
        {
            Bill bill = _session.Get<Bill>(billId);
            if (bill.Status != 1)
                return false;
            int? workflowId = bill.MasterList.Revenue.DisputeWorkflowId;
            if (!workflowId.HasValue)
                return false;

            var query = _session.QueryOver<WorkflowUser>();
            query.Where(u => u.WorkflowId == workflowId.Value);
            query.Where(u => u.PersonId == personId);

            return query.List<WorkflowUser>().Count > 0 ? true : false;
        }

        public ActiveDispute GetActiveDispute(long billId)
        {
            return _session.Get<ActiveDispute>(billId);
        }

        public IList<DisputeDetail> GetDisputeDetails(int disputeId)
        {
            var query = _session.QueryOver<DisputeDetail>();
            query.Where(dd => dd.DisputeId == disputeId);
            return query.List<DisputeDetail>();
        }

        public IList<DisputeDetail> GetDisputeDetails(long billId)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(billId);
            return (active == null) ? null : GetDisputeDetails(active.DisputeId);
        }

        public decimal CalculateAmount(long billId, IList<DisputeDetail> details)
        {
            IList<BillDetail> allBills = new BillDetailsRepository().GetBillDetails(billId, -1, -1);
            IList<BillDetail> disputed = details.Select(dd=>dd.BillDetail).ToList();
            IList<BillDetail> nonDisputed = Enumerable.Except(allBills, disputed).ToList();

            decimal total1 = allBills.Except(disputed).Sum(x => x.EditedAmount);
            decimal total2 = details.Sum(x => x.FinalAmount.Value);

            return total1 + total2;
        }

        public void ApproveDispute(Guid personId, string comment, long billId, bool overrideBill, IList<DisputeDetail> details)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(billId);

            if (active == null)
                throw new Exception("No active dispute");

            decimal amount = CalculateAmount(billId, details);

            DisputeApproval approval = new DisputeApproval();
            approval.Comment = comment;
            approval.DisputeId = active.DisputeId;
            approval.FinalAmount = amount;
            approval.PersonId = personId;
            approval.SubmittedDate = AppSettings.LocalTime;

            var query = _session.QueryOver<DisputeApproval>();
            query.Where(m => m.DisputeId == active.DisputeId);
            query.Select(Projections.RowCount());
            int existingApprovals = query.SingleOrDefault<int>();

            Bill bill = _session.Get<Bill>(billId);
            Revenue revenue = bill.MasterList.Revenue;
            decimal oldAmount = bill.Amount;
            string oldAmountString = bill.AmountString;

            int workflowId = revenue == null || !revenue.DisputeWorkflowId.HasValue ? 0 : revenue.DisputeWorkflowId.Value;
            int maxLevel = new WorkflowRepository().GetLevels(workflowId);

            if (workflowId == 0 || maxLevel == 0)
                throw new Exception("Invalid workflow specified");

            bool finalApproval = (existingApprovals + 1) >= maxLevel;

            try
            {
                _session.BeginTransaction();
                _session.Save(approval);

                foreach (DisputeDetail dd in details)
                {
                    if (dd.FinalAmount.HasValue)
                    {
                        if (finalApproval)
                        {
                            BillDetail bd = _session.Get<BillDetail>(dd.BillDetailId);
                            bd.EditedAmount = dd.FinalAmount.Value;
                            _session.Update(bd);
                        }

                        _session.Save(dd);
                    }
                    else
                        throw new Exception(string.Format("No value specified for Line number {0}", dd.LineNumber));
                }
                if (finalApproval)
                {
                    Dispute dispute = _session.Get<Dispute>(active.DisputeId);
                    dispute.ClosedDate = AppSettings.LocalTime;
                    _session.Update(dispute);
                    _session.Delete(active);

                    bill.Status = (int)BillStatus.Pending;
                    bill.LastEditDate = AppSettings.LocalTime;
                    bill.Amount = overrideBill ? amount : bill.Amount;
                    
                    _session.Save(bill);

                    BillHistory history = new BillHistory();
                    history.BillId = bill.Id;
                    history.DateChanged = AppSettings.LocalTime;
                    history.FieldChanged = "Status";
                    history.NewValue = "Pending";
                    history.OldValue = "Disputed";
                    history.PersonId = personId;
                    _session.Save(history);

                    if (oldAmount != bill.Amount)
                    {
                        BillHistory ahistory = new BillHistory();
                        ahistory.BillId = bill.Id;
                        ahistory.DateChanged = AppSettings.LocalTime;
                        ahistory.FieldChanged = "Amount";
                        ahistory.NewValue = bill.Amount.ToString("#,###.#0");
                        ahistory.OldValue = oldAmount.ToString("#,###.#0");
                        ahistory.PersonId = personId;
                        _session.Save(ahistory);
                    }

                }
                _session.Transaction.Commit();

                try
                {
                    new NotificationRepository().ApproveDispute(approval);
                    Workflow workflow = _session.Get<Workflow>(workflowId);
                    string detail = string.Format("Approved Dispute for Bill {0} using workflow {1}. Amount: {2}, Comment \"{3}\" ", 
                        bill.Description, workflow.Name, amount.ToString(), comment);
                    audit.SaveAuditTrail(Actions.Workflow, detail);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex);
                }
            }
            catch
            {
                _session.Transaction.Rollback();
                throw;
            }
        }

        public void EditBill(Guid personId, string comment, long billId, IList<DisputeDetail> details)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(billId);
            Bill bill = _session.Get<Bill>(billId);

            if (active != null)
                throw new Exception("There is an already active dispute associated with the bill");
            else if (bill.Status != 1)
                throw new Exception("Bills cannot be disputed, only pending bills can be edited");

            decimal totalDisputed = details.Sum(x => x.FinalAmount.Value);
            IList<long> detailsIds = details.Select(x=>x.BillDetailId).ToList<long>();

            IList<BillDetail> alldetails = new BillDetailsRepository().GetBillDetails(billId, -1, -1);
            decimal totalUndisputed = 0;
            foreach (BillDetail bd in alldetails)
            {
                totalUndisputed = detailsIds.Contains(bd.Id) ? totalUndisputed : totalUndisputed + bd.EditedAmount;
            }
            decimal newAmount = totalDisputed + totalUndisputed;
            decimal oldAmount = bill.Amount;

            try
            {
                _session.BeginTransaction();

                Dispute dispute = new Dispute();
                dispute.BillId = billId;
                dispute.ClosedDate = AppSettings.LocalTime;
                dispute.PersonId = personId;
                dispute.ProposedAmount = newAmount;
                dispute.Reason = comment;
                dispute.SubmittedDate = AppSettings.LocalTime;

                _session.Save(dispute);

                DisputeApproval approval = new DisputeApproval();
                approval.Comment = comment;
                approval.DisputeId = dispute.Id;
                approval.FinalAmount = newAmount;
                approval.PersonId = personId;
                approval.SubmittedDate = AppSettings.LocalTime;

                Revenue revenue = bill.MasterList.Revenue;

                _session.Save(approval);

                foreach (DisputeDetail dd in details)
                {
                    if (dd.FinalAmount.HasValue)
                    {
                        BillDetail bd = _session.Get<BillDetail>(dd.BillDetailId);
                        bd.EditedAmount = dd.FinalAmount.Value;
                        _session.Update(bd);

                        dd.DisputeId = dispute.Id;
                        _session.Save(dd);
                    }
                    else
                        throw new Exception(string.Format("No value specified for Line number {0}", dd.LineNumber));
                }

                bill.Status = (int)BillStatus.Pending;
                bill.LastEditDate = AppSettings.LocalTime;
                bill.Amount = newAmount;
                _session.Save(bill);

                BillHistory ahistory = new BillHistory();
                ahistory.BillId = bill.Id;
                ahistory.DateChanged = AppSettings.LocalTime;
                ahistory.FieldChanged = "Amount";
                ahistory.NewValue = bill.Amount.ToString("#,###.#0");
                ahistory.OldValue = oldAmount.ToString("#,###.#0");
                ahistory.PersonId = personId;
                _session.Save(ahistory);

                _session.Transaction.Commit();

                try
                {
                    string detail = string.Format("Edited Bill {0} from {1} to {2}, Comment \"{3}\" ",
                        bill.Description, oldAmount.ToString("#,###.#0"), bill.Amount.ToString("#,###.#0"), comment);

                    new NotificationRepository().EditBill(_session.Get<BillView>(bill.Id), personId, detail);
                    audit.SaveAuditTrail(Actions.Workflow, detail);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex);
                }
            }
            catch (Exception ex)
            {
                _session.Transaction.Rollback();
                ErrorLogger.Log(ex);
                throw ex;
            }
        }

        public int SaveDispute(long Id, Guid personId, string comment, decimal? amount, IList<DisputeDetail> details)
        {
            ActiveDispute active = _session.Get<ActiveDispute>(Id);

            if (active != null)
                throw new Exception("There is an already active dispute associated with the bill");

            Bill bill = _session.Get<Bill>(Id);
            if (bill == null || bill.Status != 1)
                throw new Exception("Bills cannot be disputed, only pending bills can be disputed");

            Dispute dispute = new Dispute();
            dispute.BillId = Id;
            dispute.PersonId = personId;
            dispute.ProposedAmount = amount;
            dispute.Reason = comment;
            dispute.SubmittedDate = AppSettings.LocalTime;

            try
            {
                _session.BeginTransaction();

                dispute.SubmittedDate = AppSettings.LocalTime;
                _session.Save(dispute);

                bill.LastEditDate = AppSettings.LocalTime;
                bill.Status = (int)BillStatus.Disputed;
                _session.Save(bill);

                active = new ActiveDispute();
                active.Id = dispute.BillId;
                active.DisputeId = dispute.Id;
                _session.Save(active);

                BillHistory history = new BillHistory();
                history.BillId = bill.Id;
                history.DateChanged = AppSettings.LocalTime;
                history.FieldChanged = "Status";
                history.NewValue = "Disputed";
                history.OldValue = "Pending";
                history.PersonId = personId;
                _session.Save(history);

                foreach (DisputeDetail dd in details)
                {
                    dd.DisputeId = dispute.Id;
                    _session.Save(dd);
                }

                _session.Transaction.Commit();

                try
                {
                    new NotificationRepository().NewDispute(dispute);
                    string detail = string.Format("Created a Dispute for Bill {0}. Amount: {1}, Comment \"{2}\". ",
                        bill.Description, amount.ToString(), comment);
                    audit.SaveAuditTrail(Actions.Workflow, detail);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex);
                }
            }
            catch(Exception ex)
            {
                _session.Transaction.Rollback();
                throw new Exception();
            }
            return dispute.Id;
        }

        public IList<DisputeApproval> GetApprovals(int disputeId)
        {
            var query = _session.QueryOver<DisputeApproval>();
            query.Where(da => da.DisputeId == disputeId);

            return query.List();
        }
    }
}
