﻿using AutoMapper;
using BDA.Domain.Entity;
using BDA.Domain.Models;
using BDA.Search.Data;
using BDA.Services.Models;
using System.Collections.Generic;

namespace BDA.Services
{
    public static class AutoMapperConfig
    {
        public static void Register()
        {
            Mapper.CreateMap<PaymentDetail, PaymentModel>();
            Mapper.CreateMap<VersionPayment, PaymentModel>();
            Mapper.CreateMap<Revenue, RevenueModel>();
            Mapper.CreateMap<RevenueModel, Revenue>();
            Mapper.CreateMap<ServiceProvider, CustomerModel>();
            Mapper.CreateMap<BillModel, Bill>();
            Mapper.CreateMap<Bill, BillModel>();
            Mapper.CreateMap<Organization, CustomerModel>();
            Mapper.CreateMap<Location, LocationModel>();
            Mapper.CreateMap<InvoiceBill, BillModel>();
            Mapper.CreateMap<DateReport, DateReportModel>();
            Mapper.CreateMap<Invoice, SettlementInvoice>()
                .ForMember(x => x.Agency, m => m.MapFrom(x => x.Agency))
                .ForMember(x => x.CustomerName, m => m.MapFrom(x => x.ServiceProviderName))
                .ForMember(x => x.InvoiceDate, m => m.MapFrom(x => x.InvoiceFormatDate))
                .ForMember(x => x.TotalAmount, m => m.MapFrom(x => x.Amount));

            Mapper.CreateMap<VersionBill, BillModel>()
                .ForMember(t => t.Id, m => m.MapFrom(x => x.BillId));

            Mapper.CreateMap<InvoiceView, InvoiceDetailModel>()
            .ForMember(d => d.CustomerId, m => m.MapFrom(x => x.ProviderId))
            .ForMember(d => d.Customer, m => m.MapFrom(x => x.Provider.Name))
            .ForMember(d => d.MinAmountPayable, m => m.MapFrom(x => 1))
            .ForMember(d => d.CustomerAIN, m => m.MapFrom(x => x.Provider.Code))
            .ForMember(d => d.CustomerName, m => m.MapFrom(x => x.Name))
            .ForMember(d => d.AgencyId, m => m.MapFrom(x => x.Revenue.AgencyId))
            .ForMember(d => d.Agency, m => m.MapFrom(x => x.Revenue.Agency.Name))
            .ForMember(d => d.AgencyCode, m => m.MapFrom(x => x.Revenue.Agency.Code))
            .ForMember(d => d.RevenueId, m => m.MapFrom(x => x.RevenueId))
            .ForMember(d => d.Revenue, m => m.MapFrom(x => x.Revenue.Name))
            .ForMember(d => d.RevenueCode, m => m.MapFrom(x => x.Revenue.Code))
            .ForMember(d => d.RevenueType, m => m.MapFrom(x => x.Revenue.RevType))
            .ForMember(d => d.Currency, m => m.MapFrom(x => x.Currency.Name))
            .ForMember(d => d.CurrencyCode, m => m.MapFrom(x => x.Currency.Code))
            .ForMember(d => d.CurrencyId, m => m.MapFrom(x => x.CurrencyId))
            .ForMember(d => d.Location, m => m.MapFrom(x => x.Location.Address))
            .ForMember(d => d.LocationType, m => m.MapFrom(x => x.Location.Type))
            .ForMember(d => d.LocationCode, m => m.MapFrom(x => x.Location.Code))
            .ForMember(d => d.LocationId, m => m.MapFrom(x => x.LocationId))
            .ForMember(d => d.AmountPaidText, m => m.MapFrom(x => x.AmountPaidString))
            .ForMember(d => d.AmountText, m => m.MapFrom(x => x.AmountString));

            Mapper.CreateMap<VersionInvoice, InvoiceDetailModel>()
            .ForMember(d => d.CustomerId, m => m.MapFrom(x => x.CustomerId))
            .ForMember(d => d.Customer, m => m.MapFrom(x => x.Customer))
            .ForMember(d => d.CustomerAIN, m => m.MapFrom(x => x.CustomerAIN))
            .ForMember(d => d.CustomerName, m => m.MapFrom(x => x.CustomerName))
            .ForMember(d => d.AgencyId, m => m.MapFrom(x => x.AgencyId))
            .ForMember(d => d.Agency, m => m.MapFrom(x => x.Agency))
            .ForMember(d => d.AgencyCode, m => m.MapFrom(x => x.AgencyCode))
            .ForMember(d => d.RevenueId, m => m.MapFrom(x => x.RevenueId))
            .ForMember(d => d.Revenue, m => m.MapFrom(x => x.Revenue))
            .ForMember(d => d.RevenueCode, m => m.MapFrom(x => x.RevenueCode))
            .ForMember(d => d.RevenueType, m => m.MapFrom(x => ""))
            .ForMember(d => d.Currency, m => m.MapFrom(x => x.Currency))
            .ForMember(d => d.CurrencyCode, m => m.MapFrom(x => x.CurrencyCode))
            .ForMember(d => d.CurrencyId, m => m.MapFrom(x => x.CurrencyId))
            .ForMember(d => d.Location, m => m.MapFrom(x => x.Location))
            .ForMember(d => d.LocationType, m => m.MapFrom(x => ""))
            .ForMember(d => d.LocationCode, m => m.MapFrom(x => x.LocationCode))
            .ForMember(d => d.LocationId, m => m.MapFrom(x => x.LocationId))
            .ForMember(d => d.AmountPaidText, m => m.MapFrom(x => x.AmountPaidString))
            .ForMember(d => d.AmountText, m => m.MapFrom(x => x.AmountString));

            Mapper.CreateMap<Invoice, NIBSSInvoice>()
            .ForMember(d => d.InvoiceNumber, m => m.MapFrom(x => x.InvoiceNumber))
            .ForMember(d => d.Customer, m => m.MapFrom(x => x.Provider.Name))
            .ForMember(d => d.CustomerAIN, m => m.MapFrom(x => x.Provider.Code))
            .ForMember(d => d.CustomerName, m => m.MapFrom(x => x.Customer.Name))
            .ForMember(d => d.Agency, m => m.MapFrom(x => x.Revenue.Agency.Name))
            .ForMember(d => d.AgencyId, m => m.MapFrom(x => x.Revenue.Agency.Id))
            .ForMember(d => d.Revenue, m => m.MapFrom(x => x.Revenue.Name))
            .ForMember(d => d.Currency, m => m.MapFrom(x => x.CurrencyId.ToString()))
            .ForMember(d => d.Location, m => m.MapFrom(x => x.Location.Address))
            .ForMember(d => d.TotalAmount, m => m.MapFrom(x => x.Amount))
            .ForMember(d => d.Amount, m => m.MapFrom(x => x.Amount))
            .ForMember(d => d.Email, m => m.MapFrom(x => x.Customer.Email))
            .ForMember(d => d.PhoneNumber, m => m.MapFrom(x => x.Customer.Phone))
            .ForMember(d => d.BillerName, m => m.MapFrom(x => "VGG Collections"))
            .ForMember(d => d.BillerBank, m => m.MapFrom(x => "011"))
            .ForMember(d => d.BillerAccount, m => m.MapFrom(x => "0123456789"));
            Mapper.CreateMap<LocationView, LocationModel>()
            .ForMember(d => d.State, m => m.MapFrom(x => x.State.Name));

            Mapper.CreateMap<Invoice, ReturnedInvoiceModel>()
            .ForMember(d => d.InvoiceNumber, m => m.MapFrom(x => x.InvoiceNumber))
            .ForMember(d => d.Customer, m => m.MapFrom(x => x.Provider.Name))
            .ForMember(d => d.CustomerAIN, m => m.MapFrom(x => x.Provider.Code))
            .ForMember(d => d.CustomerName, m => m.MapFrom(x => x.Customer.Name))
            .ForMember(d => d.Agency, m => m.MapFrom(x => x.Revenue.Agency.Name))
            .ForMember(d => d.Revenue, m => m.MapFrom(x => x.Revenue.Name))
            .ForMember(d => d.Currency, m => m.MapFrom(x => x.CurrencyId.ToString()))
            .ForMember(d => d.Location, m => m.MapFrom(x => x.Location.Address))
            .ForMember(d => d.Amount, m => m.MapFrom(x => x.Amount))
            .ForMember(d => d.Email, m => m.MapFrom(x => x.Customer.Email));

            Mapper.CreateMap<LocationView, LocationModel>()
            .ForMember(d => d.State, m => m.MapFrom(x => x.State.Name));

            Mapper.CreateMap<AgencyModel, Agency>()
                .ForMember(d => d.Address, m => m.MapFrom(x => x.Service));

            Mapper.CreateMap<Agency, AgencyModel>()
                .ForMember(d => d.Service, m => m.MapFrom(x => x.Address));

            Mapper.CreateMap<TitleModel, Title>();
            Mapper.CreateMap<Title, TitleModel>();

            Mapper.CreateMap<FidesicCredential, FidesicModel>();
            Mapper.CreateMap<FidesicModel, FidesicCredential>();
            Mapper.CreateMap<Settlement, SettlementModel>();
            Mapper.CreateMap<SettlementModel, Settlement>();
            Mapper.CreateMap<SettlementBenModel, SettlementBeneficiary>();
            Mapper.CreateMap<SettlementBeneficiary, SettlementBenModel>();
            Mapper.CreateMap<EbillsModel, EbillsBeneficiary>();
            Mapper.CreateMap<EbillsBeneficiary, EbillsModel>();
            Mapper.CreateMap<CustomerInvoiceModel, CustomerInvoice>();
            Mapper.CreateMap<InvoiceItemModel, InvoiceItem>();
            Mapper.CreateMap<State, StateModel>();
            Mapper.CreateMap<StateModel, State>();

            //service provider title
            Mapper.CreateMap<ServiceProviderTitle, ServiceProviderTitleModel>();


            Mapper.CreateMap<ServiceProviderTitleModel, Title>()
                .ForMember(d => d.Id, m => m.MapFrom(x => x.TitleId));

            Mapper.CreateMap<ServiceProviderTitleModel, ServiceProviderTitle>()
                .ForMember(d => d.Title, m => m.MapFrom(x => x));


            //service provider
            Mapper.CreateMap<ServiceProvider, ServiceProviderModel>();
            Mapper.CreateMap<ServiceProviderModel, ServiceProvider>()
                .ForMember(x => x.AgencyProviders, opt => opt.Ignore()); ;

            Mapper.CreateMap<Invoice, SimilarInvoiceModel>();

            Mapper.CreateMap<SettlementAccount, SettlementAccountModel>();
            Mapper.CreateMap<SettlementAccountModel, SettlementAccount>();
            Mapper.CreateMap<BankModel, Bank>();
            Mapper.CreateMap<Bank, BankModel>();
            Mapper.CreateMap<RevenueBeneficiary, RevenueBeneficiaryModel>();
            Mapper.CreateMap<RevenueBeneficiaryModel, RevenueBeneficiary>();
            Mapper.CreateMap<RolePermission, RolePermissionModel>();
            Mapper.CreateMap<RolePermissionModel, RolePermission>();
            Mapper.CreateMap<Permission, PermissionModel>();
            Mapper.CreateMap<PermissionModel, Permission>();
            Mapper.CreateMap<RemitaServiceTypeModel, RemitaServiceType>();

        }


        public static RemitaServiceType ToModel(this RemitaServiceTypeModel entity)
        {
            return Mapper.Map<RemitaServiceTypeModel, RemitaServiceType>(entity);
        }

        public static InvoiceDetailModel ToModel(this InvoiceView entity)
        {
            return Mapper.Map<InvoiceView, InvoiceDetailModel>(entity);
        }

        public static InvoiceDetailModel ToModel(this VersionInvoice entity)
        {
            return Mapper.Map<VersionInvoice, InvoiceDetailModel>(entity);
        }

        public static PaymentModel ToModel(this PaymentDetail entity)
        {
            return Mapper.Map<PaymentDetail, PaymentModel>(entity);
        }

        public static PaymentModel ToModel(this VersionPayment entity)
        {
            return Mapper.Map<VersionPayment, PaymentModel>(entity);
        }

        public static DateReportModel ToModel(this DateReport entity)
        {
            return Mapper.Map<DateReport, DateReportModel>(entity);
        }

        public static SettlementInvoice ToModel(this Invoice entity)
        {
            return Mapper.Map<Invoice, SettlementInvoice>(entity);
        }

        public static CustomerInvoice ToModel(this CustomerInvoiceModel entity)
        {
            return Mapper.Map<CustomerInvoiceModel, CustomerInvoice>(entity);
        }

        public static InvoiceItem ToModel(this InvoiceItemModel entity)
        {
            return Mapper.Map<InvoiceItemModel, InvoiceItem>(entity);
        }

        public static IEnumerable<StateModel> ToModel(this IEnumerable<State> entity)
        {
            return Mapper.Map<IEnumerable<State>, IEnumerable<StateModel>>(entity);
        }

        public static SettlementAccount ToModel(this SettlementAccountModel entity)
        {
            return Mapper.Map<SettlementAccountModel, SettlementAccount>(entity);
        }

        public static SettlementAccountModel ToModel(this SettlementAccount entity)
        {
            return Mapper.Map<SettlementAccount, SettlementAccountModel>(entity);
        }

        public static IEnumerable<SettlementAccountModel> ToModel(this List<SettlementAccount> entity)
        {
            return Mapper.Map<IEnumerable<SettlementAccount>, IEnumerable<SettlementAccountModel>>(entity);
        }

        public static IEnumerable<BankModel> ToModel(this IEnumerable<Bank> entity)
        {
            return Mapper.Map<IEnumerable<Bank>, IEnumerable<BankModel>>(entity);
        }

        public static RevenueBeneficiary ToModel(RevenueBeneficiaryModel entity)
        {
            return Mapper.Map<RevenueBeneficiaryModel, RevenueBeneficiary>(entity);
        }

        public static RevenueBeneficiaryModel ToModel(RevenueBeneficiary entity)
        {
            return Mapper.Map<RevenueBeneficiary, RevenueBeneficiaryModel>(entity);
        }

        public static RevenueModel ToModel(Revenue entity)
        {
            return Mapper.Map<Revenue, RevenueModel>(entity);
        }

        public static IEnumerable<RevenueBeneficiaryModel> ToModel(IEnumerable<RevenueBeneficiary> entity)
        {
            return Mapper.Map<IEnumerable<RevenueBeneficiary>, IEnumerable<RevenueBeneficiaryModel>>(entity);
        }

        public static RolePermission ToModel(RolePermissionModel model)
        {
            return Mapper.Map<RolePermissionModel, RolePermission>(model);
        }

        public static RolePermissionModel ToModel(RolePermission model)
        {
            return Mapper.Map<RolePermission, RolePermissionModel>(model);
        }

        public static Permission ToModel(PermissionModel model)
        {
            return Mapper.Map<PermissionModel, Permission>(model);
        }

        public static PermissionModel ToModel(Permission model)
        {
            return Mapper.Map<Permission, PermissionModel>(model);
        }
    }

    }
