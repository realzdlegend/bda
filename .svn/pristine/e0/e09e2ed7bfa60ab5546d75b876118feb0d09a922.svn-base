﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using EnterpriseDB.Entities;
using System.Data;
using System.Xml.Serialization;
using System.IO;

namespace EnterpriseDB.Core
{
    abstract class BillProvider
    {
        protected string connString = Utility.ConnectionString;
        protected string bdaconnString = Utility.BDAConnectionString;
        protected int revenueId, systemId;
        protected string system;
        protected int disputeWindow = 0;
        protected int defaultLocationId = 0;

        public BillProvider(string system, string apiKey, string revenue)
        {
            try
            {
                this.system = system;
                systemId = (int)SqlHelper.ExecuteDataset(bdaconnString, "edb_GetSystem", system, apiKey).Tables[0].Rows[0]["Id"];
                DataTable rTable = SqlHelper.ExecuteDataset(bdaconnString, "edb_GetRevenue", revenue).Tables[0];
                revenueId = (int)rTable.Rows[0]["Id"];
                disputeWindow = (int)rTable.Rows[0]["DisputeWindow"];
                defaultLocationId = (int)rTable.Rows[0]["LocationId"];
            }
            catch(Exception ex)
            {
                throw new EDBException("Invalid apiKey or system or revenue code", ex, ExceptionType.Auth);
            }
        }

        public BillProvider(int systemId, int revenueId, int locationId, int disputeWindow)
        {

        }

        protected long InsertMainBill(string sourceId, int providerId, Bill bill, List<BillDetail> details)
        {
            SqlConnection conn = new SqlConnection(Utility.BDAConnectionString);
            conn.Open();
            using (SqlTransaction transaction = conn.BeginTransaction())
            {

                try
                {
                    object mainId = SqlHelper.ExecuteScalar(transaction, "InsertMain",
                        systemId, sourceId, providerId, revenueId, Utility.LocalTime);
                    bill.LocationId = bill.LocationId > 0 ? bill.LocationId : defaultLocationId;
                    bill.Status = (disputeWindow == 0) ? 2 : 1;
                    SqlHelper.ExecuteNonQuery(transaction, "edb_InsertBill",
                        mainId, bill.Amount, bill.LastEditDate, bill.Description,
                        bill.TransactionDate, bill.Currency, bill.LocationId, bill.Status, details.Count);

                    foreach (BillDetail bd in details)
                    {
                        SqlHelper.ExecuteNonQuery(transaction, "edb_InsertBillDetail",
                            bd.Amount, mainId, bd.Description, bd.LineNumber, 1);
                    }

                    transaction.Commit();
                    return long.Parse(mainId.ToString());
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    BDALogger.Log(ex);
                    return 0;
                }
            }
        }

        protected T Des<T>(string data)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                byte[] buffer = Encoding.UTF8.GetBytes(data);
                MemoryStream stream = new MemoryStream(buffer);

                T bill = (T)xs.Deserialize(stream);

                stream.Close();

                return bill;
            }
            catch (Exception ex)
            {
                throw new EDBException("Error reading data, data not in correct format", ex, ExceptionType.Data);
            }
        }

  

        public int GetLocationId(string code)
        {
            try
            {
                return (int)SqlHelper.ExecuteDataset(bdaconnString, "edb_GetLocation", code).Tables[0].Rows[0]["Id"];
            }
            catch
            {
                return 0;
            }
        }

        public int GetProviderId(string code)
        {
            try
            {
                return (int)SqlHelper.ExecuteDataset(bdaconnString,
                    "edb_GetOrganization", code).Tables[0].Rows[0]["Id"];
            }
            catch
            {
                throw new EDBException(string.Format("Service Provider with code {0} does not exist", code), ExceptionType.Data);
            }
        }

        public static BillProvider GetProvider(string host, string apiKey, string revenue)
        {
            if (revenue == Revenues.TSC)
                return new ManifestProvider(host, apiKey, revenue);
            else if (revenue == Revenues.FID)
                return new FIDManifestProvider(host, apiKey, revenue);
            else if (revenue == Revenues.OFC)
                return new OverflightProvider(host, apiKey, revenue);
            else if (revenue == Revenues.TNC)
                return new TerminalProvider(host, apiKey, revenue);
            else if (revenue == Revenues.FLC)
                return new FuelProvider(host, apiKey, revenue);
            else if (revenue == Revenues.POS || revenue == Revenues.COF)
                return new PosProvider(host, apiKey, revenue);
            else if (revenue == Revenues.CSC)//pp
                return new CargoSalesProvider(host, apiKey, revenue);
            else if (revenue == Revenues.FLC)
                return new FuelProvider(host, apiKey, revenue);
            else if (revenue == Revenues.POS || revenue == Revenues.COF)
                return new PosProvider(host, apiKey, revenue);
            else if (revenue == Revenues.OVF)
                return new OverflightProvider(host, apiKey, revenue);
            else if (revenue == Revenues.ECI)
                return new EnrouteProvider(host, apiKey, revenue);
            else if (revenue == Revenues.EBC)
                return new ExcessBaggageProvider(host, apiKey, revenue);
            else if (revenue == Revenues.ONC)
                return new OneCentChargeProvider(host, apiKey, revenue);
            else
                throw new EDBException("Invalid Revenue code", ExceptionType.Auth);
        }

        abstract public ResultList Process(string billData);
        abstract public DataSet GetBillData(int dataKey);
    }
}
