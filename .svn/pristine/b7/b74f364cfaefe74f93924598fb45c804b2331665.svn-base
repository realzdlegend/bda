﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using BDA.Domain.Entity;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Web.UI;
using BDA.Domain.Util;
using BDA.Search.Data;
using System.Web;
using System.Threading;

namespace BDA.Logic.logic
{
	public class ExportToExcel
	{
		public static byte[] DownloadSPSearchedBills(List<Bill> bills, NameValueCollection collection)
		{

			byte[] todownload = null;
			MemoryStream ms = new MemoryStream();
			try {
				string StartDate = collection["startDate"].ToString();
				string EndDate = collection["endDate"].ToString();
				string Revenue = collection["Revenue"].ToString();

				//start here
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
				Cell cell1 = (HSSFCell)sheet.CreateRow(1).CreateCell(1);
				CellStyle style = getStyle(workbook);
				cell1.SetCellValue("Search Result");
				//cell1.CellStyle = style;

				HSSFRow Row1 = (HSSFRow)sheet.CreateRow(2);
				Row1.CreateCell(1).SetCellValue("Start Date:");
				Row1.GetCell(1).CellStyle = style;
				Row1.CreateCell(2).SetCellValue(StartDate);

				HSSFRow Row2 = (HSSFRow)sheet.CreateRow(3);
				Row2.CreateCell(1).SetCellValue("End Date:");
				Row2.GetCell(1).CellStyle = style;
				Row2.CreateCell(2).SetCellValue(EndDate);

				HSSFRow Row3 = (HSSFRow)sheet.CreateRow(4);
				Row3.CreateCell(1).SetCellValue("Revenue:");
				Row3.GetCell(1).CellStyle = style;
				Row3.CreateCell(2).SetCellValue(Revenue);


				HSSFRow headerRow = (HSSFRow)sheet.CreateRow(6);
				headerRow.CreateCell(1).SetCellValue("S/N");
				headerRow.GetCell(1).CellStyle = style;
				headerRow.CreateCell(2).SetCellValue("Date Modified");
				headerRow.GetCell(2).CellStyle = style;
				headerRow.CreateCell(3).SetCellValue("Description");
				headerRow.GetCell(3).CellStyle = style;
				headerRow.CreateCell(4).SetCellValue("Amount");
				headerRow.GetCell(4).CellStyle = style;
				headerRow.CreateCell(5).SetCellValue("Type");
				headerRow.GetCell(5).CellStyle = style;
				headerRow.CreateCell(6).SetCellValue("Service Provider");
				headerRow.GetCell(6).CellStyle = style;
				//headerRow.CreateCell(7).SetCellValue("Ref. Number");
				//headerRow.GetCell(7).CellStyle = style;
				headerRow.CreateCell(7).SetCellValue("Status");
				headerRow.GetCell(7).CellStyle = style;


				HSSFRow valueRow = null;
				int count = 7;
				int sn = 0;
				foreach (Bill bill in bills) {
					++sn;
					++count;
					valueRow = (HSSFRow)sheet.CreateRow(count);
					valueRow.CreateCell(1).SetCellValue(sn);
					if (bill.LastEditDate != null)
						valueRow.CreateCell(2).SetCellValue(bill.LastEditDate.ToString());
					if (bill.Description != null)
						valueRow.CreateCell(3).SetCellValue(bill.Description.ToString());
					if (bill.Amount > 0)
						valueRow.CreateCell(4).SetCellValue(bill.Amount.ToString());
					if (bill.MasterList.Revenue != null)
						valueRow.CreateCell(5).SetCellValue(bill.MasterList.Revenue.ToString());
					if (bill.MasterList.ServiceProvider != null)
						valueRow.CreateCell(6).SetCellValue(bill.MasterList.ServiceProvider.ToString());
					//if (bill.ExtBillNumber != null)
					//valueRow.CreateCell(7).SetCellValue(bill.ExtBillNumber.ToString());
					if (bill.Status > 0)
						valueRow.CreateCell(7).SetCellValue(bill.Status.ToString());
				}
				workbook.Write(ms);
				ms.Flush();
				todownload = ms.ToArray();

			}
			catch (Exception ex) {

                ErrorLogger.Log(ex);
			}


			return todownload;
		}

		public static byte[] DownloadInvoices(IEnumerable<VersionInvoice> invoices,
            IList<BDA.Search.Data.InvoiceSummary> summary, NameValueCollection collection, Agency agency = null)
		{
		    string revenueGroup = "Revenue Group";
		    if (agency != null)
		    {
		        revenueGroup = agency.RevenueGroupTag ?? "Revenue Group";
		    }
		    byte[] todownload = null;
			var ms = new MemoryStream();
			try {
				string startDate = collection["startDate"];
				string endDate = collection["endDate"];
				string provider = collection["provider"];
				string sent = collection["sent"];
				string revenue = collection["Revenue"];
				string location = collection["location"];
				string currency = collection["currency"];
				string status = collection["status"];
				string invoiceNumber = collection["invoiceNumber"];
                string printDate = collection["printDate"];

                //start here
                HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
				sheet.AutoSizeColumn(3, true);
				sheet.DefaultColumnWidth = 15;
				var headerFont = (NPOI.SS.UserModel.Font)workbook.CreateFont();
				headerFont.Color = IndexedColors.WHITE.Index;
				headerFont.Boldweight = (short)25;
				headerFont.FontName = "Georgia";
				Cell cell1 = (HSSFCell)sheet.CreateRow(1).CreateCell(1);
				CellStyle style = getStyle1(workbook);
				cell1.SetCellValue("Search Result");

				//cell1.CellStyle.SetFont(headerFont);

				HSSFRow Row1 = (HSSFRow)sheet.CreateRow(2);
				Row1.CreateCell(1).SetCellValue("Start Date:");
				Row1.GetCell(1).CellStyle = style;
				Row1.CreateCell(2).SetCellValue(startDate);
				Row1.GetCell(2).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row1.CreateCell(4).SetCellValue("End Date:");
				Row1.GetCell(4).CellStyle = style;
				Row1.CreateCell(5).SetCellValue(endDate);
				Row1.GetCell(5).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row1.CreateCell(7).SetCellValue("Status:");
				Row1.GetCell(7).CellStyle = style;
				Row1.CreateCell(8).SetCellValue(status);
				Row1.GetCell(8).CellStyle.Alignment = HorizontalAlignment.LEFT;

				HSSFRow Row2 = (HSSFRow)sheet.CreateRow(3);
				Row2.CreateCell(1).SetCellValue("Location:");
				Row2.GetCell(1).CellStyle = style;
				Row2.CreateCell(2).SetCellValue(location);
				Row2.GetCell(2).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row2.CreateCell(4).SetCellValue("Customer:");
				Row2.GetCell(4).CellStyle = style;
				Row2.CreateCell(5).SetCellValue(provider);
				Row2.GetCell(5).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row2.CreateCell(7).SetCellValue("Invoice Number:");
				Row2.GetCell(7).CellStyle = style;
				Row2.CreateCell(8).SetCellValue(invoiceNumber);
				Row2.GetCell(8).CellStyle.Alignment = HorizontalAlignment.LEFT;

               


                HSSFRow Row3 = (HSSFRow)sheet.CreateRow(4);
				Row3.CreateCell(1).SetCellValue("Revenue:");
				Row3.GetCell(1).CellStyle = style;
				Row3.CreateCell(2).SetCellValue(revenue);

				//Row3.CreateCell(4).SetCellValue("Currency:");
				//Row3.GetCell(4).CellStyle = style;
				//Row3.CreateCell(5).SetCellValue(currency);

				Row3.CreateCell(4).SetCellValue("Sent:");
				Row3.GetCell(4).CellStyle = style;
				Row3.CreateCell(5).SetCellValue(sent);

                Row3.CreateCell(7).SetCellValue("Print Date");
                Row3.GetCell(7).CellStyle = style;
                Row3.CreateCell(8).SetCellValue(printDate);


                HSSFRow RowH = (HSSFRow)sheet.CreateRow(7);
				RowH.CreateCell(5).SetCellValue("Summary");
				RowH.GetCell(5).CellStyle = style;
				RowH.GetCell(5).CellStyle.SetFont(headerFont);
				var summaryHeader = (HSSFRow)sheet.CreateRow(8);
				summaryHeader.CreateCell(3).SetCellValue("Currency");
				summaryHeader.GetCell(3).CellStyle = style;
				summaryHeader.CreateCell(4).SetCellValue("Count");
				summaryHeader.GetCell(4).CellStyle = style;
				summaryHeader.CreateCell(5).SetCellValue("Amount Invoiced");
				summaryHeader.GetCell(5).CellStyle = style;
				summaryHeader.CreateCell(6).SetCellValue("Amount Paid");
				summaryHeader.GetCell(6).CellStyle = style;
				summaryHeader.CreateCell(7).SetCellValue("Amount Voided");
				summaryHeader.GetCell(7).CellStyle = style;
				int count = 8;
				foreach (var s in summary) {
					++count;
					HSSFRow valueRow = (HSSFRow)sheet.CreateRow(count);
					valueRow.CreateCell(3).SetCellValue(s.Currency ?? "");
					valueRow.CreateCell(4).SetCellValue(s.Count.ToString("#,###"));
					valueRow.CreateCell(5).SetCellValue(s.Amount.ToString("#,###.#0"));
					valueRow.CreateCell(6).SetCellValue(s.Paid.ToString("#,###.#0"));
					valueRow.CreateCell(7).SetCellValue(s.Voided.ToString("#,###.#0"));
				}
				count = count + 3;
				HSSFRow headerRow = (HSSFRow)sheet.CreateRow(count);
				headerRow.CreateCell(1).SetCellValue("S/N");
				headerRow.GetCell(1).CellStyle = style;
				headerRow.CreateCell(2).SetCellValue("Invoice");
				headerRow.GetCell(2).CellStyle = style;
				headerRow.CreateCell(3).SetCellValue("Status");
				headerRow.GetCell(3).CellStyle = style;
				headerRow.CreateCell(4).SetCellValue("Date");
				headerRow.GetCell(4).CellStyle = style;
				headerRow.CreateCell(5).SetCellValue("Amount Invoiced");
				headerRow.GetCell(5).CellStyle = style;
				headerRow.CreateCell(6).SetCellValue("Amount Paid");
				headerRow.GetCell(6).CellStyle = style;
				headerRow.CreateCell(7).SetCellValue("Customer");
				headerRow.GetCell(7).CellStyle = style;
				headerRow.CreateCell(8).SetCellValue("Name");
				headerRow.GetCell(8).CellStyle = style;
				headerRow.CreateCell(9).SetCellValue("Revenue");
				headerRow.GetCell(9).CellStyle = style;
				headerRow.CreateCell(10).SetCellValue(revenueGroup);
				headerRow.GetCell(10).CellStyle = style;
                headerRow.CreateCell(11).SetCellValue("Location");
                headerRow.GetCell(11).CellStyle = style;
                headerRow.CreateCell(12).SetCellValue("State");
			    headerRow.GetCell(12).CellStyle = style;

				int sn = 0;
				try {

				foreach (var invoice in invoices) {
					++count;
					++sn;
					HSSFRow valueRow = (HSSFRow)sheet.CreateRow(count);
					valueRow.CreateCell(1).SetCellValue(sn);
					valueRow.GetCell(1).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(2).SetCellValue(invoice.InvoiceNumber);
					valueRow.GetCell(2).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(3).SetCellValue(invoice.Status);
					valueRow.GetCell(3).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(4).SetCellValue(invoice.InvoiceDate.ToString("dd MMM yyyy"));
					valueRow.GetCell(4).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(5).SetCellValue(invoice.AmountString);
					valueRow.GetCell(5).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(6).SetCellValue(invoice.AmountPaidString);
					valueRow.GetCell(6).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(7).SetCellValue(invoice.Customer);
					valueRow.GetCell(7).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(8).SetCellValue(invoice.CustomerName);
					valueRow.GetCell(8).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(9).SetCellValue(invoice.Revenue.ToString());
					valueRow.GetCell(9).CellStyle.Alignment = HorizontalAlignment.CENTER;
				    if (string.IsNullOrWhiteSpace(invoice.RevenueGroup))
				    {
                        valueRow.CreateCell(10)
                        .SetCellValue("-");
				    }
				    else
				    {
                        valueRow.CreateCell(10)
                        .SetCellValue(string.Format("{0} - ({1})", invoice.RevenueGroupName, invoice.RevenueGroup));
				    }
					
					valueRow.GetCell(10).CellStyle.Alignment = HorizontalAlignment.CENTER;
                    valueRow.CreateCell(11).SetCellValue(invoice.LocationCode);
                    valueRow.GetCell(11).CellStyle.Alignment = HorizontalAlignment.CENTER;
                    valueRow.CreateCell(12).SetCellValue(invoice.LocationState);
                    valueRow.GetCell(12).CellStyle.Alignment = HorizontalAlignment.CENTER;


                    
				}
				}
				catch (Exception e) {
					//something went wrong
					string msg = e.Message;
                    ErrorLogger.Log(e);
				}
				workbook.Write(ms);
				ms.Flush();
				todownload = ms.ToArray();
			}
			catch (Exception ex) {
                ErrorLogger.Log(ex);
			}
			return todownload;
		}

		public static byte[] DownloadInvoices(IList<InvoiceView> invoices, IList<InvoiceRepository.InvoiceSummary> summary, NameValueCollection collection)
		{
			byte[] todownload = null;
			var ms = new MemoryStream();
			try {
				string startDate = collection["startDate"];
				string endDate = collection["endDate"];
				string provider = collection["provider"];
				string sent = collection["sent"];
				string revenue = collection["Revenue"];
				string location = collection["location"];
				string currency = collection["currency"];
				string status = collection["status"];
				string invoiceNumber = collection["invoiceNumber"];

				//start here
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
				sheet.AutoSizeColumn(3, true);
				sheet.DefaultColumnWidth = 15;
				var headerFont = (NPOI.SS.UserModel.Font)workbook.CreateFont();
				headerFont.Color = IndexedColors.WHITE.Index;
				headerFont.Boldweight = (short)25;
				headerFont.FontName = "Georgia";
				Cell cell1 = (HSSFCell)sheet.CreateRow(1).CreateCell(1);
				CellStyle style = getStyle1(workbook);
				cell1.SetCellValue("Search Result");

				//cell1.CellStyle.SetFont(headerFont);

				HSSFRow Row1 = (HSSFRow)sheet.CreateRow(2);
				Row1.CreateCell(1).SetCellValue("Start Date:");
				Row1.GetCell(1).CellStyle = style;
				Row1.CreateCell(2).SetCellValue(startDate);
				Row1.GetCell(2).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row1.CreateCell(4).SetCellValue("End Date:");
				Row1.GetCell(4).CellStyle = style;
				Row1.CreateCell(5).SetCellValue(endDate);
				Row1.GetCell(5).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row1.CreateCell(7).SetCellValue("Status:");
				Row1.GetCell(7).CellStyle = style;
				Row1.CreateCell(8).SetCellValue(status);
				Row1.GetCell(8).CellStyle.Alignment = HorizontalAlignment.LEFT;

				HSSFRow Row2 = (HSSFRow)sheet.CreateRow(3);
				Row2.CreateCell(1).SetCellValue("Location:");
				Row2.GetCell(1).CellStyle = style;
				Row2.CreateCell(2).SetCellValue(location);
				Row2.GetCell(2).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row2.CreateCell(4).SetCellValue("Customer:");
				Row2.GetCell(4).CellStyle = style;
				Row2.CreateCell(5).SetCellValue(provider);
				Row2.GetCell(5).CellStyle.Alignment = HorizontalAlignment.LEFT;

				Row2.CreateCell(7).SetCellValue("Invoice Number:");
				Row2.GetCell(7).CellStyle = style;
				Row2.CreateCell(8).SetCellValue(invoiceNumber);
				Row2.GetCell(8).CellStyle.Alignment = HorizontalAlignment.LEFT;


				HSSFRow Row3 = (HSSFRow)sheet.CreateRow(4);
				Row3.CreateCell(1).SetCellValue("Revenue:");
				Row3.GetCell(1).CellStyle = style;
				Row3.CreateCell(2).SetCellValue(revenue);

				Row3.CreateCell(4).SetCellValue("Currency:");
				Row3.GetCell(4).CellStyle = style;
				Row3.CreateCell(5).SetCellValue(currency);

				Row3.CreateCell(4).SetCellValue("Sent:");
				Row3.GetCell(4).CellStyle = style;
				Row3.CreateCell(5).SetCellValue(sent);


				HSSFRow RowH = (HSSFRow)sheet.CreateRow(7);
				RowH.CreateCell(5).SetCellValue("Summary");
				RowH.GetCell(5).CellStyle = style;
				RowH.GetCell(5).CellStyle.SetFont(headerFont);
				var summaryHeader = (HSSFRow)sheet.CreateRow(8);
				summaryHeader.CreateCell(3).SetCellValue("Currency");
				summaryHeader.GetCell(3).CellStyle = style;
				summaryHeader.CreateCell(4).SetCellValue("Count");
				summaryHeader.GetCell(4).CellStyle = style;
				summaryHeader.CreateCell(5).SetCellValue("Amount Invoiced");
				summaryHeader.GetCell(5).CellStyle = style;
				summaryHeader.CreateCell(6).SetCellValue("Amount Paid");
				summaryHeader.GetCell(6).CellStyle = style;
				summaryHeader.CreateCell(7).SetCellValue("Amount Voided");
				summaryHeader.GetCell(7).CellStyle = style;
				int count = 8;
				foreach (var s in summary) {
					++count;
					HSSFRow valueRow = (HSSFRow)sheet.CreateRow(count);
					valueRow.CreateCell(3).SetCellValue(s.Currency ?? "");
					valueRow.CreateCell(4).SetCellValue(s.Count.ToString("#,###"));
					valueRow.CreateCell(5).SetCellValue(s.Amount.ToString("#,###.#0"));
					valueRow.CreateCell(6).SetCellValue(s.Paid.ToString("#,###.#0"));
					valueRow.CreateCell(7).SetCellValue(s.Voided.ToString("#,###.#0"));
				}
				count = count + 3;
				HSSFRow headerRow = (HSSFRow)sheet.CreateRow(count);
				headerRow.CreateCell(1).SetCellValue("S/N");
				headerRow.GetCell(1).CellStyle = style;
				headerRow.CreateCell(2).SetCellValue("Invoice");
				headerRow.GetCell(2).CellStyle = style;
				headerRow.CreateCell(3).SetCellValue("Status");
				headerRow.GetCell(3).CellStyle = style;
				headerRow.CreateCell(4).SetCellValue("Date");
				headerRow.GetCell(4).CellStyle = style;
				headerRow.CreateCell(5).SetCellValue("Amount Invoiced");
				headerRow.GetCell(5).CellStyle = style;
				headerRow.CreateCell(6).SetCellValue("Amount Paid");
				headerRow.GetCell(6).CellStyle = style;
				headerRow.CreateCell(7).SetCellValue("Customer");
				headerRow.GetCell(7).CellStyle = style;
				headerRow.CreateCell(8).SetCellValue("Name");
				headerRow.GetCell(8).CellStyle = style;
				headerRow.CreateCell(9).SetCellValue("Revenue");
				headerRow.GetCell(9).CellStyle = style;
				headerRow.CreateCell(10).SetCellValue("Location");
				headerRow.GetCell(10).CellStyle = style;

				int sn = 0;
				foreach (var invoice in invoices) {
					++count;
					++sn;
					HSSFRow valueRow = (HSSFRow)sheet.CreateRow(count);
					valueRow.CreateCell(1).SetCellValue(sn);
					valueRow.GetCell(1).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(2).SetCellValue(invoice.InvoiceNumber);
					valueRow.GetCell(2).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(3).SetCellValue(invoice.Status);
					valueRow.GetCell(3).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(4).SetCellValue(invoice.InvoiceDate.ToString("dd MMM yyyy"));
					valueRow.GetCell(4).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(5).SetCellValue(invoice.AmountString);
					valueRow.GetCell(5).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(6).SetCellValue(invoice.AmountPaidString);
					valueRow.GetCell(6).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(7).SetCellValue(invoice.Provider.ToString());
					valueRow.GetCell(7).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(8).SetCellValue(invoice.Name.ToString());
					valueRow.GetCell(8).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(9).SetCellValue(invoice.Revenue.ToString());
					valueRow.GetCell(9).CellStyle.Alignment = HorizontalAlignment.CENTER;
					valueRow.CreateCell(10).SetCellValue(invoice.LocationCode);
					valueRow.GetCell(10).CellStyle.Alignment = HorizontalAlignment.CENTER;
				}
				workbook.Write(ms);
				ms.Flush();
				todownload = ms.ToArray();
			}
			catch(Exception ex) {
                ErrorLogger.Log(ex);
			}
			return todownload;
		}



		public static byte[] DownloadAgencySearchedBills(List<Bill> bills, NameValueCollection collection)
		{

			byte[] todownload = null;
			MemoryStream ms = new MemoryStream();
			try {
				string StartDate = collection["startDate"].ToString();
				string EndDate = collection["endDate"].ToString();
				string Revenue = collection["Revenue"].ToString();

				//start here
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("Sheet1");
				Cell cell1 = (HSSFCell)sheet.CreateRow(1).CreateCell(1);
				CellStyle style = getStyle(workbook);
				cell1.SetCellValue("Search Result");
				//cell1.CellStyle = style;

				HSSFRow Row1 = (HSSFRow)sheet.CreateRow(2);
				Row1.CreateCell(1).SetCellValue("Start Date:");
				Row1.GetCell(1).CellStyle = style;
				Row1.CreateCell(2).SetCellValue(StartDate);

				HSSFRow Row2 = (HSSFRow)sheet.CreateRow(3);
				Row2.CreateCell(1).SetCellValue("End Date:");
				Row2.GetCell(1).CellStyle = style;
				Row2.CreateCell(2).SetCellValue(EndDate);

				HSSFRow Row3 = (HSSFRow)sheet.CreateRow(4);
				Row3.CreateCell(1).SetCellValue("Revenue:");
				Row3.GetCell(1).CellStyle = style;
				Row3.CreateCell(2).SetCellValue(Revenue);


				HSSFRow headerRow = (HSSFRow)sheet.CreateRow(6);
				headerRow.CreateCell(1).SetCellValue("S/N");
				headerRow.GetCell(1).CellStyle = style;
				headerRow.CreateCell(2).SetCellValue("Date Modified");
				headerRow.GetCell(2).CellStyle = style;
				headerRow.CreateCell(3).SetCellValue("Revenue Type");
				headerRow.GetCell(3).CellStyle = style;
				headerRow.CreateCell(4).SetCellValue("Amount");
				headerRow.GetCell(4).CellStyle = style;
				headerRow.CreateCell(5).SetCellValue("Transaction Date");
				headerRow.GetCell(5).CellStyle = style;
				//headerRow.CreateCell(6).SetCellValue("Ref. Number");
				//headerRow.GetCell(6).CellStyle = style;
				headerRow.CreateCell(6).SetCellValue("Status");
				headerRow.GetCell(6).CellStyle = style;
				//headerRow.CreateCell(8).SetCellValue("Status");
				//headerRow.GetCell(8).CellStyle = style;


				HSSFRow valueRow = null;
				int count = 7;
				int sn = 0;
				foreach (Bill bill in bills) {
					++sn;
					++count;
					valueRow = (HSSFRow)sheet.CreateRow(count);
					valueRow.CreateCell(1).SetCellValue(sn);
					if (bill.LastEditDate != null)
						valueRow.CreateCell(2).SetCellValue(bill.LastEditDate.ToString());
					if (bill.Description != null)
						valueRow.CreateCell(3).SetCellValue(bill.Description);
					if (bill.Amount > 0)
						valueRow.CreateCell(4).SetCellValue(bill.Amount.ToString());
					if (bill.TransactionDate != null)
						valueRow.CreateCell(5).SetCellValue(bill.TransactionDate.ToString());
					//if (bill.Organization != null)
					//    valueRow.CreateCell(6).SetCellValue(bill.Organization.ToString());
					//if (bill.ExtBillNumber != null)
					//    valueRow.CreateCell(6).SetCellValue(bill.ExtBillNumber.ToString());
					if (bill.Status > 0)
						valueRow.CreateCell(6).SetCellValue(bill.Status.ToString());
				}
				workbook.Write(ms);
				ms.Flush();
				todownload = ms.ToArray();

			}
			catch (Exception ex) {
                ErrorLogger.Log(ex);
			}


			return todownload;
		}
		public static CellStyle getStyle(HSSFWorkbook workbook)
		{
			// Create font to use.
			NPOI.SS.UserModel.Font font = (NPOI.SS.UserModel.Font)workbook.CreateFont();
			font.Color = IndexedColors.WHITE.Index;
			font.Boldweight = (short)20;
			font.FontName = "Tahoma";
			//font.Color = IndexedColors.WHITE.Index;


			// Create cell style.
			CellStyle cellstyle = (NPOI.SS.UserModel.CellStyle)workbook.CreateCellStyle();
			cellstyle.FillBackgroundColor = IndexedColors.BLUE.Index;
			cellstyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
			cellstyle.FillForegroundColor = IndexedColors.DARK_BLUE.Index;
			cellstyle.WrapText = true;
			cellstyle.SetFont(font);
			return cellstyle;
		}

		public static CellStyle getStyle1(HSSFWorkbook workbook)
		{
			// Create font to use.
			NPOI.SS.UserModel.Font font = (NPOI.SS.UserModel.Font)workbook.CreateFont();
			font.Color = IndexedColors.WHITE.Index;
			font.Boldweight = (short)20;
			font.FontName = "Georgia";
			//font.Color = IndexedColors.WHITE.Index;


			// Create cell style.
			CellStyle cellstyle = (NPOI.SS.UserModel.CellStyle)workbook.CreateCellStyle();
			cellstyle.FillBackgroundColor = IndexedColors.BLUE.Index;
			cellstyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
			cellstyle.FillForegroundColor = IndexedColors.DARK_BLUE.Index;
			cellstyle.WrapText = true;
			cellstyle.SetFont(font);
			cellstyle.Alignment = HorizontalAlignment.CENTER;
			return cellstyle;
		}

		public static void Download(Page page, byte[] file, string title, FileType filetype)
		{
            try
            {
                string ext = "xls";
                page.Response.Clear();
                switch (filetype)
                {

                    case FileType.Excel:
                        ext = "xls";
                        page.Response.ContentType = "application/vnd.ms-excel";
                        break;
                    case FileType.Pdf:
                        ext = "pdf";
                        page.Response.ContentType = "application/pdf";
                        break;
                    case FileType.Excel2007:
                        ext = "xls";
                        page.Response.ContentType = "application/ms-excel";
                        break;
                    case FileType.CSV:
                        ext = "csv";
                        page.Response.ContentType = "application/csv";
                        break;
                }
                if (file != null)
                {
                    string fname = string.IsNullOrEmpty(title) ? "Report" : title;
                    page.Response.AddHeader("Content-Disposition", "attachment; filename =\"" + fname + "." + ext + "\"");
                    page.Response.BinaryWrite(file);

                    //page.Response.OutputStream.Write(file, 0, file.Length);
                    page.Response.Flush();
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                    page.Response.End();
                }
            }           
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                throw;
            }
			


		}
	}
}
