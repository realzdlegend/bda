﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using BDA.Logic.Helpers;
using BDA.Logic.logic;

namespace BDA.Services
{
    public class ApiTokenHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                string requestUri = request.RequestUri.ToString().ToLower();
                if (requestUri.Contains(@"/api/open/"))
                    return base.SendAsync(request, cancellationToken);
                //if (requestUri.Contains(@"/api/payment/syncfidesic"))
                //    return base.SendAsync(request, cancellationToken);
                //if (requestUri.Contains(@"/api/gcm/"))
                //    return base.SendAsync(request, cancellationToken);
                //if (requestUri.Contains(@"/api/payment/notify"))
                //    return base.SendAsync(request, cancellationToken);

                // permit server to handle options request
                if (request.Method == HttpMethod.Options)
                {
                    return base.SendAsync(request, cancellationToken).ContinueWith(
                        task =>
                        {
                            var response = task.Result;
                            response.StatusCode= HttpStatusCode.OK;
                            return response;
                        });
                }

                var apiCode = request.Headers.Contains("ApiCode") ? request.Headers.GetValues("ApiCode").FirstOrDefault() : "";
                var apiKey = request.Headers.Contains("ApiKey") ? request.Headers.GetValues("ApiKey").FirstOrDefault() : "";
                var userCode = request.Headers.Contains("UserCode") ? request.Headers.GetValues("UserCode").FirstOrDefault() : "";

                //validate API Code and Key
                if (string.IsNullOrEmpty(apiCode) || string.IsNullOrEmpty(apiKey))
                    throw new ArgumentException("Invalid ApiCode or ApiKey");

                int result = 0;

                string dbConn = ConfigurationManager.ConnectionStrings["bdalog"].ToString();
                using (SqlConnection connection = new SqlConnection(dbConn))
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "EXECUTE VerifyApi @apiCode, @apiKey, @userId";

                    command.Parameters.AddWithValue("@apiCode", apiCode);
                    command.Parameters.AddWithValue("@apiKey", apiKey);
                    command.Parameters.AddWithValue("@userId", userCode);
                    connection.Open();
                    object rObj = command.ExecuteScalar();
                    result = rObj == null ? 0 : int.Parse(rObj.ToString());
                }

                if (result == 0) throw new ArgumentException("ApiCode and ApiKey mismatch");

                if (requestUri.Contains(@"/api/auth/create"))
                    return base.SendAsync(request, cancellationToken);

                //validate User Code
                Guid userKey = Guid.Empty;

                if (Guid.TryParse(userCode, out userKey))
                {
                    if (!new AuthorizationRepository().ValidateUserKey(userKey))
                        throw new ArgumentException("Failed Authentication for User Code");
                }
                else
                    throw new ArgumentException("Invalid user code");
                

                return base.SendAsync(request, cancellationToken);
            }
            catch (ArgumentException ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                ErrorLogger.LogToStackify(ex);
                var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                response.ReasonPhrase = ex.Message;
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);
                return tsc.Task;
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                ErrorLogger.LogToStackify(ex);
                var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                string exMessage = AppConstants.ServerError;
                response.ReasonPhrase = exMessage;
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);
                return tsc.Task;

            }
        }
    }
}