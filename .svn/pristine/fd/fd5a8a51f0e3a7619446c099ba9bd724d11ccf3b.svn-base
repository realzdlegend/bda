﻿using BDA.Domain.Models;
using BDA.Domain.Util.ApiSettings;
using CommonLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.Client.logic
{
   public  class RevenueRepository : BaseRepository
    {
        RevenueAPI reveueAPI = new RevenueAPI();

        public IEnumerable<SettlementAccountModel> GetSettlementAccounts()
        {
            try
            {
                return Post<IEnumerable<SettlementAccountModel>>(null,reveueAPI.SettlementAccounts , "GET").Data;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                throw;
            }

        }

       public IEnumerable<BankModel> GetBanks()
       {
            try
            {
                return Post<IEnumerable<BankModel>>(null, reveueAPI.GetBanks , "GET").Data;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                throw;
            }
       }

        public bool CreateServiceType(RemitaServiceTypeModel model, Guid userId)
        {
            try
            {
                AddUserIdHeader(userId);
                var result = Post<int>(model, string.Format(reveueAPI.CreateSettlementAccount));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return false;
            }
        }

        public bool CreateSettlementAccount(SettlementAccountModel model, Guid userId)
        {
            try
            {
                AddUserIdHeader(userId);
                var result = Post<int>(model, string.Format(reveueAPI.CreateSettlementAccount));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return false;
            }
        }


        public bool UpdateServiceType(int id, RemitaServiceTypeModel model, Guid userId)
        {
            try
            {

                AddUserIdHeader(userId);
                var result = Post<RemitaServiceTypeModel>(model, string.Format(reveueAPI.UpdateServiceType, id));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return false;
            }
        }


        public bool UpdateSettlementAccount(int id, SettlementAccountModel model, Guid userId)
        {
            try
            {

                AddUserIdHeader(userId);
                var result = Post<SettlementAccountModel>(model, string.Format(reveueAPI.UpdateSettlementAccount, id));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);
                
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return false;
            }
        }

        public RevenueModel Revenue(int id)
        {
            try
            {
        
                var result = Post<RevenueModel>(null, string.Format(reveueAPI.GetRevenue, id), "GET");
                if (result.ResponseCode == "00")
                    return result.Data;
                else
                    throw new Exception(result.ResponseMessage);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return null;
            }
        }

        public IEnumerable<RevenueBeneficiaryModel> GetRevenueBeneficiaries(int revId)
        {
            try
            {

                var result = Post<IEnumerable<RevenueBeneficiaryModel>>(null, string.Format(reveueAPI.RevenueBeneficiaries, revId),"GET");
                if (result.ResponseCode == "00")
                    return result.Data;
                else
                    throw new Exception(result.ResponseMessage);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return null;
            }
        }

        public bool CreateRevenueBeneficiary(RevenueBeneficiaryModel model, Guid userId)
        {
            try
            {
                AddUserIdHeader(userId);
                var result = Post<int>(model, string.Format(reveueAPI.CreateRevenueBeneficiary));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return false;
            }
        }

        public bool UpdateRevenueBeneficiary(RevenueBeneficiaryModel model, Guid userId, int id)
        {
            try
            {
                AddUserIdHeader(userId);
                var result = Post<RevenueBeneficiaryModel>(model, string.Format(reveueAPI.UpdateRevenueBeneficiary, id));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return false;
            }
        }

        public bool RemoveRevenueBeneficiary(int id, Guid userId)
        {
            try
            {
                AddUserIdHeader(userId);
                var result = Post<int>(null, string.Format(reveueAPI.RemoveRevenueBeneficiary, id));
                if (result.ResponseCode == "00")
                    return true;
                else
                    throw new Exception(result.ResponseMessage);


            }
            catch (Exception ex)
            {

                ErrorLogger.Log(ex);
                return false;
            }
        }

        public IEnumerable<RemitaServiceTypeModel> GetAgencyServiceTypes(int agencyId)
        {
            try
            {

                var result = Post<IEnumerable<RemitaServiceTypeModel>>(null, string.Format(reveueAPI.GetAgencyServiceTypes, agencyId), "GET");
                if (result.ResponseCode == "00")
                    return result.Data;
                else
                    throw new Exception(result.ResponseMessage);


            }
            catch (Exception ex)
            {

                ErrorLogger.Log(ex);
                return null;
            }
        }


        public RemitaServiceTypeModel GetServiceType(int accountNumber, int agency)
        {
            try
            {

                var result = Post<RemitaServiceTypeModel>(null, string.Format(reveueAPI.GetServiceType, accountNumber, agency), "GET");
                if (result.ResponseCode == "00")
                    return result.Data;
                else
                    throw new Exception(result.ResponseMessage);


            }
            catch (Exception ex)
            {

                ErrorLogger.Log(ex);
                return null;
            }
        }

        public SettlementAccountModel GetAccount(string accountNumber)
        {
            try
            {
              
                var result = Post<SettlementAccountModel>(null, string.Format(reveueAPI.GetAccount, accountNumber),"GET");
                if (result.ResponseCode == "00")
                    return result.Data;
                else
                    throw new Exception(result.ResponseMessage);


            }
            catch (Exception ex)
            {

                ErrorLogger.Log(ex);
                return null;
            }
        }
    }
}
