﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.Search.Data
{
    [ElasticsearchType(Name = "invoices", IdProperty = "InvoiceId")]
	public class VersionInvoice
	{
        [Number(Name = "Invoice Id", IncludeInAll = false)]
        public virtual long InvoiceId { get; set; }

        [String(Name = "CreatedBy Name", Index = FieldIndexOption.NotAnalyzed)]
        public virtual string CreatedByName { get; set; }

        [String(Name = "CreatedBy Email", Index = FieldIndexOption.NotAnalyzed)]
        public virtual string CreatedByEmail { get; set; }

        [String(Name = "Invoice Number", Index = FieldIndexOption.NotAnalyzed)]
		public virtual string InvoiceNumber { get; set; }

        [String(Name = "InvoiceNumber_Analyzed", Analyzer = "autocomplete")]
		public virtual string InvoiceNumber_Analyzed { get; set; }

        [Date(Name = "Invoice Date")]
		public virtual DateTime InvoiceDate { get; set; }

        [Number(Name = "Amount")]
		public virtual decimal Amount { get; set; }

        [Number(Name = "Customer Id", IncludeInAll = false)]
		public virtual int CustomerId { get; set; }

        [String(Name = "Customer", Index = FieldIndexOption.NotAnalyzed)]
		public virtual string Customer { get; set; }

        [String(Name = "Customer_Analyzed", Analyzer = "autocomplete")]
		public virtual string Customer_Analyzed { get; set; }

        [String(Name = "Customer Number", Index = FieldIndexOption.NotAnalyzed)]
		public virtual string CustomerAIN { get; set; }

        [String(Name = "Customer Name", Analyzer = "autocomplete")]
		public virtual string CustomerName { get; set; }



        [String(Name = "Customer Phone", IncludeInAll = false)]
        public virtual string CustomerPhone { get; set; }

        [String(Name = "Customer Address", IncludeInAll = false)]
        public virtual string CustomerAddress { get; set; }

        //[Nested(Name = "Customer Emails")] //this worked locally
        [String(Name = "Customer Emails")]
        public virtual List<string> CustomerEmails { get; set; }



        [Number(Name = "Customer Parent Id", IncludeInAll = false)]
        public virtual int CustomerParentId { get; set; }

        [String(Name = "CustomerParent")]
        public virtual string CustomerParent { get; set; }

        [String(Name = "CustomerParent_Analyzed", Analyzer = "autocomplete")]
        public virtual string CustomerParent_Analyzed { get; set; }

        [String(Name = "Customer Parent Number")]
        public virtual string CustomerParentAIN { get; set; }


        [Number(Name = "Agency Id", IncludeInAll = false)]
		public virtual int AgencyId { get; set; }

        [String(Name = "Agency", Index = FieldIndexOption.NotAnalyzed)]
		public virtual string Agency { get; set; }

        [String(Name = "Agency Code", Index = FieldIndexOption.NotAnalyzed)]
		public virtual string AgencyCode { get; set; }

        [Number(Name = "Revenue Id", IncludeInAll = false)]
		public virtual int RevenueId { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Revenue")]
		public virtual string Revenue { get; set; }

        [String(Name = "Revenue_Analyzed", Analyzer = "autocomplete")]
		public virtual string Revenue_Analyzed { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Revenue Code")]
		public virtual string RevenueCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Revenue Group")]
	    public virtual string RevenueGroup { get; set; }

        [String(Name = "Revenue Group Name", Analyzer = "autocomplete")]
        public virtual string RevenueGroupName { get; set; }

        [Number(Name = "Payment Prediction", IncludeInAll = false)]
		public virtual decimal? PaymentPrediction { get; set; }

        [Number(Name = "Currency Id", IncludeInAll = false)]
		public virtual int CurrencyId { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Currency")]
		public virtual string Currency { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Currency Code")]
		public virtual string CurrencyCode { get; set; }

        [Number(Name = "Currency UTF32 Code", IncludeInAll = false)]
		public virtual int CurrencyUTF32Code { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Location")]
		public virtual string Location { get; set; }

        [String(Name = "Location_Analyzed", Analyzer = "autocomplete")]
		public virtual string Location_Analyzed { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Location Code")]
		public virtual string LocationCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "State")]
		public virtual string LocationState { get; set; }


        [String(Index = FieldIndexOption.NotAnalyzed,Name = "LocationParent")]
        public virtual string LocationParent { get; set; }

        [String(Name = "LocationParent_Analyzed", Analyzer = "autocomplete")]
        public virtual string LocationParent_Analyzed { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Location Parent Code")]
        public virtual string LocationParentCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Parent State")]
        public virtual string LocationParentState { get; set; }
 

        [String(Name = "Location Parent Id", IncludeInAll = false)]
        public virtual int LocationParentId { get; set; }



        [String(Name = "Address", Analyzer = "autocomplete")]
		public virtual string Address { get; set; }

        [Number(Name = "Location Id", IncludeInAll = false)]
		public virtual int LocationId { get; set; }
        [Boolean(Name = "Deleted", DocValues = true)]
        public virtual bool Deleted { get; set; }

        [Number(Name = "Payment Count", IncludeInAll = false)]
		public virtual int PaymentCount { get; set; }

        [Number(Name = "Amount Paid", IncludeInAll = false)]
		public virtual decimal AmountPaid { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Payment Status")]
		public virtual string PaymentStatus { get; set; }

        [Date(Name = "Payment Date", IncludeInAll = false)]
		public virtual DateTime PaymentDate { get; set; }

        [Date(Name = "Created Date")]
		public virtual DateTime CreatedDate { get; set; }

        [Date(Name = "Due Date", IncludeInAll = false)]
		public virtual DateTime DueDate { get; set; }

        [Number(Name = "Fidesic Invoice Id", IncludeInAll = false)]
		public virtual int ReturnedInvoiceId { get; set; }

        [Nested(Name = "Bills", IncludeInAll = false)]
        public virtual IList<VersionBill> Bills { get; set; }

        [Number(Name = "Bill Count", IncludeInAll = false)]
		public virtual int BillCount { get; set; }

        public virtual string RemitaRetrievalReference { get; set; }

        [Boolean(Ignore = true)]
		public virtual bool FullyPaid
		{
			get
			{
				return AmountPaid >= Amount;
			}
		}

        [String(Ignore = true)]
		public virtual string AmountString
		{
			get
			{
				string code = char.ConvertFromUtf32(CurrencyUTF32Code);
				return string.Format("{0} {1}", code, Amount.ToString("#,###.#0"));
			}
		}

        [String(Ignore = true)]
		public virtual string AmountPaidString
		{
			get
			{

				string code = char.ConvertFromUtf32(CurrencyUTF32Code);
				return string.Format("{0} {1}", code, AmountPaid.ToString("#,###.#0"));
			}
		}

        [String(Ignore = true)]
        public virtual string Status
        {
            get
            {
                return Deleted ? "Voided" : PaymentStatus;
            }
        }


        [Number(Name = "Amount Outstanding")]
        public decimal AmountOutstanding { get; set; }
    }

	public enum PaymentStatus
	{
		Paid,
		Unpaid,
		Partial
	}
}
