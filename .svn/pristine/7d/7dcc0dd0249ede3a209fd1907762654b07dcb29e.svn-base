﻿using AutoMapper;
using BDA.Domain.Entity;
using BDA.Logic.Helpers;
using BDA.Logic.logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using BDA.Domain.Models;

namespace BDA.Services.Controllers
{
    public class RevenueController : BaseController
    {
        readonly RevenueRepository revRepo = new RevenueRepository();
        readonly BankRepository bankRepo = new BankRepository();
        private readonly InvoiceRepository _invoiceRepository = new InvoiceRepository();
        // GET api/revenue
        public HttpResponseMessage Get(int agencyId, int startIndex, int pageSize)
        {
            try
            {
                IList<Revenue> revenueList = revRepo.GetRevenues(agencyId, startIndex, pageSize);
                var list = revenueList.Select(Mapper.Map<Revenue, RevenueModel>).ToList();
                return Request.CreateResponse<List<RevenueModel>>(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // GET api/revenue/5
        public HttpResponseMessage Get(string code)
        {
            try
            {
                Revenue revenue = revRepo.GetRevenueByCode(PersonKey.Organization.Id, code);
                var revenueModel = Mapper.Map<Revenue, RevenueModel>(revenue);
                return (revenue == null) ? Request.CreateErrorResponse(HttpStatusCode.NotFound, "No revenue found") :
                 Request.CreateResponse<RevenueModel>(HttpStatusCode.OK, revenueModel);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }

        [HttpGet]
        public HttpResponseMessage RevenuesByAgencyId(int agencyId)
        {

            try
            {
                IList<Revenue> list = revRepo.GetRevenues(agencyId);
                List<RevenueModel> revenues = new List<RevenueModel>();

                foreach (Revenue item in list)
                {
                    RevenueModel model = new RevenueModel();

                    model.Id = item.Id;
                    model.Name = item.Name;
                    model.RevType = item.RevType;
                    model.Code = item.Code;
                    model.AgencyId = item.AgencyId;

                    revenues.Add(model);



                }

                return Request.CreateResponse<List<RevenueModel>>(HttpStatusCode.OK, revenues);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }


        }

        [HttpGet]
        public HttpResponseMessage Like(String name)
        {
            try
            {
                IList<Revenue> revenues = revRepo.GetAgencyRevenueLike(PersonKey.Organization.Id, name);
                if (revenues.ToList().Count == 0)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "No matching revenue found.");
                var revenueModels = revenues.Select(Mapper.Map<Revenue, RevenueModel>).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, revenueModels);
            }
            catch (Exception exception)
            {
                ErrorLogger.Log(exception);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetRevenues()
        {
            try
            {
                List<Revenue> revenueList = revRepo.GetRevenues().ToList();
                List<RevenueModel> revModelList = Mapper.Map<List<Revenue>, List<RevenueModel>>(revenueList);
                return Request.CreateResponse<List<RevenueModel>>(HttpStatusCode.OK, revModelList);
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetRevenueInvoices()
        {
            try
            {
                var data = _invoiceRepository.GetRevenueInvoices().GroupBy(x => x.AgencyId).Select(x => new RevenueInvoiceModel()
                {
                    Count = x.Count(),
                    Agency = x.FirstOrDefault(y => y.AgencyId == x.Key).AgencyCode,
                    Details = new InvoiceDetails()
                    {
                        TotalPaidInvoice = x.Count(y => y.AmountPaid > 0),
                        TotalInvoiceCreated = x.Count()
                    },
                    Payment = new PaymentDetails
                    {
                        TotalDollarPayment = x.Where(y => y.CurrencyId == 840).Sum(y => y.AmountPaid),
                        TotalNairaPayment = x.Where(y => y.CurrencyId == 566).Sum(y => y.AmountPaid),
                        TotalPayment = x.Sum(y => y.AmountPaid),
                        TotalUpaidInvoicePayment = x.Where(y => y.Status == "Unpaid").Sum(y => y.AmountPaid)
                    }
                });
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateBank(List<Bank> model)
        {
            try
            {
                var result = bankRepo.SaveBank(model);
                return Request.CreateResponse(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {                   
               ErrorLogger.Log(ex);
               return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateBank(int id, [FromBody] Bank model)
        {
            try
            {
                var entity = bankRepo.GetBank(id);
                if (entity == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
                entity.Name = model.Name;
                entity.BankCode = model.BankCode;
                var res = bankRepo.UpdateBank(entity);

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage Banks()
        {
            try
            {
                var entity = bankRepo.GetBanks();
                var result = AutoMapperConfig.ToModel(entity);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateSettlementAccount(SettlementAccountModel model)
        {
            try
            {
                model.RetrievalCode = model.BankId + DateTime.UtcNow.Millisecond.ToString();
                var setAcc = Mapper.Map<SettlementAccountModel, SettlementAccount>(model);
                var res = revRepo.CreateSettlementAccount(setAcc, UserCall);
                return Request.CreateResponse(HttpStatusCode.Created, res);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateSettlementAccount(int id,[FromBody] SettlementAccountModel model)
        {
            try
            {
                var entity = revRepo.GetSettlementAccount(id);
                if(entity == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
                entity.Description = model.Description;
                entity.BankId = model.BankId;
                entity.AccountName = model.AccountName;
                entity.AccountNumber = model.AccountNumber;
                entity.Organization = model.Organization;
              
                var res = revRepo.UpdateSettlementAccount(entity, UserCall);
                var data = AutoMapperConfig.ToModel(res);
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateRevenueBeneficiary(RevenueBeneficiaryModel model)
        {
            try
            {
                var enitity = AutoMapperConfig.ToModel(model);
                var res = revRepo.CreateRevenueBeneficiary(enitity, UserCall);
                return Request.CreateResponse(HttpStatusCode.Created, res);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }



        [HttpPost]
        public HttpResponseMessage CreateRemitaServiceType(RemitaServiceTypeModel model)
        {
            try
            {
                var enitity = AutoMapperConfig.ToModel(model);
                var res = revRepo.CreateServiceTypeId(enitity, UserCall);
                return Request.CreateResponse(HttpStatusCode.Created, res);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }



        [HttpPost]
        public HttpResponseMessage UpdateRevenueBeneficiary(int id, RevenueBeneficiaryModel model)
        {
            try
            {
                if (model.SharePercentage >= 100 || model.SharePercentage <= 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                var entity = revRepo.GetRevenueBeneficiary(id);
                if (entity == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
                entity.RevenueId = model.RevenueId;
                entity.SettlementAccountId = model.SettlementAccountId;
                entity.SharePercentage = model.SharePercentage;
                entity.DeductFeeFrom = model.DeductFeeFrom;

                var res = revRepo.UpdateRevenueBeneficiary(entity, UserCall);
                var data = AutoMapperConfig.ToModel(res);
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage SettlementAccounts()
        {
            try
            {
                var entities = revRepo.GetSettlementAccounts();
                var result = AutoMapperConfig.ToModel(entities);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
               return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage SettlementAccount(int id)
        {

            try
            {
                var entity = revRepo.GetSettlementAccount(id);
                var result = AutoMapperConfig.ToModel(entity);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }


        [HttpGet]
        public HttpResponseMessage GetRevenue(int id)
        {
            try
            {
                var entity = revRepo.GetRevenue(id);
                var result = AutoMapperConfig.ToModel(entity);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);               
            }
        }


        [HttpGet]
        public HttpResponseMessage RevenueBeneficiaries(int revId)
        {
            try
            {
                
                var entities = revRepo.RevenueBeneficaries(revId);
                var result = AutoMapperConfig.ToModel(entities);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError); 
            }
        }

        public HttpResponseMessage RemoveRevenueBeneficiary(int id)
        {
            try
            {
                revRepo.RemoveRevenueBeneficiary(id);
                return Request.CreateResponse(HttpStatusCode.OK, id);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError); 
            }
        }

        [HttpGet]
        public HttpResponseMessage GetAccount(string accountNumber)
        {
            try
            {
                var entity = revRepo.GetAccount(accountNumber.Trim());
                var result = AutoMapperConfig.ToModel(entity);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

    }
}
