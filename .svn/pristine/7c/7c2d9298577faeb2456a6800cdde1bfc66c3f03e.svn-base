﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDA.Domain.Models;
using BDA.Domain.Util;
using BDA.Logic.logic;
using BDA.Domain.Entity;

public partial class Administration_SettlementAccount : PageBase
{
    RevenueRepository revRep = new RevenueRepository();

   

    private int agencyId
    {
        get
        {
            try
            {
                int revenueId = int.Parse(_queryString["revenueId"]);
                Revenue revenue = revenueId > 0 ? revRep.GetRevenue(revenueId) : null;
                if (revenue != null)
                {
                    return revenue.AgencyId;
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }
    }


    BDA.Client.logic.RevenueRepository  revenueRepo = new BDA.Client.logic.RevenueRepository();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //lblSuccess.Visible = false;
            PageDefaults();
        }
    }

    private void PageDefaults()
    {
        var serviceTypes = revenueRepo.GetAgencyServiceTypes(agencyId);
        accountsGrid.DataSource = serviceTypes;
        accountsGrid.DataBind();
         
        Session["accounts"] = serviceTypes;
        viewPnl.Visible = true;
        newPnl.Visible = false;
        editPnl.Visible = false;
    }

    protected void newServiceTypeId_OnClick(object sender, EventArgs e)
    {
        viewPnl.Visible = false;
        newPnl.Visible = true;
 
    }

    protected void edit_ServiceTypeId_Click(object sender, EventArgs e)
    {
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        var settId = int.Parse(grdrow.Cells[0].Text);
        Session["SettingsId"] = settId;
        var accounts = Session["accounts"] as IEnumerable<RemitaServiceTypeModel>;
        var account = accounts.FirstOrDefault(x => x.Id == settId);
        txtEditServiceTypeName.Text = account.ServiceTypeName;
        txtEditServiceTypeIdValue.Text = account.ServiceTypeId.ToString(); 

        editPnl.Visible = true;
        viewPnl.Visible = false;
        newPnl.Visible = false;
        lblEditError.Text = string.Empty;

    }

    protected void saveBtn_Click(object sender, EventArgs e)
    {
        try
        {
             
            if (!AppHelper.CheckString(ServiceTypeIdName.Text))
            {
                lblError.Text = @"Service Type Name field can only accept alphanumeric characters";
                lblError.Visible = true;
                return;
            }

            if (!AppHelper.CheckString(txtServiceTypeIdValue.Text))
            {
                lblError.Text = @"Service Type Value field can only accept alphanumeric characters";
                lblError.Visible = true;
                return;
            }

          

            var model = new RemitaServiceTypeModel
            {
                ServiceTypeId = int.Parse(txtServiceTypeIdValue.Text.Trim()),
                ServiceTypeName = ServiceTypeIdName.Text.Trim(),
                AgencyId = agencyId,
                 
            };

            var exists = revenueRepo.GetServiceType(model.ServiceTypeId, agencyId);

            if (exists != null)
            {
                lblError.Text = string.Format("Error: Service Type Number {0} allready exist for the agency", model.ServiceTypeId.ToString());
                lblError.Visible = true;
                return;
            }

            var res = revenueRepo.CreateServiceType(model, CurrentUser.Id);
            if (res)
            {
                lblSuccess.Text = "Service type successfully created";
                lblSuccess.Visible = true;
                this.PageDefaults();
            }
            else
            {
                lblError.Text = "Error, failed to create service type.";
                lblError.Visible = true;
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
            lblError.Text = "Failed to create account due to unexpected error.";
            return;
        }
    

    }



    protected void updateBtn_Click(object sender, EventArgs e)
    {
        try
        {
            var typeId = (int)Session["SettingsId"];
            if (!AppHelper.CheckString(txtEditServiceTypeName.Text))
            {
                lblError.Text = @"Service Type Name field can only accept alphanumeric characters";
                lblError.Visible = true;
                return;
            }

            if (!AppHelper.CheckString(txtEditServiceTypeIdValue.Text))
            {
                lblError.Text = @"Service Type Value field can only accept alphanumeric characters";
                lblError.Visible = true;
                return;
            }



            var model = new RemitaServiceTypeModel
            {
                ServiceTypeId = int.Parse(txtEditServiceTypeIdValue.Text.Trim()),
                ServiceTypeName = txtEditServiceTypeName.Text.Trim(),
                AgencyId = agencyId

            };

            

            var res = revenueRepo.UpdateServiceType(typeId, model, CurrentUser.Id);
            if (res)
            {
                lblSuccess.Text = "Service type successfully updated";
                lblSuccess.Visible = true;
                this.PageDefaults();
            }
            else
            {
                lblEditError.Text = "Error, failed to update service type.";
                lblEditError.Visible = true;
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
            lblEditError.Text = @"Update failed due to unexpected error";
            return;
        }
        
    }

    protected void cancelEditBtn_Click(object sender, EventArgs e)
    {
        PageDefaults();
    }

    protected void btnCancelNew_Click(object sender, EventArgs e)
    {
        PageDefaults();
    }

    
}