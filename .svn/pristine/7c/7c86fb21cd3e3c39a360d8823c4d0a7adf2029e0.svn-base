﻿using BDA.Client.util;
using BDA.Domain;
using BDA.Domain.Dtos;
using BDA.Domain.Entity;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using StackifyLib;
using CommonLogger;

namespace BDA.Client.logic
{
    public class BaseRepository
    {
        private HttpClient client { get; set; }
        private Guid userId { get; set; }

        public BaseRepository()
        {
            string url = AppSettings.BdaServiceUrl;
            client = new HttpClient { BaseAddress = new Uri(url) };
            client.Timeout = new TimeSpan(0, 15, 0);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            AddHeaders();
        }

        public BaseRepository(string baseUrl)
        {
            string url = baseUrl;
            client = new HttpClient { BaseAddress = new Uri(url) };
            client.Timeout = new TimeSpan(0, 15, 0);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
             
        }

        public void AddHeaders()
        {
            client.DefaultRequestHeaders.Remove("ApiKey");
            client.DefaultRequestHeaders.Remove("ApiCode");
            client.DefaultRequestHeaders.Remove("UserCode");

            client.DefaultRequestHeaders.Add("ApiKey", AppSettings.BdaKey);
            client.DefaultRequestHeaders.Add("ApiCode", AppSettings.BdaCode);
            client.DefaultRequestHeaders.Add("UserCode", AppSettings.BdaUserCode);         
        }

        public void AddUserIdHeader(Guid userId)
        {
            client.DefaultRequestHeaders.Remove("userId");
            client.DefaultRequestHeaders.Add("userId", userId.ToString());
        }


        public void AddSpryteHeader()
        {
            client.DefaultRequestHeaders.Remove("ApiKey");
            client.DefaultRequestHeaders.Remove("ApiCode");
            client.DefaultRequestHeaders.Add("ApiKey", AppSettings.SpriteKey);
            client.DefaultRequestHeaders.Add("ApiCode", AppSettings.SpriteCode);
        }

        public HttpResponseMessage Post(object data, string url)
        {
            var param = JsonConvert.SerializeObject(data);
            HttpContent contentPost = new StringContent(param, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = null;
            try
            {
                responseMessage = client.PostAsync(url, contentPost).Result;
            }
            catch (Exception ex)
            {
                responseMessage.StatusCode = HttpStatusCode.InternalServerError;
                responseMessage.ReasonPhrase = string.Format("RestHttpClient.SendRequest failed: {0}", ex);
            }

            return responseMessage;
        }

        public ApiResponse<T> Post<T>(object postdata, string url, string method = "POST")
        {
            
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {             
                if (method == "POST")
                {                 
                    var param = JsonConvert.SerializeObject(postdata);
                    Log.Info(param, "Request Data");
                    HttpContent contentPost = new StringContent(param, Encoding.UTF8, "application/json");                  
                    responseMessage = client.PostAsync(url, contentPost).Result;             
                }
                else
                {
                   
                    responseMessage = client.GetAsync(url).Result;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                if (responseMessage != null)
                {
                    responseMessage.StatusCode = HttpStatusCode.InternalServerError;
                    responseMessage.ReasonPhrase = string.Format("RestHttpClient.SendRequest failed: {0}", ex.Message);
                }              
               
            }

            try
            {
                Log.Info(responseMessage.IsSuccessStatusCode, "Response Code");
                var resp = new ApiResponse<T>();
                if (responseMessage.IsSuccessStatusCode)
                {
                    try
                    {
                        object data = responseMessage.Content.ReadAsAsync<T>().Result;
                        Log.Info(data, "Response Data");
                        resp.Ok(data);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        resp.DataError(ex.Message);                    
                    }
                }
                else
                {
                    ErrorMessage err = responseMessage.Content.ReadAsAsync<ErrorMessage>().Result;
                    resp.NotOk(err.Message);
                }
                Log.Info(resp, "Final Response");
                return resp;
            }
            catch (Exception ex)
            {
               
                ErrorLogger.Log(ex);
                throw;
            }
          
        }

        public ApiResponse<T> Get<T>(string getdata, string url)
        {
            StringBuilder sb = new StringBuilder(url);
            if (!string.IsNullOrEmpty(getdata))
            {
                if (!sb.ToString().EndsWith("/"))
                {
                    sb.Append("/");
                }
                sb.Append(getdata);
            }
            url = sb.ToString();
            return Post<T>(null, url, "GET");
        }
    }
}
