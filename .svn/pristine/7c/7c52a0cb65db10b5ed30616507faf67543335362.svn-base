﻿using System;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;
using System.Xml.Serialization;
namespace BDA.Domain.Entity
{
    [Serializable]
    [XmlInclude(typeof(Revenue))]
    public class Revenue : Entity<int>
    {
        public virtual string Name { get; set; }
        public virtual string NamePro {
            get
            {
                return Name + ": " + Id;
            }
        }
        public virtual string RevType { get; set; }
        public virtual string Code { get; set; }
        public virtual Agency Agency { get; set; }
        public virtual int AgencyId { get; set; }
        public virtual bool EnableInvoicing { get; set; }
        public virtual int DisputeWindow { get; set; }
        public virtual int SettlementId { get; set; }
        public virtual int? ScheduleId { get; set; }
        public virtual int? DisputeWorkflowId { get; set; }
        public virtual int? RevenueGroupId { get; set; }
        public virtual AgencyRevenueGroup AgencyRevenueGroup { get; set; }

        public virtual string Settlement
        {
            get
            {
                if (SettlementId == 1)
                    return "Fidesic";
                else if (SettlementId == 2)
                    return "IATA";
                else
                    return "Not Set";
            }
        }

        public virtual string Revenue_Agency
        {
            get
            {
                return string.Format("{0}({1})", Name, Agency.Code);
            }
        }


        public override string ToString()
        {
            return Name;
        }

        private ISet<Currency> _revenueCurrency;

        public virtual ISet<Currency> RevenueCurrency
        {
            get { return _revenueCurrency; }
            set { _revenueCurrency = value; }
        }

        public virtual void addRevenueCurrency(Currency currency)
        {
            if (_revenueCurrency == null)
                _revenueCurrency = new HashedSet<Currency>();

            if (!_revenueCurrency.Contains(currency))
                _revenueCurrency.Add(currency);
        }
    }
}
