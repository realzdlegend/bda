﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDA.Domain.Entity;
using BDA.Logic.Helpers;
using BDA.Logic.logic;
using System.Diagnostics;
using System.IO;
using BDA.Reports;
using BDA.Domain.Util;
using BDA.Utils;
using BDA.Domain.Search;
using StackifyLib;
using BDA.Domain.Models;
using System.Threading.Tasks;

public partial class Common_InvoiceDetails : PageBase
{
    //InvoiceRepository invRep = new InvoiceRepository();
    BDA.Client.logic.InvoiceRepository invRep = new BDA.Client.logic.InvoiceRepository();
    InvoiceRepository invLogicRep = new  InvoiceRepository();
    //SearchClient client = new SearchClient();
    private readonly OrganizationRepository organizationRepo = new OrganizationRepository();


    public VersionInvoice invoice;

    private long invoiceId
    {
        get
        {

            if (!string.IsNullOrEmpty(_queryString["invoiceId"]))
            {
                return Convert.ToInt64(_queryString["invoiceId"]);
            }
            else
            {
                return 0;
            }
        }
    }

    private string invoiceNumber
    {
        get
        {

            if (!string.IsNullOrEmpty(_queryString["invoiceNumber"]))
            {
                return _queryString["invoiceNumber"].ToString();
            }
            else
            {
                return "0";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PageDefaults();
        }
        PageSetup();       
    }

    private void PageDefaults()
    {
        try
        {
            if (invoiceId != 0)
            {
                invoice = invRep.GetVersionInvoice(invoiceId);
            }
            else
            {
                invoice = invRep.GetVersionInvoice(invoiceNumber);
            }

            gvPayment.Visible = false;
            btnResend.Visible = false;

            if (invoice != null)
            {
                Session["Invoice"] = invoice;
                lbAmount.Text = invoice.AmountString.ToString();
                lbAgency.Text = invoice.Agency;
                lbInvoiceDate.Text = invoice.InvoiceDate.ToLongDateString();
                lbInvoiceNumber.Text = invoice.InvoiceNumber;
                lbAmountPaid.Text = invoice.AmountPaidString;
                lbRevenue.Text = invoice.Revenue;
                lblRevenueGroup.Text = string.IsNullOrWhiteSpace(invoice.RevenueGroup) ? "None" : invoice.RevenueGroup;
                lbServiceProvider.Text = invoice.Customer;
                lbLocation.Text = invoice.Address;
                lbStatus.Text = invoice.PaymentStatus;
                lbStatus.ToolTip = ""; //output == null ? "" : output.PaidScore.ToString();
                Panel4.Visible = (CurrentRole.RoleName == "Agency" && invRep.CanVoid(invoice) && invLogicRep.CanVoid(invoice.InvoiceId) && invoice.CreatedByEmail == CurrentUser.Email);
                if (_queryString["created"] != null)
                {
                    successful.Visible = true;
                    successful.InnerText = "Invoice has been successfully created";
                }
                //customer details

                labelCEmail.Visible = labelCName.Visible = labelCPhone.Visible = true;
                lbCEmail.Visible = lbCName.Visible = lbCPhone.Visible = true;
                lbCPhone.Text = invoice.CustomerPhone;
                lbCName.Text = invoice.CustomerName;
                if (invoice.CustomerEmails != null && invoice.CustomerEmails.Count > 0)
                    lbCEmail.Text = string.Join(", ", invoice.CustomerEmails);

                if (invoice.Deleted)
                {
                    lbVoid.Visible = true;
                    lbVoid.Text = @"This invoice has been voided";

                    Panel4.Visible = false;
                    Panel3.Visible = false;

                    Panel1.Enabled = false;
                    btnPrintInvoice.Visible = false;
                }

                lblRRR.Text = !string.IsNullOrEmpty(invoice.RemitaRetrievalReference) ? invoice.RemitaRetrievalReference: "None";
                if (invoice.PaymentStatus != "Unpaid")
                {
                    voidInvoice.Visible = false;
                }

                if (invoice.PaymentStatus == "Unpaid" && !string.IsNullOrEmpty(invoice.RemitaRetrievalReference))
                {
                    ButtonRemita.Visible = true;
                    //rrr_block.Visible = true;
                }
                else {
                    ButtonRemita.Visible = false;
                    //rrr_block.Visible = false;
                }



            }
            else
            {
                Logger.Queue("Info", "Invoice is null. redirecting", invoiceNumber);
                Response.Redirect("Invoices.aspx");
            }


        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
        }

    }

    private void GetInvoice(long invoiceId)
    {
        invoice = invRep.GetVersionInvoice(invoiceId);
        Session["Invoice"] = invoice;
    }


    private void RemitaRequery(string invoiceId)
    {
        BDA.Client.logic.InvoiceRepository invoiceRepo = new BDA.Client.logic.InvoiceRepository();
        BDA.Client.logic.SpriteAPIRepository spriteRepo = new BDA.Client.logic.SpriteAPIRepository(); 
        try
        {
            var invoice = invoiceRepo.GetInvoice(invoiceId);
            if (invoice == null)
                throw new Exception(string.Format("No Invoice is with invoice number {0}", invoiceId));

            if (!string.IsNullOrEmpty(invoice.RRR) && invoiceRepo.GetRemitaStatus(invoice.RRR))
            {

                var pay = new OfflinePaymentRequestModel()
                {
                    AgencyGatewayId = invoice.AgencyId,
                    AmountPaid = invoice.Amount,
                    Description = "Remita requery marked it as paid",
                    EntryDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                    InvoiceAmount = invoice.Amount,
                    InvoiceNumber = invoice.InvoiceNumber,
                    PaymentDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                    PaymentReference = "RemitaRequery",
                    SpriteReference = ""
                };
                 
                var resp = spriteRepo.OfflinePayment(pay);

            } 
        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
        }
        
    }


    protected void ObjDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        var inv = Session["invoice"] as VersionInvoice;
        if (e.ReturnValue != null && e.ReturnValue.GetType() == Type.GetType("System.Int32"))
        {
            int recordCount = (int)e.ReturnValue;
            lblPrompt.Visible = true;
            if (recordCount > 0)
            {
                lblPrompt.ForeColor = System.Drawing.Color.Black;
                //lblPrompt.Text = string.Format("{0} records found", recordCount);
                lkItem.Text = string.Format("Items ({0})", recordCount);

                int paymentCount = invRep.GetPaymentCount(inv.InvoiceId);
                lkPayment.Text = string.Format("Payments ({0})", paymentCount);
            }
            else
            {
                lblPrompt.ForeColor = System.Drawing.Color.Red;
                //lblPrompt.Text = "No record found";
            }
        }
    }  

    protected void dsPayment_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.ReturnValue != null && e.ReturnValue.GetType() == Type.GetType("System.Int32"))
        {
            int recordCount = (int)e.ReturnValue;
            lkPayment.Text = string.Format("Payments ({0})", recordCount);
        }
    }

    protected void ObjDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        var inv = Session["invoice"] as VersionInvoice;
        e.InputParameters["invoiceId"] = inv.InvoiceId;
    }

    protected void btnPrintInvoice_Click(object sender, EventArgs e)
    {
        try
        {
            var inv = Session["Invoice"] as VersionInvoice;

            var user = Membership.GetUser();
            if (user == null) return;
            Guid userId = (Guid)user.ProviderUserKey;
            //VersionInvoice invoice = invRep.GetVersionInvoice(invoiceId);
            string invoiceNumber = inv.InvoiceNumber;
            int agencyId = inv.AgencyId;
            bool hasPaid = inv.AmountPaid >= inv.Amount; //true;   //false;
            hasPaid = inv.AmountPaid >= inv.Amount;
            switch (agencyId)
            {
                case 20:
                    string url = string.Format("../LocalReports/Reports.aspx?Id={0}&agencyId={1}&ReportName=FAANInvoice&ReportTitle=Invoice{2}&paid={3}",
                        inv.InvoiceId, agencyId, inv.InvoiceNumber, hasPaid);
                    Response.Redirect(url, false);
                    break;
                case 30:
                    Response.Redirect(string.Format("../LocalReports/Reports.aspx?Id={0}&agencyId={1}&userId={3}&ReportName=NAMAInvoice&ReportTitle=Invoice{2}&paid={4}",
                        inv.InvoiceId, agencyId, inv.InvoiceNumber, userId, hasPaid), false);
                    break;
                default:
                    Response.Redirect(string.Format("../LocalReports/Reports.aspx?Id={0}&agencyId={1}&ReportName=Invoice&ReportTitle=Invoice{2}&paid={3}",
                        inv.InvoiceId, agencyId, inv.InvoiceNumber, hasPaid), false);
                    break;
            }

        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
        }
    }

    protected void btnResend_Click(object sender, EventArgs e)
    {
        new PaymentRepository().RefreshPayment(invoice.InvoiceId);
        Response.Redirect(URLEncryption.EncryptURL("InvoiceDetails.aspx", "invoiceId", invoice.InvoiceId.ToString()));
    }


    protected void btnRequeryRemitaInvoice_Click(object sender, EventArgs e)
    {
        //var irep = new BDA.Client.logic.InvoiceRepository();
        //RemitaRequery(lbInvoiceNumber.Text);
        //var res  = irep.RequeryRemita(lbInvoiceNumber.Text);
        BDA.Client.logic.InvoiceRepository invoiceRepo = new BDA.Client.logic.InvoiceRepository();
        BDA.Client.logic.SpriteAPIRepository spriteRepo = new BDA.Client.logic.SpriteAPIRepository();
        try
        { 
            invoice = invRep.GetVersionInvoice(lbInvoiceNumber.Text);
            if (invoice == null)
                throw new Exception(string.Format("No Invoice is with invoice number {0}", invoiceId));
           // "rrr": "340189211183","channnel": "BRANCH","channel": "BRANCH","amount": "245000.0","transactiondate": "2017-11-03 00:00:00","debitdate": "null","bank": " ","branch": " ","serviceTypeId": "517406802","dateRequested": "06/11/2017","orderRef": "null","payerName": "accounts@izyair.com",	"payerPhoneNumber": "+2348090206015",	"payerEmail": "",	"uniqueIdentifier": " ","transType": " ","uniqueReference": ""
            if (!string.IsNullOrEmpty(invoice.RemitaRetrievalReference))
            {
                var status = invoiceRepo.GetRemitaStatus(invoice.RemitaRetrievalReference);
                if (status)
                {
                    var pay = new OfflinePaymentRequestModel()
                    {
                        AgencyGatewayId = invoice.AgencyId,
                        AmountPaid = invoice.Amount,
                        Description = "Remita requery marked it as paid",
                        EntryDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                        InvoiceAmount = invoice.Amount,
                        InvoiceNumber = invoice.InvoiceNumber,
                        PaymentDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                        PaymentReference = invoice.InvoiceNumber,
                        SpriteReference = ""
                    };
                    var resp = spriteRepo.OfflinePayment(pay);
                }

            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
        }
        Response.Redirect(Request.RawUrl);
    }
    


    protected void btnVoid_Click(object sender, EventArgs e)
    {
        try
        {
            var inv = Session["invoice"] as VersionInvoice;
            if (string.IsNullOrEmpty(txtReason.Text))
            {
                lblPrompt.Visible = true;
                lblPrompt.ForeColor = System.Drawing.Color.Red;
                lblPrompt.Text = @"Please enter a reason";
                return;
            }

            invRep.AddUserIdHeader(CurrentUser.Id);
          var res =  invRep.VoidInvoice(inv.InvoiceNumber, txtReason.Text);
            if (!res)
            {
                lbVoid.Visible = true;
                lbVoid.Text = @"Failed to void invoice. an error occured ";
                return;
            }
            GetInvoice(inv.InvoiceId);
            Response.Redirect(URLEncryption.EncryptURL("InvoiceDetails.aspx", "invoiceId", inv.InvoiceId.ToString()));
        }
        catch (Exception ex)
        {
            lblPrompt.Visible = true;
            lblPrompt.ForeColor = System.Drawing.Color.Red;
            lblPrompt.Text = ex.Message;
        }
    }

    protected void lkStatus_Click(object sender, EventArgs e)
    {
        lkItem.CssClass = "";
        lkPayment.CssClass = "";

        LinkButton current = (LinkButton)sender;
        if (current.CommandArgument == "1")
        {
            grdView.Visible = true;
            gvPayment.Visible = false;
        }
        else
        {
            grdView.Visible = false;
            gvPayment.Visible = true;
        }

        hdStatus.Value = (current).CommandArgument;
        current.CssClass = "active";
        //grdView.DataBind();
        //gvsummary.DataBind();

    }
    //protected void btnPrintExcel_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        Invoice invoice = invRep.GetById(invoiceId);
    //        int agencyId = invoice.AgencyId;

    //        Response.Redirect("../LocalReports/Reports.aspx?Id=" + invoiceId + "&agencyId=" + agencyId + "&format=excel" + "&ReportName=Invoice", false);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //        //ErrorLogger.Log(ex);
    //    }
    //}
    protected void voidInvoice_Click(object sender, EventArgs e)
    {
        if (Panel3.Visible)
        {
            Panel3.Visible = false;
            voidInvoice.Text = "Void this Invoice";
        }
        else
        {
            Panel3.Visible = true;
            voidInvoice.Text = "Cancel Void";
        }

    }

    private void PageSetup()
    {
        //set Agency revenue group label

        var userAgency = organizationRepo.GetAgency(CurrentUser.Organization.Id);
        if (AppConstants.AgenciesWithRevGroups.Contains(CurrentUser.Organization.Code.ToLower()))
        {
            lblRevGroupTitle.Text = userAgency.RevenueGroupTag ?? "Revenue Group";
        }

    }
}