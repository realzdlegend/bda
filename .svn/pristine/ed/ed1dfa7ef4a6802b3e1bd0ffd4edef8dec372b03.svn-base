﻿using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using BDA.Domain.Entity;
using BDA.Logic.Helpers;
using BDA.Logic.logic;
using BDA.Logic.Mailer;
using BDA.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using BDA.Search;
using StackifyLib;
using BDA.Domain.Models;

namespace BDA.Services.Controllers
{
    public class InvoiceController : BaseController
    {
        private readonly InvoiceRepository invoiceRepo = new InvoiceRepository();
        private readonly OrganizationRepository orgRepo = new OrganizationRepository();
        private readonly BillsRepository billRepo = new BillsRepository();
        private readonly DataSync dataSync = new DataSync();
        private static string REPORT_INVOICE = "http://bda.avitechng.com/LocalReports/Reports.aspx?";



        public HttpResponseMessage Get()
        {
            try
            {
                DateTime sd = DateTime.Now; // DateTime.ParseExact(startDate, "", null);
                DateTime ed = DateTime.Now.AddYears(-1); // DateTime.ParseExact(endDate, "", null);
                var invoices = invoiceRepo.SearchInvoices(UserKey, sd, 0, 200);
                if (invoices == null || invoices.Count == 0)
                    throw new Exception(string.Format("No Invoices found"));

                var response = from a in invoices.AsEnumerable() select a.ToModel();

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        /**
         * Get Invoice doc (by apdocjs.com)
         *@api {get} /api/invoice/Get Request Invoice
         * 
         *@apiName  Get Invoice by invoice number
         *@apiGroup Invoice
         *@apiVersion 0.1.0
         *
         * @apiHeader {string} apicode   api access code
         * @apiHeader {string} apikey    api access key
         * @apiHeader {string} usercode  a unique guid generated for client
         *
         * @apiHeaderExample {json} Sample Header:
         *     {
         *       "apikey": "test"
         *       "apicode": "test"
         *       "usercode":"0a56201b-416b-4a24-aedf-a5f30107a8e6"
         *     }
         *
         * 
         * @apiParam  {string} invoiceNumber Invoice Number
         * 
         *
         *@apiSuccessExample Success-Response
         *
         * HTTP/1.1 200 OK
         *{
         *  "InvoiceNumber": "5100920016765",
         *  "InvoiceDate": "2016-05-23T16:22:21",
         *  "CreatedDate": "2016-05-23T16:22:21",
         *  "Amount":5500000,
         *  "CustomerName":"Example plc limited",
         *  "CustomerAIN":"1710003589",
         *  "AgencyCode": "CGX",
         *  "Revenue": "Tution",
         *  ................................
         *  ................................
         *  ................................
         *}
         * @apiError error messsage
         *
         * @apiErrorExample Error-Response:
         * HTTP/1.1 400 Bad Request
         * {
         * "error":"error message"
         * }
         */
        public HttpResponseMessage Get(string invoiceNumber)
        {
            try
            {
                var invoice = invoiceRepo.GetInvoiceView(UserKey, invoiceNumber);
                if (invoice == null)
                    throw new Exception(string.Format("Invoice with invoice number {0} does not exist", invoiceNumber));

                var invoiceModel = Mapper.Map<InvoiceView, InvoiceDetailModel>(invoice);
                return Request.CreateResponse(HttpStatusCode.OK, invoiceModel);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        [HttpGet]
        public HttpResponseMessage GetByteArrayPdf(string invoiceNumber)
        {
            var invoice = invoiceRepo.GetInvoice(invoiceNumber);
            if (invoice == null)
                throw new Exception(string.Format("Invoice with invoice number {0} does not exist", invoiceNumber));
            int agencyId = invoice.AgencyId;
            var response = Request.CreateResponse(HttpStatusCode.Moved);

            switch (agencyId)
            {
                case 20:
                    try
                    {
                        response.Headers.Location =
                            new Uri(
                                string.Format(
                                    REPORT_INVOICE + "Id={0}&agencyId={1}&ReportName=FAANInvoice&ReportTitle=Invoice{2}",
                                    invoice.Id, agencyId, invoiceNumber));
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(response.Headers.Location);
                        req.PreAuthenticate = true;
                        HttpWebResponse HttpWResp = (HttpWebResponse)req.GetResponse();
                        Stream fileStream = HttpWResp.GetResponseStream();
                        byte[] fileBytes = ReadFully(fileStream);
                        HttpWResp.Close();
                        HttpResponseMessage SuccessResponse = new HttpResponseMessage(HttpStatusCode.OK);
                        SuccessResponse.Content = new ByteArrayContent(fileBytes);
                        return SuccessResponse;

                    }
                    catch (Exception ex)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                    }

                case 30:
                    try
                    {
                        response.Headers.Location =
                            new Uri(
                                string.Format(
                                    REPORT_INVOICE + "Id={0}&agencyId={1}&ReportName=NAMAInvoice&ReportTitle=Invoice{2}",
                                    invoice.Id, agencyId, invoiceNumber));
                        HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create(response.Headers.Location);
                        req2.PreAuthenticate = true;
                        HttpWebResponse HttpWResp2 = (HttpWebResponse)req2.GetResponse();
                        Stream fStream2 = HttpWResp2.GetResponseStream();

                        byte[] fileBytes2 = ReadFully(fStream2);
                        HttpWResp2.Close();
                        HttpResponseMessage SuccessResponse = new HttpResponseMessage(HttpStatusCode.OK);
                        SuccessResponse.Content = new ByteArrayContent(fileBytes2);
                        return SuccessResponse;
                    }
                    catch (Exception ex)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                    }

                default:
                    try
                    {
                        response.Headers.Location =
                            new Uri(
                                string.Format(
                                    REPORT_INVOICE + "Id={0}&agencyId={1}&ReportName=Invoice&ReportTitle=Invoice{2}",
                                    invoice.Id, agencyId, invoiceNumber));
                        HttpWebRequest req3 = (HttpWebRequest)WebRequest.Create(response.Headers.Location);
                        req3.PreAuthenticate = true;
                        HttpWebResponse HttpWResp3 = (HttpWebResponse)req3.GetResponse();
                        Stream fStream3 = HttpWResp3.GetResponseStream();

                        byte[] fileBytes3 = ReadFully(fStream3);
                        HttpWResp3.Close();
                        HttpResponseMessage SuccessResponse = new HttpResponseMessage(HttpStatusCode.OK);
                        SuccessResponse.Content = new ByteArrayContent(fileBytes3);
                        return SuccessResponse;
                    }
                    catch (Exception ex)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                    }

            }


        }

        [HttpGet]
        public HttpResponseMessage Sync(string date, bool elasticSearch = false)
        {
            try
            {
                IEnumerable<InvoiceDetailModel> bb = null;
                DateTime sd = DateTime.ParseExact(date, "yyyy-MM-ddTHH:mm:ss", null);
                var invoices = invoiceRepo.SearchInvoices_ES(UserKey, sd, 0, 200);
                if (invoices == null || invoices.Count == 0)
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No invoices found");

                bb = from a in invoices.AsEnumerable() select a.ToModel();
                //if (elasticSearch == false)
                //{
                    


                //}
                //else
                //{
                //    var invoices = invoiceRepo.SearchInvoices(UserKey, sd, 0, 200);
                //    if (invoices == null || invoices.Count == 0)
                //        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No invoices found");

                //    bb = from a in invoices.AsEnumerable() select a.ToModel();
                //}

                return Request.CreateResponse(HttpStatusCode.OK, bb);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        //POST api/invoice
        [HttpPost]
        public HttpResponseMessage Create(InvoiceModel invoice)
        {
            try
            {
                if (invoice == null) throw new Exception("Invoice data null or not in right format");
                var items = invoice.Bills.Select(bill =>
                    new BillItem { Amount = bill.Amount, Description = bill.Description, Quantity = 1 }).ToList();
                Guid apiGuid = Guid.Empty;
                string invoiceNumber = invoiceRepo.CreateInvoice(1, invoice.RevenueId, invoice.CustomerId,
                    invoice.CurrencyId, invoice.LocationId, invoice.Name, invoice.Emails, invoice.Phone,
                    invoice.Address, UserKey, items, invoice.InvoiceDate, UserCall);

                return Request.CreateResponse<string>(HttpStatusCode.Created, invoiceNumber);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        /**
       * Create Invoice api doc (by apidocsjs.com)
      *
      *@api {post} /api/invoice/new  Create Invoice
      *@apiName  CreateInvoice
      *@apiGroup Invoice
      *@apiVersion 0.1.0
      *
      * @apiHeader {string} apicode   api access code
      * @apiHeader {string} apikey    api access key
      * @apiHeader {string} usercode  a unique guid generated for client
      *
         * @apiHeaderExample {json} Sample Header:
         *     {
         *       "apikey": "test"
         *       "apicode": "test"
         *       "usercode":"0a56201b-416b-4a24-aedf-a5f30107a8e6"
         *     }
         *     
         * 
         * @apiParam {String} RevenueCode  Mandatory code for invoice revenue line .
         * @apiParam {String} CustomerAin  Mandatory unique customer identification number.
         * @apiParam {String} CurrencyId   Mandatory unique number for currency type (naira or dollar).
         * @apiParam {String} LocationCode Mandatory code for customer location.
         * @apiParam {String} Name         Mandatory customer name
         * @apiParam {String} Emails       Mandatory customer email
         * @apiParam {String} Phone        Mandatory customer Phone
         * @apiParam {String} Address      Mandatory customer address
         * @apiParam {Object} Bills        Mandatory array of bill items
         * 
      * @apiParamExample {json} Sample request body:
        * 
        * {
        *  "RevenueCode":"VCA",
        *  "CustomerAin":"0810005545",
        *  "CurrencyId":566,
        *  "LocationCode":"LOS",
        *  "Name":"Example Customer",
        *  "Emails":"customer@example.com",
        *  "Phone":"09011111111",
        *  "Address":"20, example address, Lagos",
        *  "Bills":[
        *       {"Amount":250.00,"Description":"bill for item 1","Quantity" : 1},
        *       {"Amount":300.00,"Description":"bill for item 2", "Quantity" : 2}
        *   ]
        *  }
        * 
      *@apiSuccessExample Success-Response
      *
      * HTTP/1.1 201 Created
      * 9120003002045
      *  
      * 
      * @apiError error server error
      *
      * @apiErrorExample Error-Response
      * HTTP/1.1 500 Internal Server Error
      * {
      * "error":"server error"
      * }
      
      * @apiErrorExample Error-Response
      * HTTP/1.1 400 Bad Request
      * {
      * "error":"error message"
      * }
      */
        [HttpPost]
        public HttpResponseMessage New(NewInvoiceModel invoice)
        {
            try
            {
                if (invoice == null) throw new Exception("Invoice data null or not in right format");
                var context = AppContext;
                //var items = invoice.Bills.Select(bill =>
                //    new BillItem { Amount = bill.Amount, Description = bill.Description, Quantity = 1 }).ToList();
                var items = invoice.Bills.Select(bill =>
                    new BillItem
                    { Amount = bill.Amount,
                        Description = bill.Description, 
                        Quantity = bill.Quantity > 0 ? bill.Quantity: 1
                    }).ToList();
                Guid apiGuid = Guid.Empty;

                //switch to userID
                string invoiceNumber = invoiceRepo.CreateInvoice(1, invoice.RevenueCode, invoice.CustomerAIN,
                    invoice.CurrencyId, invoice.LocationCode, invoice.Name, invoice.Emails, invoice.Phone,
                    invoice.Address, UserId, items, invoice.InvoiceDate, UserCall, invoice.RevenueGroupId);
                return Request.CreateResponse<string>(HttpStatusCode.Created, invoiceNumber);
            }
            catch (ArgumentException ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                if (ex.Message.Contains(AppConstants.DuplicateInvoice))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
                else if (ex.Message.Contains(AppConstants.InvalidFidesicUser))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage Void(VoidInvoice vInvoice)
        { 


            try
            {
                var context = AppContext;
                if (vInvoice == null) throw new Exception("Void Invoice data null or not in right format");

                bool voided = invoiceRepo.VoidInvoice(vInvoice.InvoiceNumber, PersonKey.Id, vInvoice.Reason, UserCall);
                if(voided)
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "true");
                else
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "false");
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateBill(CustomerBillModel customerBill)
        {
            try
            {
                var items = customerBill.Bills.Select(bill =>
                    new BillItem { Amount = bill.Amount, Description = bill.Description, Quantity = 1 }).ToList();

                int count = billRepo.CreateBills(1, UserKey, customerBill.CurrencyId, customerBill.RevenueCode,
                    customerBill.CustomerAIN, customerBill.LocationCode, items);
                return Request.CreateResponse<string>(HttpStatusCode.Created, count.ToString());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        public HttpResponseMessage GetInvoicesByRevenueId(int revenueId)
        {
            try
            {
                List<Invoice> invoices = invoiceRepo.GetInvoicesByRevenueId(revenueId);
                List<InvoiceMDBModel> invoiceModel = new List<InvoiceMDBModel>();

                foreach (Invoice item in invoices)
                {
                    InvoiceMDBModel model = new InvoiceMDBModel();
                    model.InvoiceNumber = item.InvoiceNumber;
                    model.InvoiceDate = item.InvoiceDate;
                    model.CreatedDate = item.CreatedDate;
                    model.Amount = item.Amount;
                    model.AmountPaid = item.AmountPaid;
                    model.CurrencyId = item.CurrencyId;
                    model.DueDate = item.DueDate;
                    model.Sent = item.Sent;
                    model.Status = item.Status;
                    model.RevenueName = item.RevenueName;
                    model.RevenueAgency = item.Revenue_Agency;
                    model.RevenueId = item.RevenueId;

                    invoiceModel.Add(model);

                }

                return Request.CreateResponse<List<InvoiceMDBModel>>(HttpStatusCode.OK, invoiceModel);

            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage DailyReport(int currencyId, int yearId, int monthId = -1)
        {
            try
            {
                ReportClient client = new ReportClient();

                int agencyId = PersonKey.Organization.Id < 10 ? -1 : PersonKey.Organization.Id;

                var results = client.InvoicesDaily(agencyId, currencyId, yearId, monthId);
                //IEnumerable<DateReportModel> bb = from a in results.AsEnumerable() select a.ToModel();

                return Request.CreateResponse(HttpStatusCode.OK, results);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage MonthlyReport(int currencyId, int yearId = -1)
        {
            try
            {
                ReportClient client = new ReportClient();

                int agencyId = PersonKey.Organization.Id < 10 ? -1 : PersonKey.Organization.Id;

                var results = client.InvoicesMonthly(agencyId, currencyId, yearId);
                IEnumerable<DateReportModel> bb = from a in results.AsEnumerable() select a.ToModel();

                return Request.CreateResponse(HttpStatusCode.OK, bb);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage YearlyReport(int currencyId)
        {
            try
            {
                ReportClient client = new ReportClient();

                int agencyId = PersonKey.Organization.Id < 10 ? -1 : PersonKey.Organization.Id;

                var results = client.InvoicesYearly(agencyId, currencyId);
                IEnumerable<DateReportModel> bb = from a in results.AsEnumerable() select a.ToModel();

                return Request.CreateResponse(HttpStatusCode.OK, bb);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        //Returns settlement invoice to ebips eBills relayer
        [HttpGet]
        public HttpResponseMessage SettlementInvoice(string invoiceNumber)
        {
            if (string.IsNullOrEmpty(invoiceNumber))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Please supply an Invoice Number");
            }

            try
            {
                var invoiceDetails = new SettlementInvoice();
                Logger.Queue(AppConstants.Info, "Attempt to retrieve settlement invoice");

                var invoice = invoiceRepo.GetUnpaidInvoice(invoiceNumber.Trim()); //invoiceRepo.GetInvoice(invoiceNumber);
                if (invoice == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such Invoice exist");
                }

                invoiceDetails = AutoMapperConfig.ToModel(invoice);
                var settlement = orgRepo.GetAgencySettlement(invoice.AgencyId);

                if (settlement == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Settlement information has not been configured for the agency");
                }

                var settlementInfo = invoiceDetails.SettlementInformation;

                //add agency as as settlement beneficiary first
                settlementInfo.Add(new SettlementInformation()
                {
                    Share = settlement.AgencyShare,
                    BeneficiaryCode = settlement.BeneficiaryCode,
                    Name = invoiceDetails.Agency
                });

                foreach (var item in settlement.SettlementBeneficiary)
                {
                    var si = new SettlementInformation();
                    si.Name = item.EbillsBeneficiary.Name;
                    si.Share = item.Share; 
                    si.BeneficiaryCode = item.EbillsBeneficiary.BeneficiaryCode;
                    settlementInfo.Add(si);

                }

                invoiceDetails.SettlementInformation = settlementInfo;
                Logger.Queue(AppConstants.Info, "Retrieved settlement invoice", invoiceDetails);

                return Request.CreateResponse<SettlementInvoice>(HttpStatusCode.OK, invoiceDetails);
            }
            catch (InvalidOperationException ex)
            {
                ErrorLogger.Log(ex);
                if (ex.Message.Equals(AppConstants.UnsentInvoiceError))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, AppConstants.UnsentInvoiceError);
                }
                else if (ex.Message.Equals(AppConstants.VoidedInvoiceError))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, AppConstants.VoidedInvoiceError);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, AppConstants.PaidInvoiceError);
                }
            }
            catch (Exception ex)
            {

                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }


        }

       

        // Get EBIPS Payments by invoice number
        

        [HttpGet]
        public async Task<HttpResponseMessage> RemoveInvoice(long invoiceId)
        {
            try
            {
                var result = await Task.Run(() => invoiceRepo.RemoveInvoice(invoiceId));
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> TopTen()
        {
            try
            {

                var invoices = await Task.Run(() => invoiceRepo.Top10Invoices());
                var result = Mapper.Map<IEnumerable<Invoice>, IEnumerable<NIBSSInvoice>>(invoices);
                return Request.CreateResponse(HttpStatusCode.OK, result.OrderByDescending(x => x.InvoiceDate));
            }
            catch (Exception ex)
            {

                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> TriggerSync(int? type = null, long invoiceId = 0, int remId = 0)
        {
            try
            {
                int result = 0, invRes = 0, remRes = 0;
                string response = string.Empty;

                if (invoiceId > 0)
                {
                    invRes = await Task.Run(() => dataSync.UpdateInvoice(invoiceId));
                    return Request.CreateResponse(HttpStatusCode.OK, string.Format("{0} row on invoice table updated", invRes));
                }

                if (remId > 0)
                {
                    remRes = await Task.Run(() => dataSync.UpdateRemittance(remId));
                    return Request.CreateResponse(HttpStatusCode.OK,
                        string.Format("{0} row on remittance table updated", remRes));
                }

                if (type == 0)
                {
                    result = await Task.Run(() => dataSync.UpdateInvoices());
                    response = string.Format("{0} number of rows updated on invoices table", result);
                }
                else if (type == 1)
                {
                    result = await Task.Run(() => dataSync.UpdateRemmittances());
                    response = string.Format("{0} number of rows updated on remmittances table", result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No such type defined");
                }

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage CustomerInvoice(CustomerInvoiceModel cInvoice)
        {
            try
            {
                var cInvoiceEntity = cInvoice.ToModel();
                var response = invoiceRepo.CreateCustomerInvoice(cInvoiceEntity);
                return Request.CreateResponse(HttpStatusCode.Created, response);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage InvoiceItem(InvoiceItemModel invoiceItem)
        {
            try
            {
                var entity = invoiceItem.ToModel();
                var response = invoiceRepo.CreateInvoiceItem(entity);
                return Request.CreateResponse(HttpStatusCode.Created, response);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage SyncVoidedInvoices()
        {
            try
            {
                var invRes = dataSync.SyncVoidedInvoices();
                return Request.CreateResponse(HttpStatusCode.OK, string.Format("{0} row on invoice table updated", invRes));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                throw;
            }
        }

        [HttpPost]
        public HttpResponseMessage Search(InvoiceDataModel ism)
        {
            try
            {
                var sc = new SearchClient();
                var data = sc.SearchInvoices(false, ism.agencyId, ism.providerId, ism.locationIds,
                    ism.revenueIds, ism.status, ism.currencyId, ism.startDate, ism.endDate,
                    ism.dateType, ism.invoiceNumber, ism.name, ism.startIndex, ism.pageSize, ism.state);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage SearchCount(InvoiceDataModel ism)
        {
            try
            {
                var sc = new SearchClient();
                var data = sc.SearchInvoicesCount(false, ism.agencyId, ism.providerId, ism.locationIds,
                    ism.revenueIds, ism.status, ism.currencyId, ism.startDate, ism.endDate,
                    ism.dateType, ism.invoiceNumber, ism.name, ism.startIndex, ism.pageSize, ism.state);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage SearchSummary(InvoiceDataModel ism)
        {
            try
            {
                var sc = new SearchClient();
                var data = sc.getSearchSummary(false, ism.agencyId, ism.providerId, ism.locationIds,
                    ism.revenueIds, ism.status, ism.currencyId, ism.startDate, ism.endDate,
                    ism.dateType, ism.invoiceNumber, ism.name, ism.state);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage Similar(InvoiceDataModel ism)
        {
            try
            {
                var sc = invoiceRepo.SimilarInvoice(ism.revenueId, ism.customerId, ism.amount);
                var sInvoice = Mapper.Map<Invoice, SimilarInvoiceModel>(sc);
                return Request.CreateResponse(HttpStatusCode.OK, sInvoice);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                //throw;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetView(InvoiceDataModel ism)
        {
            try
            {
                InvoiceView sc = new InvoiceView();
                if (ism.invoiceId != 0)
                {
                    sc = invoiceRepo.GetInvoiceView(ism.invoiceId);
                }
                else if (!string.IsNullOrEmpty(ism.invoiceNumber))
                {
                    sc = invoiceRepo.GetInvoiceView(ism.invoiceNumber);
                }
                else
                {
                    sc = null;
                }

                if(sc == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid invoice reference");
                }
                return Request.CreateResponse(HttpStatusCode.OK, sc);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                //throw;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetVersion(InvoiceDataModel ism)
        {
            SearchClient client = new SearchClient();
            try
            {
                BDA.Search.Data.VersionInvoice sc = new BDA.Search.Data.VersionInvoice();
                if (ism.invoiceId != 0)
                {
                    sc = client.GetInvoice(ism.invoiceId);
                }
                else if (!string.IsNullOrEmpty(ism.invoiceNumber))
                {
                    sc = client.GetInvoice(ism.invoiceNumber);
                }
                else
                {
                    sc = null;
                }


                //I didnt find info on ES, so I will look at DB
                if(sc == null)
                {
                    Logger.Queue(AppConstants.Info, "Attempt to get version invoice from DB: ES failed");
                    if (ism.invoiceId != 0)
                    {
                        sc = invoiceRepo.GetVersionInvoice(ism.invoiceId);
                    }
                    else if (!string.IsNullOrEmpty(ism.invoiceNumber))
                    {
                        var inv = invoiceRepo.GetInvoice(ism.invoiceNumber);
                        sc = invoiceRepo.GetVersionInvoice(inv.Id);
                    }
                    else
                    {
                        sc = null;
                    }
                }



                if (sc == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid invoice reference");
                }
                return Request.CreateResponse(HttpStatusCode.OK, sc);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                //throw;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage ReQueryRemita(string inv)
        {
            try
            {
                var invoice = invoiceRepo.GetInvoice(inv);
                if (invoice == null)
                    throw new Exception(string.Format("No Invoice is with invoice number {0}", inv));

                if (!string.IsNullOrEmpty(invoice.RRR) && invoiceRepo.GetRemitaStatus(invoice.RRR))
                {
                    invoice.AmountPaid = invoice.Amount;
                    invoiceRepo.Update(invoice); 
                }
                var message = invoiceRepo.GetRemitaStatusData(invoice.RRR);
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
 



        public HttpResponseMessage DailyRemitaProcessing()
        {
            try
            {
                var invoice = invoiceRepo.GetUnpaidInvoicesSinceYesterday();
                if (invoice == null )
                    throw new Exception(string.Format("No Invoice is unpaid since yesterday"));

                foreach (Invoice inv in invoice ) {
                    if (!string.IsNullOrEmpty(inv.RRR) && invoiceRepo.GetRemitaStatus(inv.RRR) ) {
                        inv.AmountPaid = inv.Amount;
                        invoiceRepo.Update(inv);
                           
                    }
                }
                 
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

           
        public HttpResponseMessage GetInvoice(string invoiceNumber)
        {
            try
            {
                var invoice = invoiceRepo.GetUnpaidInvoice(invoiceNumber);
                if (invoice == null || !invoice.Sent)
                    throw new Exception(string.Format("Invoice with invoice number {0} does not exist", invoiceNumber));
               
                var invoiceModel = Mapper.Map<Invoice, NIBSSInvoice>(invoice);
                return Request.CreateResponse(HttpStatusCode.OK, invoiceModel);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }

}

