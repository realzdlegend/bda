﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDA.Logic.logic;
using BDA.Domain.Entity;
using System.Globalization;
using BDA.Domain.Util;

public partial class ServiceProvider_BillDetails : PageBase
{
    private DisputeRepository disputeRep = new DisputeRepository();
    private BillsRepository billsRep = new BillsRepository();
    private BillDetailsRepository bdRep = new BillDetailsRepository();

    private BillView bill
    {
        get
        {
            if (!string.IsNullOrEmpty(_queryString["billId"]))
            {
                return billsRep.GetBillView(long.Parse((_queryString["billId"])));
            }
            else
            {
                return null;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentUser == null)
        {
            Response.Redirect("~/Login.aspx");
        }

        DisputeManager dm = new DisputeManager();

        lnkDispute.Visible = dm.CanViewDispute(bill.Id, CurrentUser.Id);
        lbMsg.Visible = lnkDispute.Visible;
        lnkEditBill.Visible = !lnkDispute.Visible;
        

        if (!IsPostBack)
        {
            string url = URLEncryption.EncryptURL("Dispute.aspx", "billId", _queryString["billId"]); 
            lnkDispute.NavigateUrl = url;
            lnkEditBill.NavigateUrl = string.Format("{0}&edit=true", url);
            lnkEditBill.Visible = dm.CanEdit(bill.Id, CurrentUser.Id);
            lbSaved.Visible = string.IsNullOrEmpty(_queryString["saved"]) ? false : true;

            txtAmount.Text = bill.AmountString;
            txtBillDate.Text = DateTime.Parse(bill.EntryDate.ToString()).GetDateTimeFormats()[11].ToString();
            txtDescription.Text = Convert.ToString(bill.Description);
            txtTransDate.Text = DateTime.Parse(bill.TransactionDate.ToString()).GetDateTimeFormats()[11].ToString();
            lblLocation.Text = bill.Location;
            lblRevenue.Text = bill.Revenue;
            lblStatus.Text = bill.StatusDescription;
            lblServiceProvider.Text = bill.ServiceProvider;
            lblAgency.Text = bill.Agency;
        }
    }

    protected void btnDispute_Click(object sender, EventArgs e)
    {
        string url = string.Format("Dispute.aspx?BillId={0}", _queryString["billId"]);
        Response.Redirect(url);
    }
    protected void ds_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["BillId"] = bill.Id;
    }
}