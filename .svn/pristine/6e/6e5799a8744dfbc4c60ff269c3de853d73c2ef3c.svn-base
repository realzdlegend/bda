﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace EnterpriseDB.Entities
{
    public class NCharge
    {
        [XmlElement("Id", typeof(string), IsNullable = false)]
        public string Id { get; set; }

        [XmlElement("AINumber", typeof(string), IsNullable = false)]
        public string AINumber { get; set; }

        [XmlElement("ArrivalDate", typeof(string), IsNullable = false)]
        public string ArrivalDate { get; set; }

        [XmlElement("AircraftId", typeof(string), IsNullable = false)]
        public string AircraftId { get; set; }

        [XmlElement("CargoCharge", typeof(decimal), IsNullable = false)]
        public decimal CargoCharge { get; set; }

        [XmlElement("ChargeAmount", typeof(decimal), IsNullable = false)]
        public decimal ChargeAmount { get; set; }

        [XmlElement("CVSAmount", typeof(decimal), IsNullable = false)]
        public decimal CVSAmount { get; set; }

        [XmlElement("TotalWeight", typeof(decimal), IsNullable = false)]
        public decimal TotalWeight { get; set; }

        [XmlElement("Airline", typeof(string), IsNullable = false)]
        public string Airline { get; set; }

        [XmlElement("DepartureAirport", typeof(string), IsNullable = false)]
        public string DepartureAirport { get; set; }

        [XmlElement("ManifestNumber", typeof(string), IsNullable = false)]
        public string ManifestNumber { get; set; }

        [XmlElement("AircraftNumber", typeof(string), IsNullable = false)]
        public string AircraftNumber { get; set; }

        [XmlElement("FreightForwarder", typeof(string), IsNullable = false)]
        public string FreightForwarder { get; set; }

        [XmlElement("GoodsCategory", typeof(string), IsNullable = false)]
        public string GoodsCategory { get; set; }

        [XmlElement("ShipperName", typeof(string), IsNullable = false)]
        public string ShipperName { get; set; }

        [XmlElement("AirwayBillNumber", typeof(string), IsNullable = false)]
        public string AirwayBillNumber { get; set; }
    }

    public class NChargeList
    {
        public NChargeList() { NList = new List<NCharge>(); }

        [XmlElement("NList", typeof(List<NCharge>), IsNullable = false)]
        public List<NCharge> NList { get; set; }
    }
}
