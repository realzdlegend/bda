﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataHelper
/// </summary>
public class DataHelper
{
	public DataHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string connString = ConfigurationManager.ConnectionStrings["bda"].ConnectionString;

    public static DataTable GetUser(Guid userId)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_User", userId).Tables[0];
    }

    public static DataTable GetAgencies()
    {
        return SqlHelper.ExecuteDataset(connString, "charts_Agencies").Tables[0];
    }
    public static DataTable GetReports()
    {
        return SqlHelper.ExecuteDataset(connString, "reports_Agencies").Tables[0];
    }
    public static DataTable GetImages()
    {
        return SqlHelper.ExecuteDataset(connString, "reports_Agencies").Tables[0];
    }
    public static DataTable GetLocations(string agency)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_Locations", agency).Tables[0];
    }

    public static DataTable GetAgencyRevenues(string agency, int currencyId, DateTime start, DateTime end, string location = "")
    {
        return SqlHelper.ExecuteDataset(connString, "charts_AgencyRevenues", agency, location, currencyId, start, end).Tables[0];
    }

    public static DataTable GetAgencyTime(string agency, int currencyId, DateTime start, DateTime end)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_AgencyTime", currencyId, agency, start, end).Tables[0];
    }

    public static DataTable GetMonthly(string agency, int currencyId, DateTime start, DateTime end)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_Monthly", currencyId, agency, start, end).Tables[0];
    }

    public static DataTable GetAgencyLocations(string agency, int currencyId)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_AgencyLocations", agency, currencyId).Tables[0];
    }

    public static List<DailySummary> GetDailyPayment(DateTime date, int currencyId, string agency, string location)
    {
        DataTable table = SqlHelper.ExecuteDataset(connString, "charts_DailyPayment", currencyId, agency, location).Tables[0];

        List<DailyPayment> payments = new List<DailyPayment>();
        foreach (DataRow row in table.Rows)
        {
            DailyPayment daily = new DailyPayment();
            daily.Agency = (string)row["Agency"];
            daily.Amount = (decimal)row["AmountPaid"];
            daily.PDate = (DateTime)row["PDate"];
            daily.Revenue = (string)row["Revenue"];

            payments.Add(daily);
        }

        List<DailySummary> summaryList = new List<DailySummary>();
        List<string> revenueList = payments.Select(d => d.Revenue).Distinct<string>().ToList();

        DateTime today = date.Date;
        DateTime yesterday = today.AddDays(-1);
        DateTime thisMonth = new DateTime(today.Year, today.Month, 1);
        DateTime lastMonth = thisMonth.AddMonths(-1);
        DateTime thisYear = new DateTime(today.Year, 1, 1);
        DateTime lastYear = thisYear.AddYears(-1);

        foreach (string revenue in revenueList)
        {
            IEnumerable<DailyPayment> tp = payments.Where(p => p.Revenue == revenue);
            DailySummary summary = new DailySummary();
            summary.Agency = tp.Where(x => x.Revenue == revenue).First().Agency;
            summary.Revenue = revenue;

            summary.ThisDay = tp.Where(d => d.PDate == today).Sum(d => d.Amount);
            summary.LastDay = tp.Where(d => d.PDate == yesterday).Sum(d => d.Amount);
            summary.ThisMonth = tp.Where(d => d.PDate >= thisMonth && d.PDate <= today).Sum(d => d.Amount);
            summary.LastMonth = tp.Where(d => d.PDate >= lastMonth && d.PDate < thisMonth).Sum(d => d.Amount);
            summary.ThisYear = tp.Where(d => d.PDate >= thisYear && d.PDate <= today).Sum(d => d.Amount);
            summary.LastYear = tp.Where(d => d.PDate >= lastYear && d.PDate < thisYear).Sum(d => d.Amount);

            summaryList.Add(summary);
        }

        List<string> agencyList = payments.Select(d => d.Agency).Distinct<string>().ToList();

        //DateTime today = date.Date;
        //DateTime yesterday = today.AddDays(-1);
        //DateTime thisMonth = new DateTime(today.Year, today.Month, 1);
        //DateTime lastMonth = thisMonth.AddMonths(-1);
        //DateTime thisYear = new DateTime(today.Year, 1, 1);
        //DateTime lastYear = thisYear.AddYears(-1);

        //foreach (string ag in agencyList)
        //{
        //    IEnumerable<DailyPayment> tp = payments.Where(p => p.Agency == ag);
        //    DailySummary summary = new DailySummary();
        //    summary.Agency = ag;
        //    summary.Revenue = "All Revenue";

        //    summary.ThisDay = tp.Where(d => d.PDate == today).Sum(d => d.Amount);
        //    summary.LastDay = tp.Where(d => d.PDate == yesterday).Sum(d => d.Amount);
        //    summary.ThisMonth = tp.Where(d => d.PDate >= thisMonth && d.PDate <= today).Sum(d => d.Amount);
        //    summary.LastMonth = tp.Where(d => d.PDate >= lastMonth && d.PDate < thisMonth).Sum(d => d.Amount);
        //    summary.ThisYear = tp.Where(d => d.PDate >= thisYear && d.PDate <= today).Sum(d => d.Amount);
        //    summary.LastYear = tp.Where(d => d.PDate >= lastYear && d.PDate < thisYear).Sum(d => d.Amount);

        //    summaryList.Add(summary);
        //}

        //summaryList.OrderBy(x => x.Agency);

        return summaryList.OrderBy(x => x.Agency).ToList();
    }
    public static List<DailySummary> GetDailyInvoices(DateTime date, int currencyId, string agency, string location)
    {
        DataTable table = SqlHelper.ExecuteDataset(connString, "charts_DailyInvoices", agency, currencyId).Tables[0];

        List<DailyPayment> payments = new List<DailyPayment>();
        foreach (DataRow row in table.Rows)
        {
            DailyPayment daily = new DailyPayment();
            daily.Agency = (string)row["Agency"];
            daily.Amount = (decimal)row["Invoice"];
            daily.PDate = (DateTime)row["IDate"];
            daily.Revenue = (string)row["Revenue"];

            payments.Add(daily);
        }

        List<DailySummary> summaryList = new List<DailySummary>();
        List<string> revenueList = payments.Select(d => d.Revenue).Distinct<string>().ToList();

        DateTime today = date.Date;
        DateTime yesterday = today.AddDays(-1);
        DateTime thisMonth = new DateTime(today.Year, today.Month, 1);
        DateTime lastMonth = thisMonth.AddMonths(-1);
        DateTime thisYear = new DateTime(today.Year, 1, 1);
        DateTime lastYear = thisYear.AddYears(-1);

        foreach (string revenue in revenueList)
        {
            IEnumerable<DailyPayment> tp = payments.Where(p => p.Revenue == revenue);
            DailySummary summary = new DailySummary();
            summary.Agency = tp.Where(x => x.Revenue == revenue).First().Agency;
            summary.Revenue = revenue;

            summary.ThisDay = tp.Where(d => d.PDate == today).Sum(d => d.Amount);
            summary.LastDay = tp.Where(d => d.PDate == yesterday).Sum(d => d.Amount);
            summary.ThisMonth = tp.Where(d => d.PDate >= thisMonth && d.PDate <= today).Sum(d => d.Amount);
            summary.LastMonth = tp.Where(d => d.PDate >= lastMonth && d.PDate < thisMonth).Sum(d => d.Amount);
            summary.ThisYear = tp.Where(d => d.PDate >= thisYear && d.PDate <= today).Sum(d => d.Amount);
            summary.LastYear = tp.Where(d => d.PDate >= lastYear && d.PDate < thisYear).Sum(d => d.Amount);

            summaryList.Add(summary);
        }

        List<string> agencyList = payments.Select(d => d.Agency).Distinct<string>().ToList();

        return summaryList.OrderBy(x => x.Agency).ToList();
    }
    public static DataTable GetDailyInvoice(string agency, int currencyId)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_DailyInvoice", agency, currencyId).Tables[0];
    }

    public static DataTable AgenciesInvoices(int currencyId, DateTime start, DateTime end)
    {
        return SqlHelper.ExecuteDataset(connString, "charts_AgenciesInvoices", currencyId, start, end).Tables[0];
    }

    
}