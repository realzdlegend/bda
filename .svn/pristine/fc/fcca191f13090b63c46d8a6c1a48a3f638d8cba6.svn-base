﻿using System.Data;
using BDA.Domain.Entity;
using BDA.Logic.interfaces;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace BDA.Logic.logic
{
    public class MobileRepository : BaseRepository<UserClaim, Guid>
    {
        public MobileRepository(ISession _session)
            : base(_session)
        {

        }

        string _url = "https://android.googleapis.com/gcm/send";

        public MobileRepository() : base() { }

        public void NotifyAll(NotificationType noType)
        {
            var query = _session.QueryOver<UserClaim>();
            query.Where(x => x.RegistrationId != null);
            query.Where(x => x.RegistrationId != string.Empty);

            Notify(noType, query.Select(x => x.RegistrationId).List<string>());
        }

        public void Notify(NotificationType noType, List<string> invoiceNumbers, List<int> agencyList)
        {
            List<int> orgIds = new List<int>();

            if (invoiceNumbers != null && invoiceNumbers.Count > 0)
            {
                var query = _session.QueryOver<Invoice>();
                query.Where(Restrictions.In(Projections.Property<Invoice>(x => x.InvoiceNumber), invoiceNumbers.ToArray()));
                IList<int> customerIds = query.Select(x => x.ProviderId).List<int>();

                if (customerIds != null && customerIds.Count > 0) orgIds.AddRange(customerIds);
            }
            if (agencyList != null && agencyList.Count > 0) orgIds.AddRange(agencyList);

            Notify(noType, orgIds);
        }

        public void Notify(NotificationType noType, Guid userKey)
        {
            UserClaim claim = _session.Get<UserClaim>(userKey);
            IList<string> list = new List<string>();
            if (claim != null) list.Add(claim.RegistrationId);

            Notify(noType, list);
        }

        public void Notify(NotificationType noType, List<int> orgIds)
        {
            try
            {
                if (orgIds == null || orgIds.Count == 0) return;

                var queryA = _session.QueryOver<Agency>();
                queryA.Where(x => x.IsAdmin);
                IList<int> alist = queryA.Select(x => x.Id).List<int>();

                orgIds.AddRange(alist);

                var query = _session.QueryOver<UserClaim>();
                query.Where(Restrictions.In(Projections.Property<UserClaim>(x => x.OrganisationId), orgIds.ToArray()));
                query.Where(x => x.RegistrationId != null);
                query.Where(x => x.RegistrationId != string.Empty);

                Notify(noType, query.Select(x => x.RegistrationId).List<string>());
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);              
            }          
        }

        public void Notify(NotificationType noType, IList<string> regList)
        {
            try
            {
                GCMRequest request = new GCMRequest();
                request.collapse_key = noType.ToString();
                request.data = new GCMData() { update = (int)noType, msg = noType.ToString() };
                request.dry_run = false;
                request.registration_ids = regList.Where(r => !string.IsNullOrWhiteSpace(r)).ToArray();

                if (request.registration_ids == null || request.registration_ids.Count() == 0)
                    return;

                var client = new HttpClient { BaseAddress = new Uri(_url) };
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "key=AIzaSyCT5PxCI72cet6GscKIIXNxjwm03HD77LU");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string requestString = JsonConvert.SerializeObject(request);
                string dataString = JsonConvert.SerializeObject(request.data);
                HttpContent content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var response = client.PostAsync(_url, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    GCMResponse gcmResponse = JsonConvert.DeserializeObject<GCMResponse>(responseString);
                    Log(noType.ToString(), dataString, (int)response.StatusCode, gcmResponse.multicast_id, requestString, responseString,
                        gcmResponse.success, gcmResponse.failure, gcmResponse.canonical_ids);
                    if (gcmResponse.canonical_ids > 0 || gcmResponse.failure > 0)
                    {
                        for (int i = 0; i < gcmResponse.results.Count(); i++)
                        {
                            GCMResult result = gcmResponse.results[i];
                            string oldReg = request.registration_ids[i];
                            var query = _session.QueryOver<UserClaim>();
                            query.Where(x => x.RegistrationId == oldReg);
                            UserClaim claim = query.List().FirstOrDefault();

                            if (!string.IsNullOrEmpty(result.error))
                            {
                                if (result.error == "NotRegistered" || result.error == "InvalidRegistration")
                                {
                                    //remove the reg Id
                                    claim.RegistrationId = "";
                                }
                            }
                            else if (!string.IsNullOrEmpty(result.registration_id))
                            {
                                //update reg Id
                                claim.RegistrationId = result.registration_id;
                            }
                            _session.Update(claim);
                            _session.Flush();
                        }
                    }
                }
                else
                {
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    Log(noType.ToString(), dataString, (int)response.StatusCode, "", requestString, responseString, -1, -1, -1);
                }
            }
            catch (Exception ex)
            {       
               ErrorLogger.Log(ex);
            }
         
        }

        public void Log(string noType, string data, int statusCode, string mId, string request, string response, int success, int failure, int canonical)
        {
            try
            {
                string dbConn = ConfigurationManager.ConnectionStrings["bdalog"].ToString();
                SqlHelper.ExecuteNonQuery(dbConn, "logGCM", noType, data, mId, DateTime.Now, statusCode, request, response, success, failure, canonical);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
            }
        }
    }

    public class GCMRequest
    {
        public string[] registration_ids { get; set; }
        public string collapse_key { get; set; }
        public GCMData data { get; set; }
        //public bool delay_while_idle { get; set; }
        //public int time_to_live { get; set; }
        public bool dry_run { get; set; }
    }

    public class GCMResponse
    {
        public string multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public GCMResult[] results { get; set; }
    }

    public class GCMResult
    {
        public string message_id { get; set; }
        public string registration_id { get; set; }
        public string error { get; set; }
    }

    public class GCMData
    {
        //public GCMData(string lid) { last_id = lid; }
        public int update { get; set; }
        public string msg { get; set; }
    }

    public enum NotificationType : int
    {
        NewInvoice = 1,
        NewPayment = 2,
        DeleteInvoice = 3,
        DeletePayment = 4,
        VoidInvoice = 5
    }
}
