﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDA.Domain.Entity;
using BDA.Logic.logic;

public partial class Locations_MergeLocation : PageBase
{
    private readonly LocationRepository _locationRepo = new LocationRepository();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(_queryString["location"]))
                lbStatus.Text = string.Format("Location {0} saved successfully", _queryString["location"]);
            else if (!string.IsNullOrEmpty(_queryString["deleted"]))
                lbStatus.Text = string.Format("Location {0} deleted successfully", _queryString["deleted"]);
            else
                lbStatus.Text = "";

            if (CurrentRole.RoleName == "Agency")
            {
                ddAgency.SelectedValue = CurrentUser.Organization.Id.ToString();
                ddAgency.Enabled = false;
            }
            ddRegion.DataBind();
            bindLocation();



        }
        else
        {
            //  MaintainCheckbox(gvLocations);

        }
    }

    private void RememberOldValues(GridView gv)
    {
        List<int> categoryIDList = new List<int>();
        int index = -1;
        foreach (GridViewRow row in gv.Rows)
        {
            try
            {
                index = (int)gv.DataKeys[row.RowIndex].Value;
            }
            catch (Exception)
            {
                continue;
            }

            bool result = ((CheckBox)row.FindControl("chkItem")).Checked;

            // Check in the Session
            if (Session["CHECKED_ITEMS"] != null)
                categoryIDList = (List<int>)Session["CHECKED_ITEMS"];
            if (result)
            {
                if (!categoryIDList.Contains(index))
                    categoryIDList.Add(index);
            }
            else
                categoryIDList.Remove(index);
        }
        if (categoryIDList != null && categoryIDList.Count > 0)
            Session["CHECKED_ITEMS"] = categoryIDList;
    }

    private void RePopulateValues(GridView gv)
    {
        List<int> categoryIDList = (List<int>)Session["CHECKED_ITEMS"];
        if (categoryIDList != null && categoryIDList.Count > 0)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                int index = (int)gv.DataKeys[row.RowIndex].Value;
                if (categoryIDList.Contains(index))
                {
                    CheckBox myCheckBox = (CheckBox)row.FindControl("chkItem");
                    myCheckBox.Checked = true;
                }
            }
        }
    }

    private void Recheck()
    {

        ArrayList CheckBoxArray = (ArrayList)ViewState["CheckBoxArray"];
        string checkAllIndex = "chkAll-" + gvLocations.PageIndex;

        if (CheckBoxArray.IndexOf(checkAllIndex) != -1)
        {
            CheckBox chkAll =
                (CheckBox)gvLocations.HeaderRow.Cells[0].FindControl("chkAll");
            chkAll.Checked = true;
        }
        for (int i = 0; i < gvLocations.Rows.Count; i++)
        {
            if (gvLocations.Rows[i].RowType == DataControlRowType.DataRow)
            {
                if (CheckBoxArray.IndexOf(checkAllIndex) != -1)
                {
                    CheckBox chk =
                        (CheckBox)gvLocations.Rows[i].Cells[0].FindControl("chkItem");
                    chk.Checked = true;

                }
                else
                {
                    int CheckBoxIndex = gvLocations.PageSize * (gvLocations.PageIndex) + (i + 1);
                    if (CheckBoxArray.IndexOf(CheckBoxIndex) != -1)
                    {
                        CheckBox chk =
                            (CheckBox)gvLocations.Rows[i].Cells[0].FindControl("chkItem");
                        chk.Checked = true;

                    }
                }
            }
        }
    }

    private void MaintainCheckbox(GridView gv)
    {
        ArrayList CheckBoxArray;
        if (ViewState["CheckBoxArray"] != null)
        {
            CheckBoxArray = (ArrayList)ViewState["CheckBoxArray"];
        }
        else
        {
            CheckBoxArray = new ArrayList();
        }

        int CheckBoxIndex;
        bool CheckAllWasChecked = false;
        CheckBox chkAll =
            (CheckBox)gv.HeaderRow.Cells[0].FindControl("chkAll");
        string checkAllIndex = "chkAll-" + gv.PageIndex;
        if (chkAll.Checked)
        {
            if (CheckBoxArray.IndexOf(checkAllIndex) == -1)
            {
                CheckBoxArray.Add(checkAllIndex);
            }
        }
        else
        {
            if (CheckBoxArray.IndexOf(checkAllIndex) != -1)
            {
                CheckBoxArray.Remove(checkAllIndex);
                CheckAllWasChecked = true;
            }
        }
        for (int i = 0; i < gv.Rows.Count; i++)
        {
            if (gv.Rows[i].RowType == DataControlRowType.DataRow)
            {
                CheckBox chk =
                    (CheckBox)gv.Rows[i].Cells[0].FindControl("chkItem");
                CheckBoxIndex = gv.PageSize * gv.PageIndex + (i + 1);
                if (chk.Checked)
                {
                    if (CheckBoxArray.IndexOf(CheckBoxIndex) == -1
                        && !CheckAllWasChecked)
                    {
                        CheckBoxArray.Add(CheckBoxIndex);
                    }
                }
                else
                {
                    if (CheckBoxArray.IndexOf(CheckBoxIndex) != -1
                        || CheckAllWasChecked)
                    {
                        CheckBoxArray.Remove(CheckBoxIndex);
                    }
                }
            }
        }

        ViewState["CheckBoxArray"] = CheckBoxArray;

    }

    protected void odsLocations_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["agencyId"] = (CurrentRole.RoleName == "Agency")
            ? CurrentUser.Organization.Id
            : int.Parse(ddAgency.SelectedValue);
        //e.InputParameters["isRegion"] = -1;
        e.InputParameters["isRegion"] = null;
        e.InputParameters["stateId"] = -1;
        e.InputParameters["regionId"] = int.Parse(ddRegion.SelectedValue);
    }

    protected void grdView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var index = int.Parse(e.Row.RowIndex.ToString());
        if (index == -1) return;
        var locationId = int.Parse(gvLocations.DataKeys[index]["Id"].ToString());
        if (CurrentRole.RoleName != "Agency")
        {
            gvLocations.Columns[9].Visible = false;
            // e.Row.Cells[9].Visible = false;
        }
        //else if (_locationRepo.IsAttachedtoInvoice(locationId)||_locationRepo.IsAttachedtoUser(locationId))
        //    e.Row.Cells[9].Text = "";
        //  Recheck();
        // RePopulateValues(gvLocations);
    }

    protected string ToolTips(object Id)
    {
        int locId = (int)Id;
        if (_locationRepo.IsAttachedtoInvoice(locId) || _locationRepo.IsAttachedtoUser(locId))
        {
            return "This invoice is either attached to an invoice or a user.";
        }
        else
        {
            return "";
        }
    }

    private void bindLocation()
    {
        int agencyId = (CurrentRole.RoleName == "Agency")
            ? CurrentUser.Organization.Id
            : int.Parse(ddAgency.SelectedValue);
        int stateId = -1;
        int regionId = int.Parse(ddRegion.SelectedValue);

        var locations = _locationRepo.GetLocations(agencyId, null, stateId, regionId, 0, 0);
        ddlLocations.DataSource = locations;
        ddlLocations.DataBind();


    }

    protected void grdView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //    if (CurrentRole.RoleName != "Agency")
        //        //HyperLink1.Visible = false;
        //        return;
        //    int index = int.Parse(e.CommandArgument.ToString());
        //    if (e.CommandName == "view")
        //    {
        //        string locationId = gvLocations.DataKeys[index]["Id"].ToString();
        //        Response.Redirect(URLEncryption.EncryptURL("EditLocation.aspx", "locationId", locationId));
        //    }
        //    if (e.CommandName != "deleteL") return;
        //    try
        //    {
        //        var id = int.Parse(gvLocations.DataKeys[index]["Id"].ToString());
        //        _locationRepo.DeleteLocation(id);
        //        lbStatus.Text = @"Location deleted successfully";
        //        gvLocations.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        lbStatus.ForeColor = Color.Red;
        //        lbStatus.Text = ex.Message;
        //    }
    }

    protected void ddRegion_DataBound(object sender, EventArgs e)
    {
        ddRegion.Items.Insert(0, new ListItem("All Locations", "-1"));
    }

    protected void ddAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddRegion.DataBind();

        gvLocations.PageIndex = 0;
        gvLocations.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvLocations.PageIndex = 0;
        gvLocations.DataBind();

        bindLocation();
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void chkHeader_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckAll(gvLocations);
        if (((CheckBox)gvLocations.HeaderRow.FindControl("chkHeader")).Checked)
        {
            bool isAllChecked = true;
            ViewState["isAllChecked"] = true;
        }
    }

    protected void btnSelect_OnClick(object sender, EventArgs e)
    {
        //check selected 
        var ids = GetSelectedLocations(gvLocations);
        if (ids.Count == 0)
        {
            ShowMessage(this, "Please select location", null);
            return;

        }
        var locations = _locationRepo.GetLocations(ids);
        //Session["selectedLocations"] = locations;
        //GvParent.DataSource = locations;
        //GvParent.DataBind();

    }

    //protected void btnDeselect_OnClick(object sender, EventArgs e)
    //{
    //    var idDeselected = GetSelectedLocations(GvParent);
    //    var idSelected = GetIdInGrid(GvParent);

    //    var diff = idSelected.Where(x=>!idDeselected.Contains(x)).ToList();

    //    var locations = _locationRepo.GetLocations(diff);
    //    //Session["selectedLocations"] = locations;
    //    GvParent.DataSource = locations;
    //    GvParent.DataBind();
    //}

    private List<int> GetSelectedLocations(GridView view)
    {
        List<int> Ids = new List<int>();

        foreach (GridViewRow row in view.Rows)
        {
            if (((CheckBox)row.FindControl("chkItem")).Checked)
            {
                Ids.Add(int.Parse(view.DataKeys[row.RowIndex].Value.ToString()));
            }
        }
        return Ids;
    }

    private List<int> GetIdInGrid(GridView view)
    {
        List<int> Ids = new List<int>();

        foreach (GridViewRow row in view.Rows)
        {
            //if (((CheckBox)row.FindControl("chkItem")).Checked)
            //{
            Ids.Add(int.Parse(view.DataKeys[row.RowIndex].Value.ToString()));
            //}
        }
        return Ids;
    }

    protected void btnMerge_OnClick(object sender, EventArgs e)
    {
        ArrayList categoryIDList = (ArrayList)Session["CHECKED_ITEMS"];

        var ids = categoryIDList == null ? GetSelectedLocations(gvLocations) : (List<int>)Session["CHECKED_ITEMS"];
        if (ids.Count == 0)
        {
            ShowMessage(this, "Please select at least two locations", null);
            return;

        }

        int ParentLocation = int.Parse(ddlLocations.SelectedValue);
        if (ParentLocation == -1)
        {
            ShowMessage(this, "Please select the parent location", null);
            return;
        }
        _locationRepo.MergeLocation(ids, ParentLocation);

        Session["CHECKED_ITEMS"] = null;

        gvLocations.DataBind();

    }

    protected void ddlLocations_OnDataBound(object sender, EventArgs e)
    {
        ddlLocations.Items.Insert(0, new ListItem("-Select-", "-1"));
    }


    protected void gvLocations_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        RememberOldValues(gvLocations);
        gvLocations.PageIndex = e.NewPageIndex;
        gvLocations.DataBind();
        // RePopulateValues(gvLocations);
    }

    protected void gvLocations_OnDataBound(object sender, EventArgs e)
    {
        RePopulateValues(gvLocations);
    }


    protected void odsLocations_OnSelected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.ReturnValue != null && e.ReturnValue.GetType() == System.Type.GetType("System.Int32"))
        {
            string str = e.ReturnValue.ToString();
            int val = int.Parse(str);

            if (val <= 0)
            {
                btnMerge.Visible = false;
            }
        }
    }
}