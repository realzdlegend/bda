﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDA.Domain.Entity
{
    [Serializable]
    public class InvoiceView : Entity<long>
    {
        public virtual string InvoiceNumber { get; set; }
        public virtual DateTime InvoiceDate { get; set; }
        public virtual Guid CreatedBy { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal? PaymentPrediction { get; set; }
        public virtual int ProviderId { get; set; }
        public virtual ServiceProvider Provider { get; set; }

        public virtual int ProviderParentId { get; set; }
        public virtual ServiceProvider ProviderParent { get; set; }

        public virtual int AgencyId { get; set; }
        public virtual int RevenueId { get; set; }
        public virtual Revenue Revenue { get; set; }
        public virtual int CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }


        public virtual int LocationParentId { get; set; }
        public virtual Location LocationParent { get; set; }

        public virtual bool Sent { get; set; }
        public virtual bool Deleted { get; set; }
        public virtual bool SentToFidesic { get; set; }
        public virtual int PaymentCount { get; set; }
        public virtual decimal AmountPaid { get; set; }
        public virtual string DepositOption { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime PaymentDate { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime DueDate { get; set; }
        public virtual int ReturnedInvoiceId { get; set; }
        public virtual IList<InvoiceBill> Bills { get; set; }
        public virtual int BillCount { get; set; }
        public virtual string RemitaRetrievalReference { get; set; }

        public virtual string Address
        {
            get
            {
                return Location.Address;
            }
        }

        public virtual string Status
        {
            get
            {
                if (Deleted)
                    return "Voided";
                if (Sent && (AmountPaid >= Amount))
                    return "Paid";
                //if (Sent)
                //    return "Unpaid (Sent)";
                if (AmountPaid < Amount && (AmountPaid > 0))
                    return "Partly paid";
                if (AmountPaid <= 0)
                    return "Unpaid";
                return "Not Sent ";
            }
        }

        public virtual bool FullyPaid
        {
            get
            {
                return AmountPaid >= Amount;
            }
        }

        public virtual string AmountString
        {
            get
            {
                string code = Currency.UTF32Code.HasValue ? char.ConvertFromUtf32(Currency.UTF32Code.Value) : "";
                return string.Format("{0} {1}", code, Amount.ToString("#,###.#0"));
            }
        }
        public virtual string LocationCode
        {
            get
            {
                return Location.Code;
            }
        }
        public virtual string Agency
        {
            get
            {
                return Revenue.Agency.Name;
            }
        }

        public virtual string AmountPaidString
        {
            get
            {

                string code = (CurrencyId == 566) ? char.ConvertFromUtf32(8358) :
                    (CurrencyId == 840) ? char.ConvertFromUtf32(36) : "";
                return string.Format("{0} {1}", code, AmountPaid.ToString("#,###.#0"));
            }
        }

        public virtual string RevenueGroup
        {
            get
            {
                return Revenue.AgencyRevenueGroup != null ? Revenue.AgencyRevenueGroup.RevenueGroup : string.Empty;

            }
        }

        public virtual string RevenueGroupName
        {
            get
            {
                return Revenue.AgencyRevenueGroup != null ? Revenue.AgencyRevenueGroup.Name : string.Empty;
            }
        }
    }
}
