﻿using BDA.Domain.Models;
using BDA.Logic.logic; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BDA.Services.Controllers
{
    public class MobileController : BaseController
    {
        readonly PaymentRepository payRepo = new PaymentRepository();
        readonly InvoiceRepository invoiceRepo = new InvoiceRepository();
        AuthorizationRepository authRepo = new AuthorizationRepository();


        [HttpGet]
        public HttpResponseMessage Sync(DateTime lastInvoice, DateTime lastPayment)
        {
            try
            {
                InvoicePaymentModel list = new InvoicePaymentModel();

                DateTime isd = lastInvoice; // DateTime.ParseExact(lastInvoice, "yyyy-MM-ddTHH:mm:ss", null);
                var invoices = invoiceRepo.SearchInvoices(UserKey, isd, 0, 200);
                //if (invoices == null || invoices.Count == 0)
                //    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No invoices found");

                list.Invoices = (from a in invoices.AsEnumerable() select a.ToModel()).ToList();
                list.InvoiceCount = invoiceRepo.SearchInvoicesPageCount(UserKey, isd);

                DateTime sd = lastPayment; // DateTime.ParseExact(lastPayment, "yyyy-MM-ddTHH:mm:ss", null);
                var payments = payRepo.SearchPayments(UserKey, sd, 0, 200);
                //if (payments == null || payments.Count == 0)
                //    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No payments found");

                list.Payments = (from a in payments.AsEnumerable() select a.ToModel()).ToList();
                list.PaymentCount = payRepo.SearchPaymentCount(UserKey, sd);

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage Sync(DateTime lastSync, int days, int page)
        {
            try
            {
                InvoicePaymentModel list = new InvoicePaymentModel();

                DateTime startDate = DateTime.Now.AddDays(-days).Date;

                int pageSize = 200;
                int startIndex = page * pageSize;

                var invoices = invoiceRepo.SearchInvoices(UserKey, lastSync, startIndex, pageSize);
                list.Invoices = (invoices == null || invoices.Count == 0) ? new List<InvoiceDetailModel>() :
                    (from a in invoices.AsEnumerable() select a.ToModel()).ToList();
                list.InvoiceCount = invoiceRepo.SearchInvoicesPageCount(UserKey, lastSync);

                var payments = payRepo.SearchPayments(UserKey, lastSync, startIndex, pageSize);
                list.Payments = (payments == null || payments.Count == 0) ? new List<PaymentModel>() :
                    (from a in payments.AsEnumerable() select a.ToModel()).ToList();
                list.PaymentCount = payRepo.SearchPaymentCount(UserKey, lastSync);

                list.VoidedInvoices = invoiceRepo.GetVoidedInvoices(startDate, lastSync, startIndex, pageSize).ToList();

                var paidInvoices = payRepo.PaidInvoices(UserKey, startDate, lastSync, startIndex, pageSize);

                list.PaidInvoices = (paidInvoices == null || paidInvoices.Count == 0) ? new List<InvoiceDetailModel>() :
                    (from a in paidInvoices.AsEnumerable() select a.ToModel()).ToList();

                list.PageNumber = page;
                int total = list.InvoiceCount > list.PaymentCount ? list.InvoiceCount : list.PaymentCount;
                int remainder = 0;
                int pageCount = Math.DivRem(total, pageSize, out remainder);
                list.PageCount = remainder > 0 ? pageCount + 1 : pageCount;

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage RegistrationId(string id)
        {
            try
            {
                authRepo.UpdateRegistrationId(UserKey, id);

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
