﻿using BDA.Domain.Models;
using BDA.Domain.Util;
using BDA.Logic.logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

  
public partial class Administration_RevenueBeneficiary : PageBase
{
    BDA.Client.logic.RevenueRepository revenueRepo = new BDA.Client.logic.RevenueRepository();
    private int RevenueId
    {
        get
        {
            int revenueId = 0;
            if (!string.IsNullOrEmpty(_queryString["RevenueId"]))
            {
                revenueId = Convert.ToInt32(_queryString["RevenueId"]);
                if (revenueId > 0)
                {
                    Session["RevenueId"] = revenueId;
                }
                else
                {
                    revenueId = (int)Session["RevenueId"];
                }

                return revenueId;
               
            }
            else
            {
                revenueId = (int)Session["RevenueId"];
                return revenueId;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PageDefaults();
        }
    }

    private void PageDefaults()
    {
        var revenue = revenueRepo.Revenue(RevenueId);
        Session["Revenue"] = revenue;
        var  revenueBens = revenueRepo.GetRevenueBeneficiaries(revenue.Id);
        if (revenueBens == null)
        {
            revenueBens = new List<RevenueBeneficiaryModel>();
        }
        Session["revenue-bens"] = revenueBens;
        benMapGrid.DataSource = revenueBens;
        benMapGrid.DataBind();
        lblRev.Text = revenue.Name;

        var accounts = revenueRepo.GetSettlementAccounts();
        Session["accounts"] = accounts;
        
        viewPnl.Visible = true;
        newPnl.Visible = false;
        editPnl.Visible = false;
    }

    protected void newBen_Click(object sender, EventArgs e)
    {
        viewPnl.Visible = false;
        newPnl.Visible = true;
        var accounts = Session["accounts"] as IEnumerable<SettlementAccountModel>;
        accounts = accounts.OrderBy(x => x.AccountName);
        ddAccounts.DataSource = accounts;
        ddAccounts.DataBind();
        lblError.Text = string.Empty;
        txtShare.Text = string.Empty;
    }

    protected void editMap_Click(object sender, EventArgs e)
    {
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        var mappingId = int.Parse(grdrow.Cells[0].Text);
        Session["mappinId"] = mappingId;
        var revenueBens = Session["revenue-bens"] as IEnumerable<RevenueBeneficiaryModel>;
        var revenueBen = revenueBens.FirstOrDefault(x => x.Id == mappingId);
        txtEditShare.Text = revenueBen.SharePercentage.ToString();
        var accounts = Session["accounts"] as IEnumerable<SettlementAccountModel>;
        ddEditAccount.DataSource = accounts;
        ddEditAccount.SelectedValue = revenueBen.SettlementAccountId.ToString();
        ddEditAccount.DataBind();

        ddEditDeductFee.SelectedValue = revenueBen.DeductFeeFrom ? "1" : "0";
        viewPnl.Visible = false;
        newPnl.Visible = false;
        editPnl.Visible = true;
        lblEditError.Text = string.Empty;
    }

    protected void saveBtn_Click(object sender, EventArgs e)
    {
        try
        {
            var revenueBens = Session["revenue-bens"] as IEnumerable<RevenueBeneficiaryModel>;
            float totalPercentage = revenueBens.Sum(x => x.SharePercentage);
            var deductFeeItem = revenueBens.FirstOrDefault(x => x.DeductFeeFrom == true);
           

            if (!AppHelper.CheckShareFormat(txtShare.Text))
            {
                lblError.Text = "Error: Share percentage for revenue should be between 0 and 100";
                lblError.Visible = true;
                return;
            }

            var model = new RevenueBeneficiaryModel
            {
                RevenueId = RevenueId,
                SettlementAccountId = int.Parse(ddAccounts.SelectedValue),
                SharePercentage = float.Parse(txtShare.Text),
                DeductFeeFrom  =  ddDeductFee.SelectedValue == "0"? false: true
            };

            //check deduct fee here

            if (deductFeeItem != null && model.DeductFeeFrom == true)
            {
                lblError.Text = "Error: Deduct Fee setting can only be configured for one beneficiary";
                lblError.Visible = true;
                return;
            }
            //check sum of share percentage
            totalPercentage += model.SharePercentage;
            if (totalPercentage > 100)
            {
                lblError.Text = "Error: Total share percentage for revenue cannot be greater than 100";
                lblError.Visible = true;          
                return;
            }
            // check for duplicate ben
            var dup = revenueBens.FirstOrDefault(x => x.SettlementAccountId == model.SettlementAccountId);
            if (dup != null)
            {
                lblError.Text = "Error: Mapping already exists for this beneficiary";
                lblError.Visible = true;
                return;
            }

            var res = revenueRepo.CreateRevenueBeneficiary(model, CurrentUser.Id);
            if (res)
            {
                lblSuccess.Text = "Beneficiary mapping successful";
                lblSuccess.Visible = true;
                PageDefaults();
            }
            else
            {
                lblError.Text = "Error: Could not map beneficiary";
                lblError.Visible = true;           
                return;
            }
            

        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
            lblError.Text = "Unexpected Error: Could not map beneficiary";
            lblError.Visible = true;
            return;
        }
        

    }

    protected void btnCancelNew_Click(object sender, EventArgs e)
    {
        PageDefaults();
        lblSuccess.Text = string.Empty;
    }

    


    protected void UpdateBtn_Click(object sender, EventArgs e)
    {
        try
        {
            int mappingId = (int)Session["mappinId"];
            var revenueBens = Session["revenue-bens"] as IEnumerable<RevenueBeneficiaryModel>;
            var items = revenueBens.ToList();
            var item = revenueBens.FirstOrDefault(x => x.Id == mappingId);
            items.Remove(item);

            
            float totalPercentage = items.Sum(x => x.SharePercentage);
            var deductFeeItem = items.FirstOrDefault(x => x.DeductFeeFrom == true);


            if (!AppHelper.CheckShareFormat(txtEditShare.Text))
            {
                lblEditError.Text = "Error: Share percentage for revenue should be between 0 and 100";
                lblEditError.Visible = true;
                return;
            }

            var model = new RevenueBeneficiaryModel
            {
                RevenueId = RevenueId,
                SettlementAccountId = int.Parse(ddEditAccount.SelectedValue),
                SharePercentage = float.Parse(txtEditShare.Text),
                DeductFeeFrom = ddEditDeductFee.SelectedValue == "0" ? false : true
            };

            //check deduct fee here

            if (deductFeeItem != null && model.DeductFeeFrom == true)
            {
                lblEditError.Text = "Error: Deduct Fee setting can only be configured for one beneficiary";
                lblEditError.Visible = true;
                return;
            }
            //check sum of share percentage
            totalPercentage += model.SharePercentage;
            if (totalPercentage > 100)
            {
                lblEditError.Text = "Error: Total share percentage for revenue cannot be greater than 100";
                lblEditError.Visible = true;
                return;
            }
            

            var res = revenueRepo.UpdateRevenueBeneficiary(model, CurrentUser.Id,mappingId);
            if (res)
            {
                lblSuccess.Text = "Beneficiary mapping updated successfully";
                lblSuccess.Visible = true;
                PageDefaults();
            }
            else
            {
                lblEditError.Text = "Error: Could not update mapping";
                lblEditError.Visible = true;
                return;
            }


        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
            lblEditError.Text = "Unexpected Error: Could not update mapping";
            lblEditError.Visible = true;
            return;
        }



    }

    protected void CancelEditBtn_Click(object sender, EventArgs e)
    {
        PageDefaults();
        lblSuccess.Text = string.Empty;
    }

    protected void remove_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            // string rowNumber = grdrow.Cells[0].Text;
            int itemId = Convert.ToInt32(grdrow.Cells[0].Text);
            var res = revenueRepo.RemoveRevenueBeneficiary(itemId, CurrentUser.Id);

            if (res)
            {
                lblSuccess.Text = "Successfully removed mapping";
                lblSuccess.Visible = true;
                PageDefaults();
            }
            else
            {
                lblError.Text = "Error: Failed to remove mapping";
                lblError.Visible = true;
                return;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Failed to remove mapping. Unexpected error occured.";
            lblError.Visible = true;
            return;          
        }
        

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Agency/RevenueSettings.aspx");
    }
}