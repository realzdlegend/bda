﻿using BDA.Domain.Dtos;
using BDA.Domain.Entity;
using BDA.Domain.Models;
using BDA.Domain.Search;
using BDA.Domain.Util.ApiSettings;
using System;
using System.Collections.Generic;
using StackifyLib;

namespace BDA.Client.logic
{
    public class InvoiceRepository : BaseRepository
    {
        InvoiceAPI invoiceAPI = new InvoiceAPI();
        BillAPI billAPI = new BillAPI();
        PaymentAPI paymentAPI = new PaymentAPI();

       

        public ApiResponse<T> CreateInvoice<T>(int systemId, string revenueCode, string providerCode, int currencyId, string locationCode,
          CustomerInvoice customer, List<BillModel> items, DateTime? invoiceDate, int? revGroupId = null)
        {
            var invoiceModel = new NewInvoiceModel()
            {
                Bills = items,
                Address = customer.Address,
                CurrencyId = currencyId,
                CustomerAIN = providerCode,
                Emails = string.Join(", ", customer.Emails.ToArray()),
                InvoiceDate = invoiceDate,
                LocationCode = locationCode,
                Name = customer.Name,
                Phone = customer.Phone,
                RevenueCode = revenueCode,
                RevenueGroupId = revGroupId
            };
            return Post<T>(invoiceModel, invoiceAPI.New);
        }

        public ApiResponse<T> SimilarInvoice<T>(int revenueId, int customerId, decimal amount)
        {
            try
            {
                var model = new InvoiceDataModel()
                {
                    revenueId = revenueId,
                    customerId = customerId,
                    amount = amount
                };
                
                return Post<T>(model, invoiceAPI.Similar);
            }
            catch (Exception ex)
            {
                CommonLogger.ErrorLogger.Log(ex);
                throw;
            }
           
        }

        public List<VersionInvoice> SearchInvoices(bool isAdmin, int agencyId, int providerId,
            object locationIds, object revenueIds, int status, int currencyId, DateTime startDate, DateTime endDate, int dateType,
            string invoiceNumber, string name, int startIndex, int pageSize, string state)
        {
            var _locationIds = locationIds as List<int>;
            var _revenueIds = revenueIds as List<int>;

            var ism = new InvoiceDataModel()
            {
                agencyId = agencyId,
                currencyId = currencyId,
                dateType = dateType,
                invoiceNumber = invoiceNumber,
                endDate = endDate,
                locationIds = _locationIds,
                name = name,
                pageSize = pageSize == 0 ? 10 : pageSize,
                providerId = providerId,
                revenueIds = _revenueIds,
                startDate = startDate,
                startIndex = startIndex,
                status = status,
                state = state
            };
            return Post<List<VersionInvoice>>(ism, invoiceAPI.Search).Data;
        }
        
        public int SearchInvoicesCount(bool isAdmin, int agencyId, int providerId, object locationIds,
            object revenueIds, int status, int currencyId, DateTime startDate, DateTime endDate, int dateType,
            string invoiceNumber, string name, int startIndex, int pageSize, string state)
        {


            var _locationIds = locationIds as List<int>;
            var _revenueIds = revenueIds as List<int>;
            var ism = new InvoiceDataModel()
            {
                agencyId = agencyId,
                currencyId = currencyId,
                dateType = dateType,
                invoiceNumber = invoiceNumber,
                endDate = endDate,
                locationIds = _locationIds,
                name = name,
                pageSize = pageSize,
                providerId = providerId,
                revenueIds = _revenueIds,
                startDate = startDate,
                startIndex = startIndex,
                status = status,
                state = state
            };
            return Post<int>(ism, invoiceAPI.SearchCount).Data;
        }

        public IList<InvoiceSearchSummaryModel> SearchInvoicesSummary(bool isAdmin, int agencyId, int providerId, object locationIds,
            object revenueIds, int status, int currencyId, DateTime startDate, DateTime endDate, int dateType,
            string invoiceNumber, string name, string state)
        {

            var _locationIds = locationIds as List<int>;
            var _revenueIds = revenueIds as List<int>;
            var ism = new InvoiceDataModel()
            {
                agencyId = agencyId,
                currencyId = currencyId,
                dateType = dateType,
                invoiceNumber = invoiceNumber,
                endDate = endDate,
                locationIds = _locationIds,
                name = name,
                providerId = providerId,
                revenueIds = _revenueIds,
                startDate = startDate,
                status = status,
                state = state
            };
            return Post<IList<InvoiceSearchSummaryModel>>(ism, invoiceAPI.SearchSummary).Data;
        }

        public bool CanVoid(VersionInvoice invoice)
        {
            return !(invoice == null || invoice.Deleted);
        }

        public IList<BillView> GetInvoiceBills(long invoiceId, int startIndex, int pageSize)
        {
            var ism = new InvoiceDataModel()
            {
                pageSize = pageSize,
                startIndex = startIndex,
                invoiceId = invoiceId
            };
            return Post<IList<BillView>>(ism, billAPI.List).Data;
        }

        public int GetInvoiceBillsCount(long invoiceId)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceId = invoiceId
            };
            return Post<int>(ism, billAPI.ListCount).Data;
        }

        public IList<PaymentView> GetPayments(long invoiceId)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceId = invoiceId
            };
            return Post<IList<PaymentView>>(ism, paymentAPI.List).Data;
        }

        public int GetPaymentCount(long invoiceId)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceId = invoiceId
            };
            return Post<int>(ism, paymentAPI.ListCount).Data;
        }

        public Invoice GetInvoice(string invoiceNumber)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceNumber = invoiceNumber
            };
            return Post<Invoice>(ism, invoiceAPI.GetView).Data;
        }

        public Invoice GetInvoice(long invoiceId)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceId = invoiceId
            };
            return Post<Invoice>(ism, invoiceAPI.GetView).Data;
        }

        public bool VoidInvoice(string InvoiceNumber, string text)
        {
            var ism = new VoidInvoiceDataModel()
            {
                InvoiceNumber = InvoiceNumber,
                Reason = text
            };
            return Post<bool>(ism, invoiceAPI.Void).Data;
        }

        public VersionInvoice GetVersionInvoice(string invoiceNumber)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceNumber = invoiceNumber
            };
            return Post<VersionInvoice>(ism, invoiceAPI.GetVersion).Data;
        }

        public VersionInvoice GetVersionInvoice(long invoiceId)
        {
            var ism = new InvoiceDataModel()
            {
                invoiceId = invoiceId
            };
            return Post<VersionInvoice>(ism, invoiceAPI.GetVersion).Data;
        }
    }
}
