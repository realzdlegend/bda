﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Rotator : PageBase
{
    int _chart
    {
        get { return ViewState["_chart"] == null ? 1 : int.Parse(ViewState["_chart"].ToString()); }
        set { ViewState["_chart"] = value; }
    }

    bool _playMode
    {
        get { return ViewState["_playMode"] == null ? true : bool.Parse(ViewState["_playMode"].ToString()); }
        set { ViewState["_playMode"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ShowChart();
        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        Move();
        ShowChart();
    }

    protected void LB_Click(object sender, EventArgs e)
    {
        ImageButton button = (ImageButton)sender;
        if (button.CommandName == "play")
        {
            _playMode = !_playMode;
            button.ImageUrl = string.Format("~/App_Themes/default/images/{0}-icon.png", 
                _playMode ? "play" : "pause");
            button.ToolTip = _playMode ? "Pause" : "Play";
            Timer1.Enabled = _playMode;
        }
        else if (button.CommandName == "next")
        {
            Move();
            ShowChart();
        }
        else if (button.CommandName == "prev")
        {
            Move(false);
            ShowChart();
        }

    }

    private void Move(bool forward = true)
    {
        if(forward)
            _chart = ++_chart > 7 ? 1 : _chart;
        else
            _chart = --_chart < 1 ? 7 : _chart;
        if (UserAgency == "FAAN" && _chart == 7)
            _chart = forward ? 1 : 5;
        else if (UserAgency == "NCAA" && _chart == 6)
            _chart = forward ? 7 : 5;
    }

    private void ShowChart()
    {
        Label1.Text = "";
        if (_chart == 1)
        {
            Label1.Text = Title = "Current Year ";
            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime endDate = DateTime.Now;

            if (Roles.IsUserInRole("Agency"))
            {
                DataTable table = DataHelper.GetAgencyRevenues(UserAgency, 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);
                ccAgency.Visible = true;

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenues";
                piechart.ChartData(table);
                piechart.DataBind();
            }
            else
            {
                DataTable table = DataHelper.GetAgencyTime("", 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);
                ccAgency.Visible = true;

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenue per Agency";
                DataTable table1 = DataHelper.AgenciesInvoices(566, startDate, endDate);
                piechart.ChartData(table1);
                piechart.DataBind();
            }
        }
        else if (_chart == 2)
        {
            Label1.Text = Title = "Current Month ";

            DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime endDate = DateTime.Now;

            if (Roles.IsUserInRole("Agency"))
            {
                DataTable table = DataHelper.GetAgencyRevenues(UserAgency, 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);
                ccAgency.Visible = true;

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenues";
                piechart.ChartData(table);
                piechart.DataBind();
            }
            else
            {

                DataTable table = DataHelper.GetAgencyTime("", 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenue per Agency";
                DataTable table1 = DataHelper.AgenciesInvoices(566, startDate, endDate);
                piechart.ChartData(table1);
                piechart.DataBind();
            }
        }
        else if (_chart == 3)
        {
            Label1.Text = Title = "Last Month ";

            DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 1);
            DateTime endDate = startDate; // new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

            if (Roles.IsUserInRole("Agency"))
            {
                DataTable table = DataHelper.GetAgencyRevenues(UserAgency, 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);
                ccAgency.Visible = true;

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenues";
                piechart.ChartData(table);
                piechart.DataBind();
            }
            else
            {
                DataTable table = DataHelper.GetAgencyTime("", 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenue per Agency";
                DataTable table1 = DataHelper.AgenciesInvoices(566, startDate, endDate);
                piechart.ChartData(table1);
                piechart.DataBind();
            }
        }
        else if (_chart == 4)
        {
            Label1.Text = Title = "Today";

            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            if (Roles.IsUserInRole("Agency"))
            {
                DataTable table = DataHelper.GetAgencyRevenues(UserAgency, 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);
                ccAgency.Visible = true;

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenues";
                piechart.ChartData(table);
                piechart.DataBind();
            }
            else
            {
                DataTable table = DataHelper.GetAgencyTime("", 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenue per Agency";
                DataTable table1 = DataHelper.AgenciesInvoices(566, startDate, endDate);
                piechart.ChartData(table1);
                piechart.DataBind();
            }
        }
        else if (_chart == 5)
        {
            Label1.Text = Title = "Yesterday";

            DateTime startDate = DateTime.Now.AddDays(-1);
            DateTime endDate = DateTime.Now.AddDays(-1);

            if (Roles.IsUserInRole("Agency"))
            {
                DataTable table = DataHelper.GetAgencyRevenues(UserAgency, 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);
                ccAgency.Visible = true;

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenues";
                piechart.ChartData(table);
                piechart.DataBind();
            }
            else
            {
                DataTable table = DataHelper.GetAgencyTime("", 566, startDate, endDate);
                ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
                ccAgency.ChartData(table);

                datable.DataSource = table;
                datable.DataBind();

                piechart.GviTitle = "Revenue per Agency";
                DataTable table1 = DataHelper.AgenciesInvoices(566, startDate, endDate);
                piechart.ChartData(table1);
                piechart.DataBind();
            }
        }
        else if (_chart == 6)
        {
            Label1.Text = Title = "FAAN Monthly Income";

            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime endDate = DateTime.Now;

            DataTable table = DataHelper.GetMonthly("FAAN", 566, startDate, endDate);
            ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
            ccAgency.ChartData(table);

            datable.DataSource = table;
            datable.DataBind();

            piechart.GviTitle = "Payments per revenue line";
            DataTable table1 = DataHelper.GetAgencyRevenues("faan", 566, startDate, endDate);
            piechart.ChartData(table1);
            piechart.DataBind();
        }
        else if (_chart == 7)
        {
            Label1.Text = Title = "NCAA Monthly Income";

            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime endDate = DateTime.Now;

            DataTable table = DataHelper.GetMonthly("NCAA", 566, startDate, endDate);
            ccAgency.GviTitle = "{title: 'Invoices', titleTextStyle: {color: 'red'} }";
            ccAgency.ChartData(table);

            datable.DataSource = table;
            datable.DataBind();

            piechart.GviTitle = "Payments per revenue line";
            DataTable table1 = DataHelper.GetAgencyRevenues("ncaa", 566, startDate, endDate);
            piechart.ChartData(table1);
            piechart.DataBind();
        }
    }
}