﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.Search.Data
{
    [ElasticsearchType(Name = "payments", IdProperty = "PaymentId")]
    public class VersionPayment
    {
        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Invoice Number", DocValues = true)]
        public virtual string InvoiceNumber { get; set; }

        [Number(Name = "Invoice Id", IncludeInAll = false, DocValues = true)]
        public virtual long InvoiceId { get; set; }

        [Number(Name = "Payment Id", IncludeInAll = false, DocValues = true)]
        public virtual int PaymentId { get; set; }

        [Number(Name = "Reference Number", DocValues = true)]
        public virtual int ReferenceNumber { get; set; }

        [Number(Name = "Amount", DocValues = true)]
        public virtual decimal Amount { get; set; }

        [Number(Name = "Total Amount", DocValues = true)]
        public virtual decimal TotalAmount { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Deposit Option", DocValues = true)]
        public virtual string DepositOption { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Payment Option", DocValues = true)]
        public virtual string PaymentOption { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Payment Status", DocValues = true)]
        public virtual string PaymentStatus { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Payment Method", DocValues = true)]
        public virtual string PaymentMethod { get; set; }

        [Date(Name = "Payment Date", DocValues = true)]
        public virtual DateTime PaymentDate { get; set; }

        [Date(Name = "Due Date", DocValues = true, IncludeInAll = false)]
        public virtual DateTime DueDate { get; set; }

        [Date(Name = "Processing Date", DocValues = true, IncludeInAll = false)]
        public virtual DateTime ProcessingDate { get; set; }

        [Date(Name = "Invoice Date", DocValues = true, IncludeInAll = false)]
        public virtual DateTime InvoiceDate { get; set; }

        [Number(Name = "Invoice Amount", DocValues = true, IncludeInAll = false)]
        public virtual decimal InvoiceAmount { get; set; }

        [Number(Name = "Customer Id", IncludeInAll = false, DocValues = true)]
        public virtual int CustomerId { get; set; }

        [Number(Name = "Revenue Id", IncludeInAll = false, DocValues = true)]
        public virtual int RevenueId { get; set; }

        [Number(Name = "Currency Id", IncludeInAll = false, DocValues = true)]
        public virtual int CurrencyId { get; set; }

        [Number(Name = "Agency Id", IncludeInAll = false, DocValues = true)]
        public virtual int AgencyId { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Currency", DocValues = true)]
        public virtual string Currency { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Currency Code", DocValues = true)]
        public virtual string CurrencyCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Agency", DocValues = true)]
        public virtual string Agency { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Agency Code", DocValues = true)]
        public virtual string AgencyCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Customer", DocValues = true)]
        public virtual string Customer { get; set; }

        [String(Name = "Customer Name", Analyzer = "autocomplete")]
        public virtual string CustomerName { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Customer Number", DocValues = true)]
        public virtual string CustomerAIN { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Revenue", DocValues = true)]
        public virtual string Revenue { get; set; }

        [String(Analyzer = "autocomplete", Name = "Revenue_Analyzed")]
        public virtual string Revenue_Analyzed { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Revenue Code", DocValues = true)]
        public virtual string RevenueCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Location", DocValues = true)]
        public virtual string Location { get; set; }

        [String(Analyzer = "autocomplete", Name = "Location_Analyzed")]
        public virtual string Location_Analyzed { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "Location Code", DocValues = true)]
        public virtual string LocationCode { get; set; }

        [String(Index = FieldIndexOption.NotAnalyzed, Name = "State", DocValues = true)]
        public virtual string LocationState { get; set; }

        [Number(Name = "Location Id", IncludeInAll = false, DocValues = true)]
        public virtual int LocationId { get; set; }

        [Number(Name = "Deleted", DocValues = true)]
        public virtual bool Deleted { get; set; }

        [String(Ignore = true)]
        public virtual string AmountPaidText
        {
            get
            {

                string code = (CurrencyId == 566) ? char.ConvertFromUtf32(8358) :
                    (CurrencyId == 840) ? char.ConvertFromUtf32(36) : "";
                return string.Format("{0} {1}", code, Amount.ToString("#,###.#0"));
            }
        }

        [String(Ignore = true)]
        public virtual string InvoiceAmountText
        {
            get
            {

                string code = (CurrencyId == 566) ? char.ConvertFromUtf32(8358) :
                    (CurrencyId == 840) ? char.ConvertFromUtf32(36) : "";
                return string.Format("{0} {1}", code, InvoiceAmount.ToString("#,###.#0"));
            }
        }

    }
}
