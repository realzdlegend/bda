﻿/*----------------------------------------------------------
The base color for this template is #5c87b2. If you'd like
to use a different color start by replacing all instances of
#5c87b2 with your new color.
Default color:#5c87b2
Primary color: #9fbcd9
Secondary color:#1e4a74
Contrast Color:#a67228/#ffc97a
----------------------------------------------------------*/
body {background:#fff url('images/square_bg.png') repeat;font-size: .85em;font-family: "Trebuchet MS", Verdana, Helvetica, Sans-Serif;margin: 0;padding: 0;color: #444;}

#dialog_link, #tabs a:link, #accordion a:link{font-size:12px;text-decoration: none;}

p, ul {margin: 5px;line-height: 1.6em;}

.header,.footer,nav,section {display: block;}

/* HEADINGS   
----------------------------------------------------------*/

h1, h2, h3, h4, h5, h6 {font-size: 1.5em;color: #000;}
h1 {font-size: 2em;padding-bottom: 0;margin-bottom: 0;}
h2 {padding: 0 0 0px 0;border-bottom:1px solid #ffc97a}
h3 {font-size: 1.2em;}
h4 {font-size: 1.1em;}
h5, h6 {font-size: 1em;}

hr{border:0px;border-bottom:1px solid #ffc97a;background-color:transparent;color:#fff}

.dividerBg{background-color:#5c87b2;color:#fff;width:100%;padding:0px;margin:0px;min-height:24px;background:#5c87b2;
                                background-image: -ms-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -o-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -moz-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -webkit-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -webkit-gradient(linear,bottom left,left top,color-stop(0.03, #5c87b2),color-stop(0.97, #1e4a74));}


/* PRIMARY LAYOUT ELEMENTS   
----------------------------------------------------------*/

/* you can specify a greater or lesser percentage for the 
page width. Or, you can specify an exact pixel width. */
.page {width: 960px;margin-left: auto;margin-right: auto;margin-top:5px;}
.dividerBg .page{padding:0px}
.header, #header {position: relative;margin-bottom: 0px;color: #000;padding: 0;padding-left:15px;padding-right:15px}
.header h1, #header h1 {padding-left:15px;padding-right:15px;font-weight: bold;padding: 1px 0;margin: 0;color: #5c87b2;border: none;line-height: 2em;font-size: 32px !important;text-shadow: 1px 1px 2px #111;}
.main {min-height:360px;padding: 10px 30px 5px 30px;background-color: #fff;}
.main a{text-decoration:underline;color: #a67228;}
.main a:hover{text-decoration:none}
.main .maincontainer{width:700px;}
.main .equalcontainer{width:420px;padding:0px 10px}
.main .sidecontainer{width:200px;}
.footer, #footer {padding-left:15px;padding-right:15px;color: #444;padding: 10px 0;text-align: center;line-height: normal;margin: 0 0 5px 0;font-size: .9em;border-radius: 0 0 4px 4px;-webkit-border-radius: 0 0 4px 4px;-moz-border-radius: 0 0 4px 4px;border-top:2px solid #1e4a74}
.footoer p{line-height:16px}
.footer a{color: #a67228;}
.footer a:hover{color: #a67228;text-decoration:none}

/* TAB MENU   
----------------------------------------------------------*/
.menu{font-size:0.9em;font-family:Verdana;margin:0px;padding:0px 0px 0px 25px;margin-top:-18px}
.DynamicHoverStyle{color:#ff0;}
.DynamicMenuItemStyle{padding:0px 3px;background-color:#5c87b2;color:#fff;border-bottom:1px solid #ffc97a;min-width:150px}
.DynamicMenuStyle{background-color:#F7F6F3;color:#fff;}
.DynamicSelectedStyle{background-color:#9fcbf6;color:#000;}
.StaticHoverStyle{color:#000;background-color:#a67228;}
.StaticMenuItemStyle{padding:10px 5px;color:#fff;margin:0px -1px}
.StaticSelectedStyle{background-color:#ffc97a;color:#000;}
.LevelMenuItemStyles{font-weight:bold;}
.LevelSelectedStyles{}
.LevelSubMenuStyles{}

/* FORM LAYOUT ELEMENTS   
----------------------------------------------------------*/

fieldset {border: 1px solid #ffc97a;padding: 1.4em 1.4em 1.4em 1.4em;margin: 0 0 1.5em 0;}
legend {font-size: 1.2em;font-weight: bold;color:#000}
textarea {min-height: 75px;}
input[type="text"], input[type="password"], .textentry {border: 1px solid #ffc97a;padding: 2px;font-size: 1.1em;color: #444;width: 250px;margin-bottom:4px}
input[type="checkbox"]{opacity:0;padding:10px;}
input[type="checkbox"] + label{cursor:pointer; margin:0 10px 0 -20px;clear: none;background-image:url(images/uncheck.png);background-repeat:no-repeat;background-position:0px center;padding-left:20px}
input[type="checkbox"]:checked{opacity:0;width:0px}
input[type="checkbox"]:checked + label{color:#080;cursor:pointer; margin:0 10px 0 -20px;clear: none;background-image:url(images/check.png);background-repeat:no-repeat;background-position:0px center;padding-left:20px}
select {border: 1px solid #ffc97a;padding: 0px 0px 0px 2px;font-size: 1.1em;color: #444;width:inherit;}
input[type="submit"], .button {font-size: 1.0em;padding: 5px 15px;margin:2px 5px;color:#fff;float:right;-moz-border-radius: 3px; -webkit-border-radius: 3px; 
                               -khtml-border-radius: 3px; border-radius: 3px;cursor:pointer;border:1px;background:#5c87b2;
                                background-image: -ms-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -o-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -moz-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -webkit-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -webkit-gradient(linear,bottom left,left top,color-stop(0.03, #5c87b2),color-stop(0.97, #1e4a74));}

.display{color:#222;margin-right:10px;}
label{margin-right:10px;}
.displayEntry{color:#000;padding-left:15px}



/* TABLE for Search Form or any other forms
----------------------------------------------------------*/

.Form table {border-collapse:collapse;width:100%}
.Form table tr th {padding: 6px 5px;text-align: left;background-color: #e8eef4;}
.Form table thead td{background-color:#5c87b2;color:#fff;}
.Form table td {padding: 3px;}
.Form table td input[type="text"], .Form table td input[type="password"], 
.Form table td .textentry, .Form table td select {width:90%}

.Search table {width:100%;border:1px dotted #ffc97a;padding:5px}
.Search table td {padding:3px;min-width:33%}
.Search table td input[type="text"], .Search table td input[type="password"], 
.Search table td .textentry {width:95%}
.Search table td select {width:96.5%}


/* TABLE for Grid results
----------------------------------------------------------*/

.grid table {border: solid 1px #e8eef4;border-collapse: collapse;width:100%}
.grid table thead tr, .grid table tr th{background-color:#5c87b2;color:#fff;}
.grid table td {padding: 7px;border: solid 1px #e8eef4;}
.grid table th {padding: 6px 5px;text-align: left;border: solid 1px #e8eef4;}
.grid table tr:nth-child(even) td, tbody tr.even td {background:#e5ecf9;}

.Grid{border: solid 1px #e8eef4;border-collapse:collapse;width:100%}
.Grid td, .Grid th{padding: 7px;border: solid 1px #9fbcd9;}
.AlternatingRowStyle{background:#e5ecf9;padding: 7px;border: solid 1px #e8eef4;}
.EditRowStyle{}
.FooterStyle{}
.HeaderStyle{background-color:#5c87b2;color:#fff;padding: 6px 5px;text-align: left;border: solid 1px #9fbcd9;background:#5c87b2;
                                background-image: -ms-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -o-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -moz-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -webkit-linear-gradient(bottom , #5c87b2 3%, #1e4a74 97%);
                                background-image: -webkit-gradient(linear,bottom left,left top,color-stop(0.03, #5c87b2),color-stop(0.97, #1e4a74));}
.PagerStyle{background-color:#5c87b2;color:#fff;}
.PagerStyle td, .PagerStyle th{padding: 3px;border: solid 0px #e8eef4;width:5px;}
.PagerStyle a{color:#ff0}
.RowStyle{background:#fff;padding: 7px;border: solid 1px #e8eef4;}
.SelectedRowStyle{}
.SortedAscendingCellStyle{}
.SortedAscendingHeaderStyle{}
.SortedDescendingCellStyle{}
.SortedDescendingHeaderStyle{}

/* TreeView 
----------------------------------------------------------*/
.TreeView{}
.TreeView a{color:#5c87b2}
.HoverNodeStyle{text-decoration:underline; color:#6666AA;}
.NodeStyle{font-size:12px; color:#000;padding:5px 3px;}
.ParentNodeStyle{font-weight:bold;}
.SelectedNodeStyle{background-color:#B5B5B5;text-decoration:none}

/* MISC  
----------------------------------------------------------*/
.clear {clear: both;}
.clearh1 {clear: both;height:1px}
.clearh2 {clear: both;height:2px}
.clearh3 {clear: both;height:3px}
.clearh5 {clear: both;height:5px}
.clearh10 {clear: both;height:10px}
.clearh20 {clear: both;height:20px}
.clearh30 {clear: both;height:30px}
.clearh50 {clear: both;height:50px}
.error {color: Red;}
.left{float:left}
.right{float:right}
.hide{display:none}
.bold{font-weight:bold}

/* Site title  
----------------------------------------------------------*/
.title{display:block;float:left; text-align:left;min-width:237px;min-height:89px;background:url('images/bda-logo.png') no-repeat;}

/* Login Control  
----------------------------------------------------------*/
.loginDisplay{font-size: 1.1em;display: block;text-align: right;padding: 10px;color: #5c87b2;line-height:25px}
.loginDisplay a:link{color: #a67228;font-size:12px}
.loginDisplay a:visited{color: #a67228;}
.loginDisplay a:hover{color: #a67228;text-decoration:none}
.loginDisplay .user{background:url('images/me.png') no-repeat left center;padding-left:20px}
.loginDisplay .exit{background:url('images/exit.png') no-repeat left center;padding-left:20px}
.loginDisplay .home{background:url('images/home.png') no-repeat left center;padding-left:20px}

.login a:link{color: #a67228;}
.login a:visited{color: #a67228;}
.login a:hover{color: #a67228;text-decoration:underline}

/* SiteMapPath Control  
----------------------------------------------------------*/
.SiteMapPath{float:left;font-size:0.7em;font-style:italic}

/* PasswordRecovery Control  
----------------------------------------------------------*/
.PasswordRecovery{}


/* Dash board Login Control  
----------------------------------------------------------*/
.Dashboard{font-size: 1.1em;display: block;padding:3 10px;}
.Dashboard .unique{color: #000;}
.Dashboard a:link{color: #5c87b2;}
.Dashboard a:visited{color: #5c87b2;}
.Dashboard a:hover{color: #5c87b2;}

/* Styles for validation helpers
-----------------------------------------------------------*/
.field-validation-error {color: #ff0000;}
.field-validation-valid {display: none;}
.input-validation-error {border: 1px solid #ffc97a;background-color: #ffeeee;}
.validation-summary-errors {font-weight: bold;color: #ff0000;}
.validation-summary-valid {display: none;}
.information{background-color: #afa; color:#000;padding:3px;font-style:italic;font-size:11px}
.information2{background-color: #5c87b2; color:#fff;padding:3px;font-style:italic;font-size:11px}
.information1{color:#080;}
.defaultcolor{color:#5c87b2}

/*Repeater
----------------------------------------------------------*/
.repeater{}
.repeater h2{font-size:14px}
.repeater a{float:left;width:150px;text-align:center;border:1px solid #ffc97a;margin:2px 10px;color: #5c87b2}
.repeater img{width:60px}

/*Repeater
----------------------------------------------------------*/
.repeater1{}
.repeater1 h2{font-size:13px}
.repeater1 a{float:left;margin:2px 10px;color: #5c87b2;line-height:20px}
.repeater1 a:hover{text-decoration:underline}
.repeater1 img{width:20px}


/* Styles for editor and display helpers
----------------------------------------------------------*/
.display-label, .editor-label {margin: 1em 0 0 0;}
.display-field, .editor-field {margin: 0.5em 0 0 0;}
.text-box {width: 30em;}
.text-box.multi-line {height: 6.5em;}
.tri-state {width: 6em;}

/*Icons
----------------------------------------------------------*/
.email{color:#5c87b2;background:url('images/email.png') no-repeat left center;padding-left:20px}
.edit{color:#080;background:url('images/edit.png') no-repeat left center;padding-left:20px}
.dispute{color:#f00;background:url('images/dispute.png') no-repeat left center;padding-left:20px}
.view{color:#080;background:url('images/view.png') no-repeat left center;padding-left:20px}
.update{color:#080;background:url('images/update.png') no-repeat left center;padding-left:20px}
.recover{background:url('images/recover.png') no-repeat left center;padding-left:20px}
.help{background:url('images/help.png') no-repeat left center;padding-left:20px;}
.print{background:url('images/print.png') no-repeat left center;padding-left:20px;}
.pdf{background:url('images/pdf.png') no-repeat left center;padding-left:20px;}
.excel{background:url('images/excel.png') no-repeat left center;padding-left:20px;}
.back{background:url('images/back.png') no-repeat left center;padding-left:20px;}

.time{color:#f00;font-size:10px;clear:both;padding-left:5px}
.day{line-height:35px;padding-right:5px;color:#050;font-size:25px}
.month{color:#000;line-height:15px;font-weight:bold;}
.year{line-height:10px;font-size:10px;}

