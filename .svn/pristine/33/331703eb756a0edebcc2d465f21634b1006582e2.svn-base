﻿using System.Collections.Generic;
using BDA.Logic.interfaces;
using NHibernate;
using NHibernate.Criterion;
using BDA.Domain.Entity;
using BDA.Domain.Util;
using System;
using System.Linq;
using NHibernate.Transform;
using BDA.Domain;

namespace BDA.Logic.logic
{
    public class BillsRepository : BaseRepository<Bill, long>
    {
        AuditRepository audit = new AuditRepository();

        public BillsRepository(ISession _session)
            : base(_session)
        {

        }

        public BillsRepository() : base() { }

        public Bill GetBill(int id)
        {
            ICriteria criteria = _session.CreateCriteria<Bill>();
            criteria.Add(Restrictions.Eq("Id", id));
            return criteria.UniqueResult<Bill>();
        }

        public BillView GetBillView(long id)
        {
            return _session.Get<BillView>(id);
        }

        public Bill GetBillByExtIdandDatatSource(string extId, string datasource)
        {
            ICriteria criteria = _session.CreateCriteria<Bill>();
            criteria.Add(Restrictions.Eq("ExtBillNumber", extId));
            criteria.Add(Restrictions.Eq("DataSource", datasource));
            if (criteria.List<Bill>().Count > 0)
            {
                return (Bill)criteria.List<Bill>()[0];
            }
            else
            {
                return null;
            }
        }

        public IList<Currency> GetCurrencies()
        {
            var query = _session.QueryOver<Currency>();
            return query.List();
        }

        public IList<Bill> GetBill(string type)
        {
            ICriteria criteria = _session.CreateCriteria<Bill>();
            if (type.Equals("Closed"))
            {
                criteria.Add(Restrictions.Eq("Closed", true));               
            }
            else if (type.Equals("Disputed"))
            {
                criteria.Add(Restrictions.Eq("Disputed", true));
            }
            else if (type.Equals("All"))
            {
                 criteria.List<Bill>();
            }

            return (IList<Bill>)criteria.List <Bill>();
        }

        public int CreateBills(int systemId, Guid userKey, int currencyId, string revenueCode, string customerAIN, 
            string locationCode, IList<BillItem> items)
        {
            var claim = _session.Get<UserClaim>(userKey);
            if (claim == null) throw new ArgumentException("Invalid claim");

            Person person = _session.Get<Person>(claim.UserId);
            if (person == null) throw new ArgumentException("Invalid claim, user does not exist");

            Agency agency = _session.Get<Agency>(person.Organization.Id);
            if (agency == null) throw new ArgumentException("User does not have permission to create invoice");

            if (items == null || items.Count == 0) throw new ArgumentException("Bill must have one or more items");

            decimal amount = items.Sum(x => x.Amount);
            if (amount == 0) throw new ArgumentException("Bill amount cannot be zero");

            Currency currency = _session.Get<Currency>(currencyId);
            if (currency == null) throw new ArgumentException("Specified currency not supported");

            var queryR = _session.QueryOver<Revenue>();
            queryR.Where(r => r.AgencyId == agency.Id);
            queryR.Where(r => r.Code == revenueCode);
            Revenue revenue = queryR.SingleOrDefault();
            if (revenue == null) throw new ArgumentException("Specified Revenue does not exist");

            var queryC = _session.QueryOver<ServiceProvider>();
            queryC.Where(c => c.Code == customerAIN);
            ServiceProvider provider = queryC.SingleOrDefault();
            if (provider == null) throw new ArgumentException("Specified Customer does not exists");

            var queryL = _session.QueryOver<Location>();
            queryL.Where(l => l.AgencyId == agency.Id);
            queryL.Where(l => l.Code == locationCode);
            Location location = queryL.SingleOrDefault();
            if (location == null) throw new ArgumentException("Specified Location does not exist");

            if (location.AgencyId != revenue.AgencyId)
                throw new ArgumentException("Agency does not match Revenue or Location");

            ExpertSystem system = _session.Get<ExpertSystem>(systemId);
            if (system == null) throw new ArgumentException("Specified system not configured");

            if (items != null && items.Count > 0 && amount > 0)
            {

                try
                {
                    _session.BeginTransaction();

                    List<long> billIds = new List<long>();
                    int lineNumber = 0;

                    foreach (BillItem bi in items)
                    {
                        ISQLQuery query1 = _session.CreateSQLQuery("exec InsertMain ?, ?, ?, ?, ?");
                        query1.SetInt32(0, systemId);
                        query1.SetString(1, Guid.NewGuid().ToString());
                        query1.SetInt32(2, provider.Id);
                        query1.SetInt32(3, revenue.Id);
                        query1.SetDateTime(4, AppSettings.LocalTime);

                        long mainId = long.Parse(query1.UniqueResult<decimal>().ToString());

                        ISQLQuery query2 = _session.CreateSQLQuery("exec edb_InsertBill ?, ?, ?, ?, ?, ?, ?, ?, ?");
                        query2.SetInt64(0, mainId);
                        query2.SetDecimal(1, bi.Amount);
                        query2.SetDateTime(2, AppSettings.LocalTime);
                        query2.SetString(3, string.Format("Bill for {0}", bi.Description));
                        query2.SetDateTime(4, AppSettings.LocalTime);
                        query2.SetInt32(5, currencyId);
                        query2.SetInt32(6, location.Id);
                        query2.SetInt32(7, (int)BillStatus.Pending);
                        query2.SetInt32(8, bi.Quantity);

                        query2.ExecuteUpdate();

                        billIds.Add(mainId);

                        ISQLQuery query3 = _session.CreateSQLQuery("exec edb_InsertBillDetail ?, ?, ?, ?,?");
                        query3.SetDecimal(0, bi.Amount);
                        query3.SetInt64(1, mainId);
                        query3.SetString(2, bi.Description);
                        query3.SetInt32(3, ++lineNumber);
                        query3.SetInt32(4, bi.Quantity);
                        query3.ExecuteUpdate();
                    }
                    _session.Transaction.Commit();

                    try
                    {
                        string detail = string.Format("Created {0} Bills for revenue {1} and Customer {2}.,"
                            + " Total Amount: {3}, Location: {4}", currency.Code, revenue.Name, provider.Name,
                            string.Format("{0}{1}", currency.Code, amount.ToString("#,###.#0")), location.Code);

                        audit.SaveAuditTrail(Actions.Invoice, detail);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log(ex);
                    }

                    return billIds.Count;
                }
                catch (Exception ex)
                {
                    _session.Transaction.Rollback();
                    ErrorLogger.Log(ex);
                    throw ex;
                }
            }
            else
                throw new ArgumentException("Invoice must have one or more items and invoice amount cannot be zero");
        }


        public IList<BillView> SearchBill(int status, int dateType, DateTime startDate, DateTime endDate, int providerId, 
            int revenueId, int locationId, int agencyId, Guid? personId, int startIndex, int pageSize)
        {
            var query = _session.QueryOver<BillView>();

            Person person = personId.HasValue ? _session.Get<Person>(personId.Value) : null;
            if (person != null)
            {
                if (person.RevenueAccess == null || person.RevenueAccess.Count == 0)
                    return new List<BillView>();

                List<int> rIds = new List<int>();
                foreach (Revenue r in person.RevenueAccess)
                {
                    rIds.Add(r.Id);
                }
                query.Where(Restrictions.In(Projections.Property<BillView>(x => x.RevenueId), rIds));
            }

            if(status != 0)
                query.Where(b => b.Status == status);
            if(providerId > -1)
                query.Where(b => b.ServiceProviderId == providerId);
            if(revenueId > -1)
                query.Where(b => b.RevenueId == revenueId);
            if (locationId > -1)
                query.Where(b => b.LocationId == locationId);
            else if (person != null && person.Organization.Id == agencyId)
            {
                List<int> locations = new LocationRepository().GetUserLocationIds(person.Id);
                int[] locationIds = locations.ToArray();
                query.Where(Restrictions.In(Projections.Property<BillView>(b=>b.LocationId), locationIds));
            }
            if (agencyId > 0)
                query.Where(b => b.AgencyId == agencyId);

            startDate = startDate.Date; 
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddMinutes(59);

            if (dateType == (int)DateType.TransactionDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.TransactionDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.EntryDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.EntryDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.LastEditDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.LastEditDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.ClosingBillDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.ClosingBillDate), startDate, endDate));
            }

            query.Skip(startIndex).Take(pageSize);

            query.OrderBy(x => x.EntryDate).Desc();
            return  query.List<BillView>();
        }

        public int SearchBillCount(int status, int dateType, DateTime startDate, DateTime endDate, int providerId,
            int revenueId, int locationId, int agencyId, Guid? personId)
        {
            var query = _session.QueryOver<BillView>();


            Person person = personId.HasValue ? _session.Get<Person>(personId.Value) : null;
            if (person != null)
            {
                if (person.RevenueAccess == null || person.RevenueAccess.Count == 0)
                    return 0;

                List<int> rIds = new List<int>();
                foreach (Revenue r in person.RevenueAccess)
                {
                    rIds.Add(r.Id);
                }
                query.Where(Restrictions.In(Projections.Property<BillView>(x => x.RevenueId), rIds));
            }

            if (status != 0)
                query.Where(b => b.Status == status);
            if (providerId > -1)
                query.Where(b => b.ServiceProviderId == providerId);
            if (revenueId > -1)
                query.Where(b => b.RevenueId == revenueId);
            if (locationId > -1)
                query.Where(b => b.LocationId == locationId);
            else if (person != null && person.Organization.Id == agencyId)
            {
                List<int> locations = new LocationRepository().GetUserLocationIds(person.Id);
                int[] locationIds = locations.ToArray();
                query.Where(Restrictions.In(Projections.Property<BillView>(b => b.LocationId), locationIds));
            }
            if (agencyId > 0)
                query.Where(b => b.AgencyId == agencyId);

            startDate = startDate.Date;
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);


            if (dateType == (int)DateType.TransactionDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.TransactionDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.EntryDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.EntryDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.LastEditDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.LastEditDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.ClosingBillDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.ClosingBillDate), startDate, endDate));
            }
            return query.Select(Projections.RowCount()).SingleOrDefault<int>();
        }

        public IList<BillCount> SearchBillCounts(int status, int dateType, DateTime startDate, DateTime endDate, int providerId,
            int revenueId, int locationId, int agencyId, Guid? personId)
        {
            var query = _session.QueryOver<BillView>();

            Person person = personId.HasValue ? _session.Get<Person>(personId.Value) : null;
            if (person != null)
            {
                if (person.RevenueAccess == null || person.RevenueAccess.Count == 0)
                    return new List<BillCount>();

                List<int> rIds = new List<int>();
                foreach (Revenue r in person.RevenueAccess)
                {
                    rIds.Add(r.Id);
                }
                query.Where(Restrictions.In(Projections.Property<BillView>(x => x.RevenueId), rIds));
            }

            if (providerId > -1)
                query.Where(b => b.ServiceProviderId == providerId);
            if (revenueId > -1)
                query.Where(b => b.RevenueId == revenueId);
            if (locationId > -1)
                query.Where(b => b.LocationId == locationId);
            else if (person != null && person.Organization.Id == agencyId)
            {
                List<int> locations = new LocationRepository().GetUserLocationIds(person.Id);
                int[] locationIds = locations.ToArray();
                query.Where(Restrictions.In(Projections.Property<BillView>(b => b.LocationId), locationIds));
            }
            if (agencyId > 0)
                query.Where(b => b.AgencyId == agencyId);

            startDate = startDate.Date;
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);


            if (dateType == (int)DateType.TransactionDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.TransactionDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.EntryDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.EntryDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.LastEditDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.LastEditDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.ClosingBillDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.ClosingBillDate), startDate, endDate));
            }

            ProjectionList plist = Projections.ProjectionList();
            plist.Add(Projections.Group<BillView>(x => x.Status), "Status");
            plist.Add(Projections.Count<BillView>(x => x.Id), "Count");

            query.Select(plist).TransformUsing(Transformers.AliasToBean<BillCount>());

            return query.List<BillCount>();
        }

        public IList<BillTotal> SearchBillTotals(int status, int dateType, DateTime startDate, DateTime endDate, int providerId,
            int revenueId, int locationId, int agencyId, Guid? personId)
        {
            var query = _session.QueryOver<BillView>();

            Person person = personId.HasValue ? _session.Get<Person>(personId.Value) : null;
            if (person != null)
            {
                if (person.RevenueAccess == null || person.RevenueAccess.Count == 0)
                    return new List<BillTotal>();

                List<int> rIds = new List<int>();
                foreach (Revenue r in person.RevenueAccess)
                {
                    rIds.Add(r.Id);
                }
                query.Where(Restrictions.In(Projections.Property<BillView>(x => x.RevenueId), rIds));
            }

            if (status != 0)
                query.Where(b => b.Status == status);
            if (providerId > -1)
                query.Where(b => b.ServiceProviderId == providerId);
            if (revenueId > -1)
                query.Where(b => b.RevenueId == revenueId);
            if (locationId > -1)
                query.Where(b => b.LocationId == locationId);
            else if (person != null && person.Organization.Id == agencyId)
            {
                List<int> locations = new LocationRepository().GetUserLocationIds(person.Id);
                int[] locationIds = locations.ToArray();
                query.Where(Restrictions.In(Projections.Property<BillView>(b => b.LocationId), locationIds));
            }
            if (agencyId > 0)
                query.Where(b => b.AgencyId == agencyId);

            startDate = startDate.Date;
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);


            if (dateType == (int)DateType.TransactionDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.TransactionDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.EntryDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.EntryDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.LastEditDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.LastEditDate), startDate, endDate));
            }
            else if (dateType == (int)DateType.ClosingBillDate)
            {
                query.Where(Restrictions.Between(Projections.Property<BillView>(x => x.ClosingBillDate), startDate, endDate));
            }

            ProjectionList plist = Projections.ProjectionList();
            plist.Add(Projections.Group<BillView>(x => x.CurrencyCode), "CurrencyId");
            plist.Add(Projections.Count<BillView>(x => x.Id), "Count");
            plist.Add(Projections.Sum<BillView>(x => x.Amount), "Amount");

            query.Select(plist).TransformUsing(Transformers.AliasToBean<BillTotal>());

            return query.List<BillTotal>();
        }

        public IList<Bill> GetBillByRevType(String Revenue, Organization ServiceProvider, string dateType, DateTime startDate, DateTime endDate,int status, int startIndex, int maxSize)
        {
            int endrow = startIndex + maxSize;

            int Id = Convert.ToInt32(Revenue);
            
            ICriteria criteria = _session.CreateCriteria<Bill>("bill");
            criteria.CreateAlias("list.Revenue", "rev");
            criteria.CreateAlias("MasterList", "list");

            criteria.Add(Restrictions.Eq("rev.Id", Id));

            criteria.Add(Restrictions.Eq("list.Organization", ServiceProvider));
            if (dateType.Equals("Bill Date"))
            {
                criteria.Add(Restrictions.Between("list.EntryDate", startDate, endDate));
            }
            else if (dateType.Equals("Transaction Date"))
            {
                criteria.Add(Restrictions.Between("bill.TransactionDate", startDate, endDate));
            }

            if (!(status==0))
                criteria.Add(Restrictions.Eq("Status", status));

            if (maxSize > 0)
                criteria.SetFirstResult(startIndex).SetMaxResults(endrow);
            return criteria.List<Bill>();            
        }


        public int GetBillByRevTypeCount(String Revenue, Organization ServiceProvider, string dateType, DateTime startDate, DateTime endDate, int status)
        {
           

            int Id = Convert.ToInt32(Revenue);

            ICriteria criteria = _session.CreateCriteria<Bill>("bill");
            criteria.CreateAlias("list.Revenue", "rev");
            criteria.CreateAlias("MasterList", "list");
            criteria.Add(Restrictions.Eq("rev.Id", Id));

            criteria.Add(Restrictions.Eq("list.Organization", ServiceProvider));
            if (dateType.Equals("Bill Date"))
            {
                criteria.Add(Restrictions.Between("list.EntryDate", startDate, endDate));
            }
            else if (dateType.Equals("Transaction Date"))
            {
                criteria.Add(Restrictions.Between("bill.TransactionDate", startDate, endDate));
            }

            if (!(status==0))
                criteria.Add(Restrictions.Eq("Status", status));
            
            return criteria.List<Bill>().Count;
        }
    }

    public class BillCount
    {
        public int Status { get; set; }
        public int Count { get; set; }
    }

    public class BillTotal
    {
        public int CurrencyId { get; set; }
        public int Count { get; set; }
        public decimal Amount { get; set; }

        public string Currency
        {
            get
            {
                return new InvoiceRepository().GetCurrency(CurrencyId).Name;
            }
        }
    }


}
