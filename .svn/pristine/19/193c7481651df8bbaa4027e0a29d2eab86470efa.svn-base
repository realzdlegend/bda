﻿using System.Threading;
using System.Threading.Tasks;
using BDA.Domain.Util;
using BDA.Logic.Helpers;
using BDA.Logic.logic;
using BDA.Search;
using BDA.Search.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BDA.Logic.Mailer;
using StackifyLib;
using BDA.Domain.Models;
using BDA.Domain.Entity;

namespace BDA.Services.Controllers
{
    public class OpenController : ApiController
    {
        readonly PaymentRepository payRepo = new PaymentRepository();
        readonly InvoiceRepository invRepo = new InvoiceRepository();
        readonly ReportClient sClient = new ReportClient();
        readonly SearchClient searchClient = new SearchClient();
        readonly LocationRepository locRepo = new LocationRepository();
        readonly NotificationRepository notifyRepo = new NotificationRepository();
        readonly PredictionRepository predictRepo = new PredictionRepository();
        readonly MobileRepository mobileRepo = new MobileRepository();
        readonly OrganizationRepository orgRepo = new OrganizationRepository();
        readonly RevenueRepository revenueRepo = new RevenueRepository();



        [HttpGet, HttpPost]
        public HttpResponseMessage SyncFidesic()
        {
            try
            {
                List<string> count = payRepo.CheckPaymentWithoutCount();


                return Request.CreateResponse<List<string>>(HttpStatusCode.OK, count);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        //[HttpGet, HttpPost]
        //public HttpResponseMessage test()
        //{
        //    sClient.Sample("");
        //    return Request.CreateResponse<string>(HttpStatusCode.OK, "Notification Successful");
        //}

        [HttpPost]
        public HttpResponseMessage IndexSplit(SplitInfo splitInfo)
        {
            try
            {
                //bool result = payRepo.ProcessSplit(splitInfo);
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }



        [HttpPost]
        public async Task<HttpResponseMessage> RemitaNotify(List<RemitaPayment> model)
        {

            if (!model.Any())
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid model");
          
            var saveToDB = await invRepo.SavePaymentProcess(model); 
            try
            { 
                foreach (RemitaPayment invoice in model)
                {
                    try
                    {
                        Invoice inv;
                        if (!string.IsNullOrEmpty(invoice.orderRef))
                        {
                            inv = invRepo.GetInvoice(invoice.orderRef);
                        }
                        else     
                        {
                            inv = invRepo.GetInvoiceByRRR(invoice.rrr);
                        }
                        TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
                        int secondsSinceEpoch = (int)t.TotalSeconds;
                        var pay = new OfflinePaymentRequestModel()
                        {
                            AgencyGatewayId = inv.AgencyId,
                            AmountPaid = Convert.ToDecimal(double.Parse(invoice.amount)),
                            Description = "Remita Invoice Marked As Paid",
                            EntryDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                            InvoiceAmount = inv.Amount,
                            InvoiceNumber = inv.InvoiceNumber,
                            PaymentDate = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                            PaymentReference = inv.InvoiceNumber+ "|"+ secondsSinceEpoch.ToString(),
                            SpriteReference = ""
                        };
                        var spriteRepo = new BDA.Client.logic.SpriteAPIRepository();
                        var resp = spriteRepo.OfflinePayment(pay);
                        if (!string.IsNullOrEmpty(resp.OfflinePaymentReference)) {
                            await invRepo.UpdatePaymentProcess(inv.InvoiceNumber);
                        }

                    }
                    catch (Exception ex)
                    {
                        continue;
                    }

                } 
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }


        [HttpGet, HttpPost]
        public HttpResponseMessage Notify(string userkey = null)
        {
            if (string.IsNullOrEmpty(userkey))
            {
                try
                {
                    payRepo.Notify();
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Notification Successful");
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                try
                {
                    payRepo.Notify(userkey);
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Notification Successful");
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }

        [HttpGet]
        public string Notify(int notype, string senderId)
        {
            try
            {
                NotificationType noType = (NotificationType)Enum.Parse(typeof(NotificationType), notype.ToString());
                Guid userKey = Guid.Parse(senderId);
                new MobileRepository().Notify(noType, userKey);
                return "this is fine";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public HttpResponseMessage SummarySearch(int agencyId = -1, string location = "", string agency = "", string currency = "",
            string customer = "", string revenue = "", bool? voided = null, string status = "", string text = "", int pageSize = 10, int page = 0)
        {
            try
            {
                var result = sClient.SummarySearch(agencyId, location, agency, currency, customer, revenue, voided, status, text, pageSize, page);
                return Request.CreateResponse<ResultData>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage AutoComplete(int type, string text, int pageSize = 10, int page = 0)
        {
            try
            {
                var result = sClient.AutoComplete(1, text, pageSize, page);
                return Request.CreateResponse<List<ResultItem>>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage LoadAutoComplete(int type)
        {
            try
            {
                var result = sClient.LoadAutoComplete(type);
                return Request.CreateResponse<List<string>>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                var list = new List<string>();
                list.Add(ex.Message);
                return Request.CreateResponse<List<string>>(HttpStatusCode.OK, list);
            }
        }

        [HttpGet]
        public HttpResponseMessage LoadInvoiceNumber()
        {
            try
            {
                var result = sClient.LoadInvoiceNumber();
                return Request.CreateResponse<List<string>>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage Dashboard(DateTime? date = null)
        {
            try
            {
                DateTime actDate = date.HasValue ? date.Value : DateTime.Now;
                var result = sClient.DailyDashboard(actDate,-1);
                return Request.CreateResponse<DashboardData>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, ex.Message);
            }
        }


        [HttpGet, HttpPost]
        public HttpResponseMessage DailyDashboard(DateTime? date = null, DateTime? eDate = null, int AgencyId = -1)
        {
            try
            {
                DateTime actDate = date.HasValue ? date.Value : DateTime.Now;
                var result = sClient.DailyDashboard(actDate, AgencyId, eDate);
                return Request.CreateResponse<DashboardData>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, ex.Message);
            }
        }



        [HttpGet, HttpPost]
        public HttpResponseMessage TotalDashboard(DateTime? date = null, int AgencyId = -1)
        {
            try
            {
                DateTime actDate = date.HasValue ? date.Value : DateTime.Now;
                var result =  sClient.TotalDashboard(actDate, AgencyId);
                return Request.CreateResponse<DashboardData>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, ex.Message);
            }
        }

        [HttpGet, HttpPost]
        public HttpResponseMessage AdminDashboard(int type = 0, int agencyId = -1,
            DateTime? startdate = null, DateTime? enddate = null, string orderby = "", int topN = 5, int aggSize = 10)
        {
            try
            {
                //var result = sClient.AdminDashboard(type, agencyId, startdate, enddate, orderby, topN, aggSize);

                var result = sClient.AdminDashboard(type, agencyId, startdate, enddate, orderby, topN, aggSize);
                return Request.CreateResponse<DashboardData2>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        public string NotifyAll(int notype)
        {
            try
            {
                NotificationType noType = (NotificationType)Enum.Parse(typeof(NotificationType), notype.ToString());
                new MobileRepository().NotifyAll(noType);
                return "this is fine";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public string BulkInvoiceIndex()
        {
            try
            {
                invRepo.IndexBulkInvoice();
                return "done";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public string BulkPaymentIndex()
        {
            try
            {
                payRepo.IndexBulkPayment();
                return "done";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return ex.Message;
            }
        }

        [HttpGet]
        public VersionInvoice GetInvoice(int Id)
        {
            try
            {
                var i = searchClient.GetInvoice(Id);
                return i;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return null;
            }
        }

        [HttpPost]
        public HttpResponseMessage IndexInvoices(List<long> invoiceIds)
        {
            try
            {               
                List<string> result = invRepo.IndexBulkInvoiceV2(invoiceIds);
                return Request.CreateResponse<List<string>>(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public  HttpResponseMessage IndexInvoice(long Id)
        {
            try
            { 
                invRepo.IndexInvoiceV2(Id);        
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage IndexInvoiceNumber(string Id)
        {
            try
            {
                var inv = invRepo.GetInvoice(Id);
                invRepo.IndexInvoiceV2(inv.Id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }


        [HttpPost]
        public HttpResponseMessage IndexPayments(List<int> remmitanceIds)
        {
            
            try
            {
                if (!remmitanceIds.Any())
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Expected parameter cannot be null");
                }

                List<string> result = payRepo.IndexBulkPaymentV2(remmitanceIds);
                return Request.CreateResponse<List<string>>(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            { 
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, AppConstants.ServerError);
                
            }
        }

        [HttpPost] //changed
        public HttpResponseMessage IndexPayment(int Id)
        {

            try
            { 
                var result = payRepo.IndexPayment(Id);
                if (result == false)
                {
                    throw new Exception(string.Format("Failed to index payment with id {0}", Id));
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        


        [HttpPost]
        public HttpResponseMessage DeleteInvoices(List<long> invoiceIds)
        {
            try
            {
                if (!invoiceIds.Any())
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Expected parameter cannot be null");
                }
                List<string> result = invRepo.BulkInvoiceDelete(invoiceIds);
                return Request.CreateResponse<List<string>>(HttpStatusCode.Created, result);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

        [HttpPost]
        public HttpResponseMessage DeleteInvoice(long Id)
        {
            try
            { 
                invRepo.DeleteInvoice(Id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch //(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }
        

       [HttpGet]
        public HttpResponseMessage VerifySync()
        {
            
            try
            {
                long esInovices, sqlInvoices, esPayments, sqlPayments;
                string response;
                string syncStatus = string.Empty;
                string location = Path.Combine(HttpRuntime.AppDomainAppPath, "MailTemplate/dataSyncEmail.html");
                string template = File.ReadAllText(@location);
                string title = "BDA-ELASTICSEARCH SYNC STATUS";
                string tag = "Sync Status";
                string[] toEmail = null;
                
                sqlInvoices = invRepo.InvoiceCount();
                sqlPayments = payRepo.PaymentCount();
                esInovices = searchClient.InvoiceCount();          
                esPayments = searchClient.PaymentsCount();
               
                if (sqlInvoices == esInovices && sqlPayments == esPayments)
                {
                    syncStatus = "OK!";
                }
                else
                {
                    syncStatus = "NOT OK!";
                }                              
                
                response = string.Format("Sync Status: {0} --- RESULTS >> SQL Invoices: {1}, ES Invoices: {2}, SQL Payments: {3}, ES Payments: {4}",
                     syncStatus,sqlInvoices,esInovices, sqlPayments, esPayments);
                try
                {
                    toEmail = ConfigurationManager.AppSettings["DataSync_Notify"].Split(',');
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response + "  DataSync_Notify Email settings missing from web config");
                }
                template = template.Replace("#Response#", response);
                var _mailservice = new MailService();
                foreach (var email in toEmail)
                {
                    _mailservice.Send(email, title, template, tag, null);
                }
               

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
          }
        }

        [HttpGet]
        public HttpResponseMessage InvoiceNotification(string invoiceNumber)
        {

            try
            {
             
                var contextWrapper = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                XmlXslTransform.ContextWrapper = contextWrapper;

                var invoice = invRepo.GetInvoice(invoiceNumber.Trim());

                if (invoice == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid invoice");
                }
                var customerInvoice = invRepo.GetCustomerInvoice(invoice.Id);
                var agency = orgRepo.GetAgency(invoice.AgencyId);
                var revenue = revenueRepo.GetById(invoice.RevenueId);
                var currency = invRepo.GetCurrency(invoice.CurrencyId);
                // send sms
                if (agency.Code.ToLower() != "oau" && agency.Code.ToLower() != "abu" &&
                    agency.Code.ToLower() != "swp" && agency.Code.ToLower() != "trp")
                {
                    SmsSender.InvoiceCreationSms(customerInvoice.Phone, agency.Code, currency.Name, revenue.Name,
                               invoice.Amount, invoice.InvoiceNumber);
                }

                //send email
                notifyRepo.NewInvoice(invoice);
                //save prediction
                predictRepo.Predict(invoice, customerInvoice.CreatedBy);
                //notfify bda mobile
                mobileRepo.Notify(NotificationType.NewInvoice, new List<int>() { invoice.AgencyId, invoice.ProviderId });            
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }


        [HttpGet]
        public HttpResponseMessage Test(string invoiceNumber)
        {
            try
            {
                var invoice = invRepo.GetInvoiceView(invoiceNumber.Trim());           
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);              
            }
        }





        [HttpGet]
        public HttpResponseMessage SyncInvoices()
        {
            try
            {

                var esInvoiceIds = searchClient.GetInvoiceIds().ToList();
                var sqlInvoiceIds = invRepo.GetInvoiceIds().ToList();
                IEnumerable<long> diffList = sqlInvoiceIds.Except(esInvoiceIds);

                var esPayments = searchClient.GetPaymentIds().ToList();
                var sqlPayments = payRepo.GetPaymentIds().ToList();
                var payList = sqlPayments.Except(esPayments);

                if (diffList.Any())
                {
                    foreach (var item in diffList)
                    {
                        try
                        {
                            Logger.Queue("Info", "About to index: ", item);
                            invRepo.IndexInvoiceV2(item);
                            Logger.Queue("Info", "Done");
                        }
                        catch (Exception ex)
                        {
                            ErrorLogger.Log(ex);

                        }

                    }
                }

                if (payList.Any())
                {
                    foreach (var item in payList)
                    {
                        try
                        {
                            Logger.Queue("Info", "About to index payment: ", item);
                            payRepo.IndexPayment(item);
                            Logger.Queue("Info", "Done");
                        }
                        catch (Exception ex)
                        {
                            ErrorLogger.Log(ex);

                        }

                    }
                }
                
                return Request.CreateResponse(HttpStatusCode.OK, 
                    string.Format("Indexed {0} invoices ,  {1}, payments",diffList.Count(), payList.Count()));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, AppConstants.ServerError);
            }
        }

    }
}
