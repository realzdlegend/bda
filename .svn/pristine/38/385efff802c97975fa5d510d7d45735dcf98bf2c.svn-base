﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BDA.Domain.Util;
using BDA.Logic.logic;
using BDA.Domain.Entity;

using BDA.Search;
using BDA.Search.Data;
using Nest;
using System.Data;
using BDA.Logic.Helpers;
using BDA.Domain;

public partial class Common_Invoices : PageBase
{
    RevenueRepository revRep = new RevenueRepository();
    OrganizationRepository orgRep = new OrganizationRepository();
    LocationRepository locRep = new LocationRepository();
    readonly InvoiceRepository invoiceLogicRep = new InvoiceRepository();
    readonly InvoiceRepository invoiceRepDirect = new InvoiceRepository();
    readonly BDA.Client.logic.InvoiceRepository invoiceRep = new BDA.Client.logic.InvoiceRepository();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
             
            txtStartDate.Text = txtEndDate.Text = DateTime.Now.ToString("dd MMM, yyyy");

            int agencyId = (CurrentRole.RoleName == "Agency") ? CurrentUser.Organization.Id : -1;
            if (CurrentRole.RoleName == "Agency")
            {
                ddAgency.Enabled = false;
                ddAgency.SelectedValue = CurrentUser.Organization.Id.ToString();
            } 

            ddLocation.DataSource = locRep.GetLocations(agencyId);
            ddLocation.DataBind();
            ddLocation.Items.Insert(0, new ListItem("All Locations", "-1")); 


            ddlStates.DataSource = locRep.GetAgencyStates(agencyId);
            ddlStates.DataBind();
            ddlStates.Items.Insert(0, new ListItem("All States", "-1"));


			drpCurrency.DataSource = invoiceLogicRep.GetAllCurrency();
			drpCurrency.DataBind();
			drpCurrency.Items.Insert(0, new ListItem("All", "-1"));

            if (CurrentRole.RoleName == "Service Provider")
            {
                ServiceProvider sp = (ServiceProvider)orgRep.GetById(CurrentUser.Organization.Id);
                ddlRevenue.DataSource = revRep.GetRevenuesForServiceProvider(CurrentUser.Organization.Id);
                ddlRevenue.DataBind();
                ddlRevenue.Items.Insert(0, new ListItem("All Revenues", "-1")); 

                ddProviders.Items.Add(new ListItem(CurrentUser.Organization.Name, CurrentUser.Organization.Id.ToString()));
                ddProviders.Enabled = false; 
            }
            else
            {
                ddlRevenue.DataSource = revRep.GetActiveRevenues(agencyId, -1);
                ddlRevenue.DataBind();
                ddlRevenue.Items.Insert(0, new ListItem("All Revenues", "-1"));

                ddProviders.DataSource = orgRep.GetServiceProviders(agencyId, 0, -1);
                ddProviders.DataBind();
                ddProviders.Items.Insert(0, new ListItem("All Customers", "-1"));
               
            }
             
        }
        else
        {
            btnPrintInvoice.Visible = false;
        }
    }

    protected void dsInvoices_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        IOrderedDictionary inputParameters = e.InputParameters;
        var result = formatFormFields(ref inputParameters);
        lblStatus.ForeColor = System.Drawing.Color.Red;
        lblStatus.Text = result.Item1;
        e.Cancel = result.Item2;
    }


    /**
     * Validate user submitted form values stored in members
     * 
     * @param[in] dstContainer bucket to hold validated values
     * 
     * @return Tuple<string, bool> Diagnostics error messages and event propagation status
     */
    private Tuple<string, bool> formatFormFields(ref IOrderedDictionary dstContainer)
    {
        bool isAdmin = true;

        dstContainer["agencyId"] = int.Parse(ddAgency.SelectedValue);
        dstContainer["providerId"] = int.Parse(ddProviders.SelectedValue);
        dstContainer["status"] = int.Parse(ddStatus.SelectedValue);
        dstContainer["currencyId"] = int.Parse(drpCurrency.SelectedValue);
        dstContainer["invoiceNumber"] = txtInvoice.Text;
        dstContainer["name"] = txtName.Text;
        dstContainer["dateType"] = int.Parse(ddlDateType.SelectedValue);
        dstContainer["state"] = ddlStates.SelectedItem.Text.Contains("All States") ? string.Empty : ddlStates.SelectedItem.Text;

        StringBuilder errorMsg = new StringBuilder();
        bool hasError = false;
        try
        {
            dstContainer["startDate"] = DateHandler.getDateTime(txtStartDate.Text);
            dstContainer["endDate"] = DateHandler.getDateTime(txtEndDate.Text);
        }
        catch
        {
            errorMsg.AppendLine("Date not in correct format");
            hasError = true;
        }

        int locationId = -1;
        int revenueId = -1;

        try
        {
            revenueId = int.Parse(ddlRevenue.SelectedValue);
        }
        catch (FormatException)
        {
            errorMsg.AppendLine("Invalid revenue selection");
            hasError = true;
        }

        try
        {
            locationId = int.Parse(ddLocation.SelectedValue);
        }
        catch (FormatException)
        {
            errorMsg.AppendLine("Invalid location selection");
            hasError = true;
        }

        if (CurrentRole.RoleName == "Agency")
        {
            isAdmin = false;
            Guid userGuid = CurrentUser.Id;
            dstContainer["agencyId"] = CurrentUser.Organization.Id;
            dstContainer["revenueIds"] = (object)formatIds(revenueId, userGuid, new RevenueRepository().getUserRevenueIds);
            dstContainer["locationIds"] = (object)formatIds(locationId, userGuid, new LocationRepository().GetUserLocationIds);
        }
        else
        {
            if (revenueId != -1)
            {
                dstContainer["revenueIds"] = (object)new List<int>() { revenueId };
            }
            if (locationId != -1)
            {
                dstContainer["locationIds"] = (object)new List<int>() { locationId };
            }
        }

        dstContainer["isAdmin"] = isAdmin;
        return new Tuple<string, bool>(errorMsg.ToString(), hasError);
    }



    /**
     * Normalizes locationIds by converting selected value to a list of ids and if 
     * "ALL(-1)" was specified load the list of locationids for the currently logged in
     * user 
     * 
     * @param[in] locationSelection User selected locationid 
     * @param[in] userGuid Logged in user
     * 
     * @return Ilist<int> list of location Ids
     */
    private IList<int> formatIds(int userSelection, Guid userGuid, Func<Guid, IList<int>> idRetriever)
    {
        IList<int> ids = new List<int>();
        if (userGuid == null)
        {
            return ids;
        }

        try
        {
            if (userSelection == -1)
            {
                //load all selection from database
                ids = idRetriever((Guid)userGuid);
            }
            else
            {
                ids.Add(userSelection);
            }
        }
        catch (FormatException)
        {
            lblStatus.Text = "Invalid selection";
        }

        return ids;
    }

    //protected void lkStatus_Click(object sender, EventArgs e)
    //{
    //    lkFidesic.CssClass = "";
    //    lkIATA.CssClass = "";

    //    LinkButton current = (LinkButton)sender;
    //    hdStatus.Value = current.CommandArgument;
    //    current.CssClass = "active";
    //    gvInvoices.DataBind();
    //    gvsummary.DataBind();
    //}

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            gvInvoices.PageIndex = 0;
            gvInvoices.DataBind();
            gvsummary.DataBind();
            //btnPrintInvoice.Visible = true;
           
        }
        catch (FormatException)
        {
            lblStatus.Text = "Date format or other format incorrect";
        }

       
    }

    protected void grdView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //int index = int.Parse(e.CommandArgument.ToString());
        if (e.CommandName == "view")
        {
            //string invoiceId = gvInvoices.DataKeys[index]["Id"].ToString();

            //string invoiceId = gvInvoices.SelectedValue.ToString();
            Response.Redirect(URLEncryption.EncryptURL("InvoiceDetails.aspx", "invoiceId", (string)e.CommandArgument));
        }
    }

    protected void dsLocations_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["agencyId"] = (CurrentRole.RoleName == "Agency") ? CurrentUser.Organization.Id : int.Parse(ddAgency.SelectedValue);
        e.InputParameters["isRegion"] = null;
        e.InputParameters["stateId"] = -1;
        e.InputParameters["regionId"] = -1;
    }

    protected void btnPrintInvoice_Click(object sender, EventArgs e)
    {
        try
        {
            var personId = CurrentRole.RoleName == "Agency" ? CurrentUser.Id : (Guid?)null;
            var agencyId = CurrentRole.RoleName == "Agency" ? CurrentUser.Organization.Id : int.Parse(ddAgency.SelectedValue);
            var providerId = int.Parse(ddProviders.SelectedValue);
            var revenueId = int.Parse(ddlRevenue.SelectedValue);
            var locationId = int.Parse(ddLocation.SelectedValue);
            var status = int.Parse(ddStatus.SelectedValue);
            var currencyId = int.Parse(drpCurrency.SelectedValue);
            var startDate = DateHandler.getDateTime(txtStartDate.Text);
            var endDate = DateHandler.getDateTime(txtEndDate.Text);
            var invoiceNumber = txtInvoice.Text;
            var name = txtName.Text;
            var dateType = int.Parse(ddlDateType.SelectedValue);
            var agency = orgRep.GetAgency(agencyId);
            var state = ddlStates.SelectedValue;


            //var invoices = invoiceRep.SearchInvoices(false, personId,agencyId, providerId, revenueId, locationId, status,
            //                                         currencyId, startDate, endDate, invoiceNumber, name, dateType);
            //var summary = invoiceRep.SearchInvoicesTotal(personId, agencyId, providerId, revenueId, locationId, status,
            //                                             currencyId, startDate, endDate, invoiceNumber, name, dateType);

            SearchClient searchClient = new SearchClient();

            IOrderedDictionary formValues = new OrderedDictionary();
            formatFormFields(ref formValues);

            //get data from ES server.	    
            //A bad idea, collapse parameters into a single object and pass the object
            IEnumerable<VersionInvoice> invoices = searchClient.searchInvoicesGetAll((bool)formValues["isAdmin"], (int)formValues["agencyId"],
                 (int)formValues["providerId"], formValues["locationIds"], formValues["revenueIds"], (int)formValues["status"],
                 (int)formValues["currencyId"], (DateTime)formValues["startDate"], (DateTime)formValues["endDate"],
                 (int)formValues["dateType"], (string)formValues["invoiceNumber"], (string)formValues["name"], (string)formValues["state"]);

            //This is a really bad idea, parameters should be collapsed into an object
            IList<InvoiceSummary> summary = searchClient.getSearchSummary((bool)formValues["isAdmin"], (int)formValues["agencyId"],
              (int)formValues["providerId"], formValues["locationIds"], formValues["revenueIds"], (int)formValues["status"],
              (int)formValues["currencyId"], (DateTime)formValues["startDate"], (DateTime)formValues["endDate"],
              (int)formValues["dateType"], (string)formValues["invoiceNumber"], (string)formValues["name"], (string)formValues["state"]);

            var collection = new NameValueCollection
                {
                    {"provider",ddProviders.SelectedItem.Text},
                    {"revenue",ddlRevenue.SelectedItem.Text},
                    {"location",ddLocation.SelectedItem.Text},
                    {"currency",drpCurrency.SelectedItem.Text},
                    {"status",ddStatus.SelectedItem.Text},
                    {"invoiceNumber",string.IsNullOrEmpty(txtInvoice.Text)?"All":txtInvoice.Text},
                    {"startDate", startDate.ToString("dd, MMM, yyyy")},
                    {"endDate", endDate.ToString("dd, MMM, yyyy")},
                    {"printDate", AppSettings.LocalTime.ToString("dd, MMM, yyyy")}
                };

            //byte[] theFile = ExportToExcel.DownloadInvoices(invoices, summary, collection);
            byte[] theFile = ExportToExcel.DownloadInvoices(invoices, summary, collection, agency);
            ExportToExcel.Download(this, theFile, "Invoices", FileType.Excel);

        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex);
            lblStatus.ForeColor = System.Drawing.Color.Red;
            lblStatus.Text = @"Date not in correct format";
        }
    }

    protected void dsInvoices_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            List<Nest.IHit<VersionInvoice>> results = e.ReturnValue as List<Nest.IHit<VersionInvoice>>;
            if (results != null)
            {
                if (results.Any())
                {
                    btnPrintInvoice.Visible = true;
                }
                else
                {
                    btnPrintInvoice.Visible = false;
                    gvInvoices.EmptyDataText = "Records not found";
                }

            }
        }
        else
        {
            e.ExceptionHandled = true;
        }
    }
    protected void ddAgency_SelectedIndexChanged(object sender, EventArgs e)
    {

        int agencyId = int.Parse(ddAgency.SelectedValue);

        ddProviders.DataSource = orgRep.GetServiceProviders(agencyId, 0, -1);
        ddProviders.DataBind();
        ddProviders.Items.Insert(0, new ListItem("All Customers", "-1"));

        ddlRevenue.DataSource = revRep.GetActiveRevenues(agencyId, -1);
        ddlRevenue.DataBind();
        ddlRevenue.Items.Insert(0, new ListItem("All Revenues", "-1"));

        ddLocation.DataSource = locRep.GetLocations(agencyId);
        ddLocation.DataBind();
        ddLocation.Items.Insert(0, new ListItem("All Locations", "-1"));
    }
    protected void dd_DataBound(object sender, EventArgs e)
    {
        if (CurrentRole.RoleName == "Agency")
        {
            IList<int> revenueIds = CurrentUser.RevenueAccess.Select(r => r.Id).ToList<int>();
            foreach (ListItem item in ddlRevenue.Items)
            {
                if (!revenueIds.Contains(int.Parse(item.Value)))
                    item.Enabled = false;
            }

            IList<int> locationIds = locRep.GetUserLocationIds(CurrentUser.Id);
            foreach (ListItem item in ddLocation.Items)
            {
                if (item.Value == "-1")
                {
                    continue;
                }
                if (!locationIds.Contains(int.Parse(item.Value)))
                    item.Enabled = false;
            }
        }
    }
    protected void gvInvoices_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow || e.Row.DataItem == null)
        {
            return;
        }

        IHit<VersionInvoice> invoice = (IHit<VersionInvoice>)e.Row.DataItem;
    }
    protected void gvInvoices_DataBound(object sender, EventArgs e)
    {
        try
        {
            var count = gvInvoices.Rows.Count;
            //int count = dt.Rows.Count;
            if (count > 0)
            {
                btnPrintInvoice.Visible = true;
            }
            else
            {
                btnPrintInvoice.Visible = false;
            }
        }
        catch(Exception ex)
        {
            lblStatus.Text = ex.Message;
        }
    }
}