@ECHO ON
set PRJ_NAME=BDA.Web
set WEB_ROOT=%WORKSPACE%\src\Site-Precompile
set PROJECT_ROOT=%WORKSPACE%\src\%PRJ_NAME%
set compiler=%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_compiler
 
echo Publishing site %PROJECT_ROOT% to %WEB_ROOT% 
 
del /S /Q %WEB_ROOT%\*.*
rmdir /S /Q %WEB_ROOT%\
%compiler% -p "%PROJECT_ROOT%" -v / /d "%WEB_ROOT%" || goto Error 

cd "%WEB_ROOT%"
nuget pack ..\site.nuspec -Version 1.0.%SVN_REVISION%-build%BUILD_NUMBER% || goto error
cd ..

goto Success
:Error
echo Site was not published 

goto End

:Success
echo Site published successfully 

:End