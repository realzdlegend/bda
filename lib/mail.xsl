<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <div id="header">	
	<img src="{MailTemplate/Logo}" style="width:72px;height:72px;float:left;margin-right:10px" alt="Logo"/>
	<h2 style="line-height:72px"><xsl:value-of select="MailTemplate/Title"/></h2>
	<div style="clear:both"></div>
	<xsl:for-each select="MailTemplate/Node">
		<p><xsl:value-of select="Text" /></p>
	 </xsl:for-each>
	<h4 style="border-bottom:1px solid #999"><xsl:value-of select="MailTemplate/Title"/></h4>
  </div>
	<div style="clear:both"></div>
  <div id="main">
	<table>
			<xsl:for-each select="MailTemplate/Details/MailDetail">
				<tr>
					<td><dt style="float:right"><b><xsl:value-of select="Label" /></b></dt></td>
					<td><dd style="float:left"><xsl:value-of select="Value" /></dd></td>
				</tr>
			</xsl:for-each>
	</table>
	<div style="clear:both;height:10px"></div>
  </div>
  <div id="footer">
	<small><xsl:value-of select="MailTemplate/Footer"/> 
	<a href="{MailTemplate/WebAddress}"><xsl:value-of select="MailTemplate/WebAddress"/></a></small>
  </div>  
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>